package fr.emse.rootz.mineschat.network;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryonet.Client;
import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;
import com.esotericsoftware.kryonet.Server;
import fr.emse.rootz.mineschat.data.DataCollection;
import fr.emse.rootz.mineschat.data.audiocapture.*;
import fr.emse.rootz.mineschat.display.components.ColoredInfoDialog;
import fr.emse.rootz.mineschat.display.components.ColoredWaitDialog;
import fr.emse.rootz.mineschat.display.components.ColoredWaitDialog.WaitActions;
import fr.emse.rootz.mineschat.display.options.DisplaySettings;
import fr.emse.rootz.mineschat.interfaces.DisplayInterface;
import fr.emse.rootz.mineschat.interfaces.NetworkAudioConferenceListener;
import fr.emse.rootz.mineschat.interfaces.NetworkManagerInterface;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

public class NetworkAudioConferenceManager implements NetworkAudioConferenceListener{

	private static int MAX_DATASOUNDPACKET_LIMIT = 1000;
	private DisplayInterface phaseListener;
	private NetworkManagerInterface networkManager;
	private int idDiscussion;
	private boolean active = false;
	// For the receiving part
	private AudioPlayer audioPlayer;
	private Server server;
	private Listener soundPacketReceiverListener;
	// For the sending part
	private AudioCapturer audioCapturer;
	private ArrayList<Client> clients = new ArrayList<>();
	private HashMap<String, Client> IPClients = new HashMap<>();
	private HashMap<Connection, String> connectionIP = new HashMap<>();
	private Thread senderThread;
	private Listener connectionLostListener;
	private boolean sending = false;
	private ArrayList<DataSoundPacket> soundPacketsToSend = new ArrayList<>();

	public NetworkAudioConferenceManager(NetworkManagerInterface networkManager, DisplayInterface phaseListener) {
		this.phaseListener = phaseListener;
		this.networkManager = networkManager;
		audioCapturer = new AudioCapturer();
		audioPlayer = new AudioPlayer();
		MCNetworkClient.registerAudioConferenceListener(this);
	}
	

	@Override
	public void receivedSoundPacket(DataSoundPacket packet) {
		audioPlayer.receiveDataSoundPacket(packet);
	}


	public void startAudioConference(int selectedDiscussion) {
		if(active && selectedDiscussion != idDiscussion)
			return;
		
		if(!active) {
			idDiscussion = selectedDiscussion;
			audioCapturer.startAudioCapture();
			audioPlayer.startListening();
			active = true;
			
			soundPacketReceiverListener = new Listener () {
				@Override
				public void received(Connection connection, Object object) {
					if(object instanceof DataAudioStart) {
						DataAudioStart audioConnection = (DataAudioStart) object;
						audioPlayer.receiveAudioConnection(audioConnection.getIdSender(), true);
					}
					else if(object instanceof DataSoundPacket)
						receivedSoundPacket((DataSoundPacket)object);
				}
			};
			
			connectionLostListener = new Listener () {
				
				@Override
				public void connected(Connection connection) {
					connectionIP.put(connection, connection.getRemoteAddressTCP().getAddress().getHostAddress());
					connection.sendTCP(new DataAudioStart(idDiscussion, DataCollection.getUser().getId()));
				}

				@Override
				public void disconnected(Connection connection) {
					System.out.println("Connexion perdue!");
					if(active) {
						System.out.println("Enlevage de la connexion!");
						String ip = connectionIP.get(connection);
						clients.remove(IPClients.get(ip));
					}
				}
			};
			
			startServer();
			
			networkManager.sendObject(new DataAudioConnection(selectedDiscussion, DataCollection.getUser().getId(), true));
			
			startSendingThread();
			
		}
		else {
			active = false;
			networkManager.sendObject(new DataAudioConnection(selectedDiscussion, DataCollection.getUser().getId(), false));
			audioCapturer.stopAudioCapture();
			audioPlayer.stopPlaying();
			stopServer();
		}
	}

	private void updateKryo (Kryo kryo) {
		kryo.register(int.class);
		kryo.register(String.class);
		kryo.register(byte.class);
		kryo.register(byte[].class);
		kryo.register(DataSoundPacket.class);
		kryo.register(DataAudioStart.class);
	}
	
	private void startServer () {
		if(server == null) {
			server = new Server(32768, 16384);
			//server = new Server();
			updateKryo(server.getKryo());
			server.addListener(connectionLostListener);
			server.addListener(new Listener() {
				@Override
				public void idle(Connection connection) {
					if(!active)
						return;
					if(!soundPacketsToSend.isEmpty()) {
						DataSoundPacket packet  = soundPacketsToSend.get(0);
						soundPacketsToSend.remove(0);
						if(packet!=null)
							server.sendToAllTCP(packet);
					}
				}
				
			});
		}
		
		server.start();
		
		try {
			server.bind(NetworkSettings.MCNETWORK_AUDIOCONF_TCP_PORT, NetworkSettings.MCNETWORK_AUDIOCONF_UDP_PORT);
			System.out.println("SERVEUR OPERATIONNEL!");
		} catch (IOException e) {
			new ColoredInfoDialog(phaseListener.getWindow(), DisplaySettings.nopeImage, 
					"Erreur lors de la création <br/> du mini-serveur local <br/> pour la conférence audio!");
		}
		
	}
	
	public void stopServer () {
		soundPacketsToSend.clear();
		server.stop();
		for(Client client : clients) {
			client.stop();
		}
		clients.clear();
		connectionIP.clear();
		IPClients.clear();
		
		server = null;
	}

	@Override
	public void receivedAudioConnection(final DataAudioConnection connection) {
		System.out.println("NOUVELLE CONNEXION ! "+connection.getIpAddress());
		if(connection.isConnection()) {
			new Thread(() -> {
                if (!addAndConnectNewClient(connection.getIpAddress())) {
                    new ColoredInfoDialog(phaseListener.getWindow(), DisplaySettings.nopeImage,
                            "Conférence audio : <br/>Pas réussi à se connecter <br/> au nouvel arrivant ("+
                    DataCollection.getPerson(connection.getIdSender()).getPseudonyme()+")");
                }
                //else {
                //	audioPlayer.receiveAudioConnection(connection.getIdSender(), connection.isConnection());
                //}
            }).start();
		}
		else {
			System.out.println("Enlevage de la connexion!");
			IPClients.get(connection.getIpAddress()).close();
			clients.remove(IPClients.get(connection.getIpAddress()));
			IPClients.remove(connection.getIpAddress());
		}
	}
	
	public boolean addAndConnectNewClient (String ip) {
		Client client = new Client(16384, 16384);
		//Client client = new Client();
		updateKryo(client.getKryo());
		client.addListener(soundPacketReceiverListener);
		IPClients.put(ip, client);
		client.start();
		try {
			client.connect(5000, ip, NetworkSettings.MCNETWORK_AUDIOCONF_TCP_PORT, NetworkSettings.MCNETWORK_AUDIOCONF_UDP_PORT);
			System.out.println("CLIENT OPERATIONNEL! "+ip+ ", "+client.getTcpWriteBufferSize());
			clients.add(client);
			return true;
		} catch (IOException e) {
			e.printStackTrace();
			new ColoredInfoDialog(phaseListener.getWindow(), DisplaySettings.nopeImage, 
					"Conférence audio : <br/> Impossible de se connecter <br/> à l'IP : "+ip);
		}
		
		return false;
	}
	
	private void startSendingThread() {
		senderThread = new Thread(() -> {
            /*
            if(!sending)
                sending = true;
            else
                return;*/

            System.out.println("DEBUT THREAD! " +sending);
            while(active) {
                byte[] soundPacket = audioCapturer.getNextSoundPacket();

                //if(soundPacketsToSend.size() > MAX_DATASOUNDPACKET_LIMIT)
                //	soundPacketsToSend.clear();
                if(soundPacketsToSend.size() > MAX_DATASOUNDPACKET_LIMIT) {
                    soundPacketsToSend.clear();
                }
                //if(buffer!=null)
                soundPacketsToSend.add(new DataSoundPacket(idDiscussion, DataCollection.getUser().getId(), soundPacket));
                //server.sendToAllTCP(new DataSoundPacket(idDiscussion, DataCollection.getUser().getId(), buffer.clone()));
            }
            System.out.println("FIN THREAD! "+sending);

            //sending = false;
        });
		senderThread.start();
	}
	
	/*
	private void sendSoundPacket(byte[] bytes) {
		DataSoundPacket packet = new DataSoundPacket(idDiscussion, DataCollection.getUser().getId(), bytes);
		for(Client client : clients) {
			if(client.isIdle())
				client.sendTCP(packet);
		}
	}*/

	@Override
	public void receivedConferenceInfo(final DataConferenceInfo info) {
		ColoredWaitDialog.doActionWithWaiting(phaseListener.getWindow(), new WaitActions(false) {
			@Override
			public void finishAction(boolean result) {
				if(!result) {
					//new ColoredInfoDialog(phaseListener.getWindow(), DisplaySettings.nopeImage, "Problème de connexion...");
				}
			}
			
			@Override
			public boolean backGroundAction() {
				boolean allWasGood = true;
				for(final String ip : info.getIPs()) {
					if(!addAndConnectNewClient(ip))
						allWasGood = false;
				}
				return allWasGood;
			}
		}, "Connexion à la conférence audio...");
	}


	
}
