package fr.emse.rootz.mineschat.network;

import com.esotericsoftware.kryonet.Client;
import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;
import fr.emse.rootz.mineschat.MinesChatSettings;
import fr.emse.rootz.mineschat.data.*;
import fr.emse.rootz.mineschat.data.audiocapture.DataAudioConnection;
import fr.emse.rootz.mineschat.data.audiocapture.DataConferenceInfo;
import fr.emse.rootz.mineschat.data.datatransmission.*;
import fr.emse.rootz.mineschat.data.screencapture.DataByteImagePacket;
import fr.emse.rootz.mineschat.data.screencapture.DataImagePacket;
import fr.emse.rootz.mineschat.data.screencapture.DataScreenSharingQuit;
import fr.emse.rootz.mineschat.data.screencapture.DataScreenSharingResponse;
import fr.emse.rootz.mineschat.display.components.ColoredInfoDialog;
import fr.emse.rootz.mineschat.display.options.DisplaySettings;
import fr.emse.rootz.mineschat.interfaces.NetworkConnectionListener;
import fr.emse.rootz.mineschat.interfaces.NetworkDataListener;
import fr.emse.rootz.mineschat.interfaces.NetworkFileTransfertListener;
import fr.emse.rootz.mineschat.interfaces.NetworkScreenSharingListener;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;

public class MCNetworkClient {
	
	private static Client client;
	private static boolean clientPrepared;
	private static InetAddress address = null;
	private static NetworkFileTransfertListener transfertListener;
	private static NetworkDataListener dataListener;
	private static NetworkConnectionListener connectionListener;
	private static NetworkScreenSharingListener screenSharingListener;
	private static NetworkAudioConferenceManager audioConferenceListener;
	private static boolean connected = false;
	
	public static void prepareClient () {
		if(!clientPrepared) {
			client = new Client(16384, 4096);
			//Log.DEBUG();
			//Log.TRACE();
			updateKryo();
			client.addListener(new Listener() {

				@Override
				public void connected(Connection connection) {
					MCNCconnected(connection);
				}

				@Override
				public void disconnected(Connection connection) {
					MCNCdisconnected(connection);
				}

				@Override
				public void received(Connection connection, Object object) {
					MCNCreceived(connection, object);
				}

				@Override
				public void idle(Connection connection) {
					MCNCidle(connection);
				}

			});
			client.start();
		}
	}

	public static boolean searchOnLAN () {
		boolean serverFound = false;

		if(!clientPrepared)
			prepareClient();
		
		// On teste d'abord les addresses ip habituelles
		/*
		if (setConnectionAddress("127.0.0.1")) {
			connectToServer();
			if (isConnected())
				return true;
		}*/
		
		if (setConnectionAddress("10.0.0.6")) {
			connectToServer();
			if (isConnected())
				return true;
		}
		
		if (setConnectionAddress("10.0.0.219")) {
			connectToServer();
			if (isConnected())
				return true;
		}

		if (setConnectionAddress("93.95.239.83")) {
			connectToServer();
			if (isConnected())
				return true;
		}

		if (setConnectionAddress("93.95.239.84")) {
			connectToServer();
			if (isConnected())
				return true;
		}
		
		//ArrayList<InetAddress> addresses = new ArrayList<>(client.discoverHosts(NetworkSettings.MCNETWORK_UDP_PORT, 5000));
		/*
		if(addresses.isEmpty()) {
			serverFound = false;
		} else
			try {
				if (addresses.contains(InetAddress.getByName("10.0.0.219"))) {
					address = InetAddress.getByName("10.0.0.219");
					serverFound = true;
				}
				else {
					address = addresses.get(0);
					serverFound = true;
				}
			} catch (UnknownHostException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}*/
		//*
		InetAddress address_0 = client.discoverHost(NetworkSettings.MCNETWORK_UDP_PORT, 5000);
	    if(address_0!=null) {
	    	serverFound = true;
	    	address = address_0;
	    }//*/
		return serverFound;
	}
	
	public static void programIsClosing () {
		if(clientPrepared) {
			client.stop();
			client.close();
		}
	}
	
	public static boolean setConnectionAddress (String addressString) {
		try {
			address = InetAddress.getByName(addressString);
			return true;
		} catch (UnknownHostException e) {
			return false;
		}
	}
	
	public static boolean connectionPossible () {
		return address!=null;
	}
	
	public static boolean isConnected () {
		return connected;
	}
	
	public static String getAdresseIPServer () {
		if(connectionPossible())
			return address.getHostAddress();
		else
			return null;
	}

	public static boolean connectToServer () {
		if(connectionPossible()) {
			prepareClient();
			try {
				client.connect(10000, address, NetworkSettings.MCNETWORK_TCP_PORT, NetworkSettings.MCNETWORK_UDP_PORT);
				//client.connect(10000, address, NetworkSettings.MCNETWORK_TCP_PORT);
				connected = true;
				//startDialog();
			} catch (IOException e) {
				connected = false;
				connectionListener.connectionEstablished(false);
			}
		}
		return connected;
	}
	
	public static void startDialog () {
		client.sendTCP(new ClientAuthentification(DataCollection.getUser().getPseudonyme(), MinesChatSettings.MCVersion, DisplaySettings.getColorIndex(), MinesChatSettings.MCVersionNumber));
	}
	
	private static void MCNCconnected(Connection connection) {
		//connection.setKeepAliveTCP(120000);
		new Thread(() -> {
            try {
                Thread.sleep(1000);
                connectionListener.connectionEstablished(true);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }).start();
	}

	private static void MCNCdisconnected(Connection connection) {
		clear();
		dataListener.serverLost();
	}

	private static void MCNCreceived(Connection connection, Object object) {
		if (object instanceof ServerBigData) {
			ServerBigData data = (ServerBigData) object;
			DataCollection.setServerName(data.getServerName());
			DataCollection.getUser().setId(data.getIdNewUser());
			data.getPeople().forEach(DataCollection::addPerson);
			dataListener.updateAlongDataCollection();
		}
		else if (object instanceof Message) {
			Message message = (Message) object;
			if(message.getType() == Message.MessageType.SERVER_POPUP) {
				new ColoredInfoDialog(null, DisplaySettings.minerImage, message.getMessage());
				//new ColoredInfoDialog(null, DisplaySettings.nopeImage, "Cette version est obsolète, \nveuillez la mettre à jour!");
				//connection.close();
				//clear();
				//dataListener.serverLost();
			}
			else if (message.getType() == Message.MessageType.SERVER_DIALOG) {
				//String password = new ColoredInputDialog(null, "Mot de passe du serveur "+message.getMessage()).getInput();
				sendObject(new Message(Message.MessageType.SERVER_DIALOG, 0, 0, "ME"));
			}
			else if (message.getId()==Message.translateType(Message.MessageType.SERVER_BLOCK)) {
				new ColoredInfoDialog(null, DisplaySettings.nopeImage, "Mauvais mot de passe!");
				connection.close();
			}
			else
				dataListener.receivedMessage((Message)object); 
		}
		else if (object instanceof PersonConnection) {
			dataListener.newConnection(((PersonConnection)object).getPerson());
		}
		else if (object instanceof PersonDisconnection) {
			dataListener.newDisconnection(((PersonDisconnection)object).getIdUserDisconnected());
		}
		else if (object instanceof Discussion) {
			Discussion discussion = (Discussion) object;
			dataListener.newDiscussion(discussion);
		}
		else if (object instanceof DiscussionInvitation) {
			DiscussionInvitation discussionInvitation = (DiscussionInvitation) object;
			dataListener.newDiscussionUpdate(discussionInvitation);
		}
		else if (object instanceof DiscussionQuit) {
			DiscussionQuit discussionQuit = (DiscussionQuit) object;
			dataListener.newDiscussionUpdate(discussionQuit);
		}
		else if (object instanceof DataArtGrid) {
			DataArtGrid dataArt = (DataArtGrid) object;
			dataListener.receivedArtGrid(dataArt);
		}
		else if (object instanceof DataTransfertInvitation) {
			DataTransfertInvitation invitation = (DataTransfertInvitation) object;
			transfertListener.receivedDataTransfertInvitation(invitation);
		}
		else if (object instanceof DataTransfertResponse) {
			DataTransfertResponse response = (DataTransfertResponse) object;
			transfertListener.receivedDataTransfertResponse(response);
		}
		else if (object instanceof DataBytePacket) {
			DataBytePacket dataPacket = (DataBytePacket) object;
			transfertListener.receivedDataPacket(dataPacket);
		}
		else if (object instanceof DataTransfertCancel) {
			DataTransfertCancel dataTransfertCancel= (DataTransfertCancel) object;
			transfertListener.receivedDataTransfertCancel(dataTransfertCancel);
		}
		else if (object instanceof DataTransfertConnectionInfo) {
			DataTransfertConnectionInfo dataTransfertInfo= (DataTransfertConnectionInfo) object;
			transfertListener.receivedDataTransfertInfo(dataTransfertInfo);
		}
		else if (object instanceof DataScreenSharingResponse) {
			screenSharingListener.receivedScreenSharingResponse((DataScreenSharingResponse) object);
		}
		else if (object instanceof DataImagePacket) {
			screenSharingListener.receiveImagePacket((DataImagePacket) object);
		}
		else if (object instanceof DataByteImagePacket) {
			screenSharingListener.receiveByteImagePacket((DataByteImagePacket) object);
		}
		else if (object instanceof DataScreenSharingQuit) {
			screenSharingListener.receivedScreenSharingQuit();
		}
		else if (object instanceof DataConferenceInfo) {
			audioConferenceListener.receivedConferenceInfo((DataConferenceInfo) object);
		}
		else if (object instanceof DataAudioConnection) {
			audioConferenceListener.receivedAudioConnection((DataAudioConnection) object);
		}
	}

	private static void MCNCidle(Connection connection) {
	}

	private static void updateKryo () {
		DataCollection.updateKryo(client.getKryo());
	}

	public static void registerDataListener (NetworkDataListener listener) {
		dataListener = listener;
	}

	public static void registerConnectionListener (NetworkConnectionListener listener) {
		connectionListener = listener;
	}

	public static void registerTransfertListener (NetworkFileTransfertListener listener) {
		transfertListener = listener;
	}

	public static void registerScreenSharingListener (NetworkScreenSharingListener listener) {
		screenSharingListener = listener;
	}

	public static void registerAudioConferenceListener (NetworkAudioConferenceManager listener) {
		audioConferenceListener = listener;
	}
	
	public static void sendObject (Object object) {
		if(connected)
			client.sendTCP(object);
	}
	
	private static void clear () {
		client.close();
		clientPrepared = false;
		address = null;
	}

	public static void addConnectionListener (Listener listener) {
		client.addListener(listener);
	}
	
	public static void removeConnectionListener (Listener listener) {
		client.removeListener(listener);
	}
}
