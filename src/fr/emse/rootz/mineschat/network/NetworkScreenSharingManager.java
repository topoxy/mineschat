package fr.emse.rootz.mineschat.network;

import com.esotericsoftware.kryonet.util.InputStreamSender;
import fr.emse.rootz.mineschat.data.DataCollection;
import fr.emse.rootz.mineschat.data.DataUtils;
import fr.emse.rootz.mineschat.data.screencapture.*;
import fr.emse.rootz.mineschat.display.components.ColoredImageDisplayer;
import fr.emse.rootz.mineschat.interfaces.DisplayInterface;
import fr.emse.rootz.mineschat.interfaces.NetworkManagerInterface;
import fr.emse.rootz.mineschat.interfaces.NetworkScreenSharingListener;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.util.HashMap;
import java.util.Random;

public class NetworkScreenSharingManager implements NetworkScreenSharingListener {
	private NetworkManagerInterface networkMediator;
	private DisplayInterface phaseListener;
	private Random random = new Random();
	

	// For the streamer part
	private ScreenCapturer capturer = null;
	private ScreenCaptureFrame screenFrame = null;
	private int idDiscussion;
	private boolean sending = false;
	
	// For the receiver part
	private ColoredImageDisplayer displayer = null;
	private HashMap<Integer, ImagePacketReceptor> images = new HashMap<>();
	private boolean receiving = false;

	public NetworkScreenSharingManager(
			NetworkManagerInterface mcBrain,
			DisplayInterface phaseListener) {
		this.networkMediator = mcBrain;
		this.phaseListener = phaseListener;
		MCNetworkClient.registerScreenSharingListener(this);
	}

	@Override
	public void startScreenSharing(int idDiscussion) {
		if(sending || receiving) {
			quitAndRelease();
		}
		else {
			DataScreenSharingRequest request = new DataScreenSharingRequest(idDiscussion, DataCollection.getUser().getId());
			networkMediator.sendObject(request);
		}
	}
	
	private void quitAndRelease () {
		DataScreenSharingQuit quit = new DataScreenSharingQuit(idDiscussion, DataCollection.getUser().getId());
		networkMediator.sendObject(quit);
		if(sending) {
			capturer.stopTimer();
			capturer = null;
			screenFrame = null;
			sending = false;
		}
		if(receiving) {
			displayer.dispose();
			displayer = null;
			images.clear();
			receiving = false;
		}
	}

	@Override
	public void receivedScreenSharingResponse(DataScreenSharingResponse response) {
		switch (response.getResponse()) {
		case 0:
			// Something failed
			break;
		case 1:
			// It creates a new stream
			startSendingStream(response);
			sending = true;
			break;
		case 2:
			startReceivingStream(response);
			receiving = true;
			break;
		default:
			break;
		}
	}

	private void startSendingStream(DataScreenSharingResponse response) {
		capturer = new ScreenCapturer();
		screenFrame = new ScreenCaptureFrame(100, 100, 400,400);
		capturer.connect(screenFrame, this);
		idDiscussion = response.getIdDiscussion();
		capturer.startTimer();
	}

	@Override
	public void sendScreenShot(BufferedImage image) {
		byte[] dataImage = DataUtils.imageToByteArray(image);
		final DataImagePacket imagePacket = new DataImagePacket(random.nextInt(), idDiscussion, dataImage.length);
		InputStreamSender stream = new InputStreamSender(new ByteArrayInputStream(dataImage), 4028) {
            protected void start () {
            	//System.out.println("Sending new image!");
        		networkMediator.sendObject(imagePacket);
            }

            protected Object next (byte[] bytes) {
            	return new DataByteImagePacket(imagePacket.getIdImage(), imagePacket.getIdDiscussion(), bytes);
            }
		};
		MCNetworkClient.addConnectionListener(stream);
	}
	
	private void startReceivingStream (DataScreenSharingResponse response) {
		displayer = new ColoredImageDisplayer(phaseListener.getWindow());
		displayer.setVisible(true);
	}

	@Override
	public void receiveImagePacket(DataImagePacket packet) {
		images.put(packet.getIdImage(), new ImagePacketReceptor(packet));
	}

	@Override
	public void receiveByteImagePacket(DataByteImagePacket packet) {
		if(images.containsKey(packet.getIdImage())) {
			images.get(packet.getIdImage()).receiveByteImagePacket(packet);
			if(images.get(packet.getIdImage()).isComplete()) {
				displayer.displayImage(images.get(packet.getIdImage()).createImageFromBytes());
				images.remove(packet.getIdImage());
			}
		}
	}

	@Override
	public void receivedScreenSharingQuit() {
		quitAndRelease();
	}
	
	

}
