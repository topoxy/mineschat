package fr.emse.rootz.mineschat.network;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryonet.Client;
import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;
import com.esotericsoftware.kryonet.Server;
import com.esotericsoftware.kryonet.util.InputStreamSender;
import fr.emse.rootz.mineschat.data.DataCollection;
import fr.emse.rootz.mineschat.data.DataUtils;
import fr.emse.rootz.mineschat.data.Person;
import fr.emse.rootz.mineschat.data.datatransmission.*;
import fr.emse.rootz.mineschat.display.components.ColoredInfoDialog;
import fr.emse.rootz.mineschat.display.components.ColoredProgressDialog;
import fr.emse.rootz.mineschat.display.components.ColoredQuestionDialog;
import fr.emse.rootz.mineschat.display.options.DisplaySettings;
import fr.emse.rootz.mineschat.interfaces.DisplayInterface;
import fr.emse.rootz.mineschat.interfaces.NetworkFileTransfertListener;
import fr.emse.rootz.mineschat.interfaces.NetworkManagerInterface;

import java.io.*;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

public class NetworkFileTransfertManager implements NetworkFileTransfertListener{

	protected static HashMap<Integer, FileReception> dataTransferts = new HashMap<>();
	// For the receiving part
	private static Client client;
	private DisplayInterface phaseListener;
	private NetworkManagerInterface networkManager;
	private Connection connection;
	// For the sending part
	private Server server = null;
	private HashMap <Integer, InputStreamSender> transferts = new HashMap<>();
	private HashMap <Integer, ColoredProgressDialog> transfertsDialogs = new HashMap<>();
	private HashMap <Integer, InputStream> transfertsFiles = new HashMap<>();
	private boolean sending = false;
	private boolean receiving = false;

	public NetworkFileTransfertManager(NetworkManagerInterface networkManager, DisplayInterface phaseListener) {
		this.phaseListener = phaseListener;
		this.networkManager = networkManager;
		MCNetworkClient.registerTransfertListener(this);
	}

	public static void removeDataTransfert (int idDataTransfert) {
		dataTransferts.remove(idDataTransfert);
	}

	public void sendDataTransfertInvitation (ArrayList<Person> receivers) {
		if(sending || receiving)
			return;
		if(receivers.contains(DataCollection.getUser()))
			receivers.remove(DataCollection.getUser());
		if(receivers.isEmpty())
			return;

		File file = DataUtils.chooseFile(phaseListener.getWindow());
		if(file!=null)
			for(Person receiver : receivers) {
				DataTransfertInvitation invitation = new DataTransfertInvitation(new Random().nextInt(), DataCollection.getUser().getId(), receiver.getId(), file.getName(), file.length(), file.getAbsolutePath());
				networkManager.sendObject(invitation);
			}
	}
	
	public void receivedDataTransfertInvitation(final DataTransfertInvitation invitation) {
		new Thread( () -> {
				phaseListener.signal();
				final ColoredQuestionDialog dialog = new ColoredQuestionDialog(null, DisplaySettings.receiveImage, "Transfert de fichier",
						DataCollection.getPerson(invitation.getIdSender()).getPseudonyme()+" vous envoie ce fichier : <br>\""+
				invitation.getNameFile()+"\", pesant "+DataUtils.sizeToText(invitation.getSizeFile())+". <br/> Voulez-vous le recevoir ?");
				dialog.setVisible(true);

				DataTransfertResponse response = new DataTransfertResponse(invitation, dialog.getResult());

				if(dialog.getResult()) {
					prepareDataTransfertReceivePart(invitation);
				}

				networkManager.sendObject(response);

		}).start();
	}

	private void updateKryo (Kryo kryo) {
		kryo.register(int.class);
		kryo.register(byte.class);
		kryo.register(byte[].class);
		kryo.register(String.class);
		kryo.register(DataBytePacket.class);
		kryo.register(DataTransfertCancel.class);
		kryo.register(DataTransfertComplete.class);
	}
	
	private void prepareDataTransfertSendPart (final DataTransfertInvitation invitation) {
		server = new Server(16384, 16384);
		updateKryo(server.getKryo());
		server.start();
		try {
			server.bind(NetworkSettings.MCNETWORK_SENDINGFILES_TCP_PORT, NetworkSettings.MCNETWORK_SENDINGFILES_UDP_PORT);
			sending = true;
			System.out.println("Serveur connecté!");
		} catch (IOException e) {
			new ColoredInfoDialog(phaseListener.getWindow(), DisplaySettings.nopeImage,
					"Erreur lors de la création <br/> du mini-serveur local <br/> pour l'envoi du fichier.");
			System.out.println("Erreur serveur!");
			return;
		}

		InputStream input = null;
		try {
			input = new FileInputStream(new File(invitation.getPathFile()));
			transfertsFiles.put(invitation.getIdDataTransfert(), input);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		final ColoredProgressDialog progressDialog = new ColoredProgressDialog(phaseListener.getWindow());
		transfertsDialogs.put(invitation.getIdDataTransfert(), progressDialog);
		progressDialog.startTransfert(this, invitation.getNameFile(), invitation.getSizeFile(), invitation.getIdDataTransfert(), invitation.getIdSender(), invitation.getIdInvitedPerson());

		final InputStreamSender stream = new InputStreamSender(input, 4028) {
            protected void start () {
            }

            protected Object next (byte[] bytes) {
            	//System.out.println("Sending bytes : "+bytes.length);
            	progressDialog.dataReceived(bytes.length);
                return new DataBytePacket(invitation.getIdDataTransfert(), invitation.getIdInvitedPerson(), bytes);
            }
		};

        //MCNetworkClient.addConnectionListener(stream);
		server.addListener(new Listener() {
			@Override
			public void connected(Connection connection) {
				connection.addListener(stream);
				setConnection(connection);
			}

			public void received(Connection connection, Object object) {
				if (object instanceof DataTransfertComplete) {
					closeServer();
					sending = false;
				}
				else if (object instanceof DataTransfertCancel) {
					DataTransfertCancel transfertCancel = (DataTransfertCancel) object;
					receivedDataTransfertCancel(transfertCancel);
				}
			}
		});
		sending = true;
		//server.addListener(stream);

		networkManager.sendObject(new DataTransfertConnectionInfo(invitation.getIdSender(), invitation.getIdInvitedPerson(), DataCollection.getUser().getIp()));

        transferts.put(invitation.getIdDataTransfert(), stream);
		progressDialog.setVisible(true);
	}
	
	private void closeServer () {
		server.close();
		connection = null;
		sending = false;
	}
	
	private void prepareDataTransfertReceivePart (DataTransfertInvitation invitation) {
		File receptionFile = DataUtils.chooseFileLocation(phaseListener.getWindow(), invitation.getNameFile());
		FileReception dataReception = new FileReception(invitation, receptionFile);
		addDataTransfert(dataReception);
		ColoredProgressDialog progressDialog = new ColoredProgressDialog(phaseListener.getWindow());
		transfertsDialogs.put(invitation.getIdDataTransfert(), progressDialog);
		dataReception.connectToDisplayer(this, progressDialog, invitation.getIdSender(), invitation.getIdInvitedPerson());
		dataReception.startTransfert();
		progressDialog.setVisible(true);
	}

	@Override
	public void receivedDataTransfertInfo(final DataTransfertConnectionInfo dataInfo) {
		new Thread( () -> {
			try {
				client = new Client(16384, 4096);
				updateKryo(client.getKryo());

				client.addListener(new Listener () {
					@Override
					public void connected(Connection connection) {
						setConnection(connection);
					}

					@Override
					public void received(Connection connection, Object object) {
						if (object instanceof DataBytePacket) {
							DataBytePacket dataPacket = (DataBytePacket) object;
							receivedDataPacket(dataPacket);
						}
						else if (object instanceof DataTransfertCancel) {
							DataTransfertCancel transfertCancel = (DataTransfertCancel) object;
							receivedDataTransfertCancel(transfertCancel);
						}
					}
				});
				InetAddress address = InetAddress.getByName(dataInfo.getSenderAddress());

				client.start();
				client.connect(5000, address, NetworkSettings.MCNETWORK_SENDINGFILES_TCP_PORT, NetworkSettings.MCNETWORK_SENDINGFILES_UDP_PORT);
				receiving = true;
			} catch (IOException e) {
				new ColoredInfoDialog(phaseListener.getWindow(), DisplaySettings.nopeImage,
						"Erreur lors de la connexion <br/> au serveur transmetteur <br/> pour l'envoi du fichier.");
				e.printStackTrace();
			}
		}).start();

	}

	public void receivedDataTransfertResponse(final DataTransfertResponse response) {
		new Thread(() -> {
				if(response.isResponse()) {
				new ColoredInfoDialog(phaseListener.getWindow(), DisplaySettings.okImage,
						DataCollection.getPerson(response.getInvitation().getIdInvitedPerson()).getPseudonyme() +
						" a accepté le transfert du fichier <br/><b>"+response.getInvitation().getNameFile()+" !</b> <br/> L'envoi du fichier va commencer!");
				prepareDataTransfertSendPart(response.getInvitation());
			}
			else {
				new ColoredInfoDialog(phaseListener.getWindow(), DisplaySettings.nopeImage,
						DataCollection.getPerson(response.getInvitation().getIdInvitedPerson()).getPseudonyme() +
						" a refusé le transfert du fichier "+response.getInvitation().getNameFile()+" ! <br/> L'envoi du fichier va commencer!");
			}
		}).start();
		phaseListener.signal();
	}

	public void receivedDataPacket (DataBytePacket dataPacket) {
		relayDataPacket(dataPacket);
	}

	public void receivedDataTransfertCancel(final DataTransfertCancel dataTransfertCancel) {
		new Thread(() -> {
				if(receiving) {
					closeClient(dataTransfertCancel.getIdDataTransfert());
					if(transfertsDialogs.containsKey(dataTransfertCancel.getIdDataTransfert())) {
						transfertsDialogs.get(dataTransfertCancel.getIdDataTransfert()).dispose();
						transfertsDialogs.remove(dataTransfertCancel.getIdDataTransfert());
					}
					new ColoredInfoDialog(phaseListener.getWindow(), DisplaySettings.nopeImage, "Le transfert du fichier "+dataTransfertCancel.getFileName()+" ! <br/> a été annulé!");
				}
				if(sending) {
					closeServer();
					if(transferts.containsKey(dataTransfertCancel.getIdDataTransfert()))
						transferts.remove(dataTransfertCancel.getIdDataTransfert());
					if(transfertsDialogs.containsKey(dataTransfertCancel.getIdDataTransfert())) {
						transfertsDialogs.get(dataTransfertCancel.getIdDataTransfert()).dispose();
						transfertsDialogs.remove(dataTransfertCancel.getIdDataTransfert());
					}
					new ColoredInfoDialog(phaseListener.getWindow(), DisplaySettings.nopeImage, "Le transfert du fichier "+dataTransfertCancel.getFileName()+" ! <br/> a été annulé!");
				}
				/*
				if(dataTransfertCancel.getIdReceiver() == DataCollection.getUser().getId()) {
					transferts.remove(dataTransfertCancel.getIdDataTransfert());
					transfertsDialogs.get(dataTransfertCancel.getIdDataTransfert()).dispose();
					transfertsDialogs.remove(dataTransfertCancel.getIdDataTransfert());
					new ColoredInfoDialog(phaseListener.getWindow(), DisplaySettings.nopeImage, "Le transfert du fichier "+dataTransfertCancel.getFileName()+" ! <br/> a été annulé!");
				}
				else {
					if(getDataTransfert(dataTransfertCancel.getIdDataTransfert())!=null) {
						getDataTransfert(dataTransfertCancel.getIdDataTransfert()).cancelDataTransfert();
						removeDataTransfert(dataTransfertCancel.getIdDataTransfert());
						new ColoredInfoDialog(phaseListener.getWindow(), DisplaySettings.nopeImage, "Le transfert du fichier "+dataTransfertCancel.getFileName()+" ! <br/> a été annulé!");
					}
				}*/
            phaseListener.signal();
		}).start();
	}

	public void sendCancelTransfert(DataTransfertCancel transfertCancel) {
		//networkManager.sendObject(transfertCancel);
		connection.sendTCP(transfertCancel);
		receivedDataTransfertCancel(transfertCancel);
	}

	public void transfertFinished(int idDataTransfert) {
		if(transfertsFiles.containsKey(idDataTransfert)) {
			try {
				transfertsFiles.get(idDataTransfert).close();
				transfertsFiles.remove(idDataTransfert);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if(transferts.containsKey(idDataTransfert)) {
				MCNetworkClient.removeConnectionListener(transferts.get(idDataTransfert));
				transferts.remove(idDataTransfert);
		}
		if(transfertsDialogs.containsKey(idDataTransfert)) {
			transfertsDialogs.remove(idDataTransfert);
		}
        phaseListener.signal();
		System.gc();
	}
	
	public void addDataTransfert (FileReception dataReception) {
		dataTransferts.put(dataReception.getIdDataTransfert(), dataReception);
	}
	
	public FileReception getDataTransfert (int idDataTransfert) {
		return dataTransferts.get(idDataTransfert);
	}
	
	private void closeClient (int idDataTransfert) {
		client.sendTCP(new DataTransfertComplete());
		client.close();
		receiving = false;
		connection = null;
		removeDataTransfert(idDataTransfert);
	}

	public void relayDataPacket (DataBytePacket dataPacket) {
		if (dataTransferts.containsKey(dataPacket.getIdDataTransfert())) {
			dataTransferts.get(dataPacket.getIdDataTransfert()).receiveDataPacket(dataPacket);
			if(dataTransferts.get(dataPacket.getIdDataTransfert()).isComplete()) {
				closeClient(dataPacket.getIdDataTransfert());
			}
		}
	}
	
	public void setConnection(Connection connection) {
		this.connection = connection;
	}
}
