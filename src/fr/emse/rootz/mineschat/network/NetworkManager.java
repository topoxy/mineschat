package fr.emse.rootz.mineschat.network;

import fr.emse.rootz.mineschat.data.DataCollection;
import fr.emse.rootz.mineschat.data.Person;
import fr.emse.rootz.mineschat.display.components.ColoredInfoDialog;
import fr.emse.rootz.mineschat.display.components.ColoredInputDialog;
import fr.emse.rootz.mineschat.display.options.DisplaySettings;
import fr.emse.rootz.mineschat.interfaces.DisplayInterface;
import fr.emse.rootz.mineschat.interfaces.NetworkManagerInterface;

import java.util.ArrayList;

/**
 * Created by vincent on 02/06/15.
 *
 * Contient toutes les classes liées au réseau.
 * Elle s'occupe de transmettre au serveur les données.
 */
public class NetworkManager implements NetworkManagerInterface {

    // DISPLAY MANAGERS
    private DisplayInterface phaseListener;

    // NETWORK MANAGERS
    private NetworkFileTransfertManager fileTransfertManager;
    private NetworkScreenSharingManager screenSharingManager;
    private NetworkAudioConferenceManager audioConferenceManager;

    public void setPhaseListener(DisplayInterface phaseListener) {
        this.phaseListener = phaseListener;
        fileTransfertManager = new NetworkFileTransfertManager(this, phaseListener);
        screenSharingManager = new NetworkScreenSharingManager(this, phaseListener);
        audioConferenceManager = new NetworkAudioConferenceManager(this, phaseListener);
    }

    @Override
    public void startDialog () {
        if(MCNetworkClient.isConnected()) {

            String userName = "";

            while(userName.isEmpty()) {
                userName = new ColoredInputDialog(phaseListener.getWindow(), "Choix du pseudonyme").getInput();
                if(userName.isEmpty()) {
                    new ColoredInfoDialog(phaseListener.getWindow(), DisplaySettings.quitImage, "Un peu vide ce pseudo, non ? \n Je sais que tu peux faire mieux!");
                }
            }

            DataCollection.getUser().setPseudonyme(userName);

            MCNetworkClient.startDialog();
        }
    }

    @Override
    public void sendObject (Object object) {
        MCNetworkClient.sendObject(object);
    }

    @Override
    public void sendDataTransfertInvitation (ArrayList<Person> receivers) {
        fileTransfertManager.sendDataTransfertInvitation(receivers);
    }

    @Override
    public void startScreenSharing(int idDiscussion) {
        screenSharingManager.startScreenSharing(idDiscussion);
    }

    @Override
    public void startAudioConference(int selectedDiscussion) {
        audioConferenceManager.startAudioConference(selectedDiscussion);
    }
}
