package fr.emse.rootz.mineschat.network;

public class NetworkSettings {

	/////////////////
	//		PORTS
	/////////////////

	public final static int MCNETWORK_TCP_PORT = 54424;
	public final static int MCNETWORK_UDP_PORT = 54742;

	public final static int MCNETWORK_SENDINGFILES_TCP_PORT = 54425;
	public final static int MCNETWORK_SENDINGFILES_UDP_PORT = 54743;
	
	public final static int MCNETWORK_AUDIOCONF_TCP_PORT = 54426;
	public final static int MCNETWORK_AUDIOCONF_UDP_PORT = 54744;
}
