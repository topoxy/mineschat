package fr.emse.rootz.mineschat.network;

import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;
import com.esotericsoftware.kryonet.Server;
import fr.emse.rootz.mineschat.data.*;
import fr.emse.rootz.mineschat.data.audiocapture.DataAudioConnection;
import fr.emse.rootz.mineschat.data.audiocapture.DataConferenceInfo;
import fr.emse.rootz.mineschat.data.datatransmission.*;
import fr.emse.rootz.mineschat.data.screencapture.*;
import fr.emse.rootz.mineschat.display.options.DisplaySettings;
import fr.emse.rootz.mineschat.interfaces.TextListener;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

public class MCNetworkServer {

	private static Server server = null;
	private static TextListener log = null;
	private static boolean serverActive = false;
	private static HashMap<Integer, Connection> usersConnection = new HashMap<>();
	private static HashMap<Connection, Integer> connectionUsers = new HashMap<>();
	private static ClientAuthentification temporaryClientAuthentification;
	private static HashMap<Connection, String> connectionIps = new HashMap<>();
	
	// For file transfert
	private static HashMap<Integer, ArrayList<DataBytePacket>> dataPackets = new HashMap<>();
	
	// For screensharing
	private static HashMap<Integer, ScreenSharingServer> screenSharingStreams = new HashMap<>();
	
	// For audio conferences
	private static HashMap<Integer, ArrayList<Integer>> audioConferences = new HashMap<>();
	private static HashMap<Integer, ArrayList<String>> audioConferencesIP = new HashMap<>();

	public static void registerLogger(final TextListener listener) {
		MCNetworkServer.log = listener;
		
		//Log.set(Log.LEVEL_TRACE);
		//Log.TRACE();
		/*
		Log.setLogger(new Logger() {
			@Override
			public void log(int level, String category, String message, Throwable ex) {
				StringBuilder builder = new StringBuilder(256);
	            builder.append(new Date());
	            builder.append(' ');
	            builder.append(level);
	            builder.append('[');
	            builder.append(category);
	            builder.append("] ");
	            builder.append(message);
	            if (ex != null) {
	                StringWriter writer = new StringWriter(256);
	                ex.printStackTrace(new PrintWriter(writer));
	                builder.append('\n');
	                builder.append(writer.toString().trim());
	            }
	            System.out.println(builder);
				MCNetworkServer.log.writeMessage(builder.toString());
			}
		});*/
	}

	public static String startServer() {
		String message = "";

		if (!serverActive) {
			server = new Server(16384, 16384);
			updateKryo();
			server.start();
			try {
				server.bind(NetworkSettings.MCNETWORK_TCP_PORT, NetworkSettings.MCNETWORK_UDP_PORT);
				//server.bind(NetworkSettings.MCNETWORK_TCP_PORT);
				//log.writeMessage("SERVER : Serveur démarré!");
				serverActive = true;
				prepareServerReception();
				//prepareAudioStream();
			} catch (IOException e) {
				message = "Erreur : " + e.getMessage();
			}
		}
		return message;
	}
	
	/*
	private static void prepareAudioStream () {
		if(!soundPackets.isEmpty()) {
			sendSoundPacket(soundPackets.get(0));
			soundPackets.remove(0);
		}
	}*/

	private static void prepareServerReception() {
		Discussion generalDiscussion = new Discussion(0, "Général", new ArrayList<>(), false, true);
		DataCollection.addDiscussion(generalDiscussion);
		
		prepareGeneralChannels();
		
		server.addListener(new Listener() {

			@Override
			public void connected(Connection connection) {
				MCNSconnected(connection);
			}

			@Override
			public void disconnected(Connection connection) {
				MCNSdisconnected(connection);
			}

			@Override
			public void received(Connection connection, Object object) {
				MCNSreceived(connection, object);
			}

			@Override
			public void idle(Connection connection) {
				MCNSidle(connection);
			}

		});
	}
	
	private static void prepareGeneralChannels () {
		DataCollection.addDiscussion(new Discussion(1, "1A", new ArrayList<>(), false, true));
		DataCollection.addDiscussion(new Discussion(2, "2A", new ArrayList<>(), false, true));
		DataCollection.addDiscussion(new Discussion(3, "3A", new ArrayList<>(), false, true));
		DataCollection.addDiscussion(new Discussion(4, "ME", new ArrayList<>(), false, true));
		DataCollection.addDiscussion(new Discussion(5, "NME", new ArrayList<>(), false, true));
		DataCollection.addDiscussion(new Discussion(6, "HorsME", new ArrayList<>(), false, true));
		DataCollection.addDiscussion(new Discussion(7, "Conquérants", new ArrayList<>(), false, true));
		DataCollection.addDiscussion(new Discussion(8, "Mesl", new ArrayList<>(), false, true));
		DataCollection.addDiscussion(new Discussion(9, "Sportifs", new ArrayList<>(), false, true));
		DataCollection.addDiscussion(new Discussion(10, "Pirates", new ArrayList<>(), false, true));
		DataCollection.addDiscussion(new Discussion(11, "Politique", new ArrayList<>(), false, true));
		DataCollection.addDiscussion(new Discussion(12, "Philosophes", new ArrayList<>(), false, true));
	}

	private static void MCNSconnected(Connection connection) {
		//connection.setKeepAliveTCP(120000);
		connectionIps.put(connection, connection.getRemoteAddressTCP().getAddress().getHostAddress());
	}

	private static void MCNSdisconnected(Connection connection) {
		int idDiscussion = -1;

		for(ArrayList<Integer> receivers : audioConferences.values()) {
			if(receivers.contains(connectionUsers.get(connection))) {
				receivers.remove(connectionUsers.get(connection));
			}
		}
		
		for(ArrayList<String> receivers : audioConferencesIP.values()) {
			if(receivers.contains(connectionIps.get(connection))) {
				receivers.remove(connectionIps.get(connection));
			}
		}
		
		for(ScreenSharingServer screenSharings : screenSharingStreams.values()) {
			if(screenSharings.getIdStreamer() == connectionUsers.get(connection))
				idDiscussion = screenSharings.getIdDiscussion();
			else if(screenSharings.getReceivers().contains(connectionUsers.get(connection)))
				userQuitsFromScreenSharing(connectionUsers.get(connection), screenSharings.getIdDiscussion());
		}
		if(idDiscussion>=0)
			userQuitsFromScreenSharing(connectionUsers.get(connection), idDiscussion);
		
		if(connection!=null && connectionUsers.containsKey(connection)) {
			PersonDisconnection disco = new PersonDisconnection(connectionUsers.get(connection));
			DataCollection.removePerson(connectionUsers.get(connection));
			noticeDisconnection(disco);
		}
		
	}
	
	private static void registerNewPerson (ClientAuthentification authentification, Connection connection) {
		int idUser = DataCollection.getNewValidID();
		final Person person = new Person(idUser, authentification.getPseudonyme(), connection.getRemoteAddressTCP().getHostString(), authentification.getColorIndex());
		DataCollection.addPerson(person, authentification.getMCVersion());

		noticeConnection(person);

		usersConnection.put(idUser, connection);
		connectionUsers.put(connection, idUser);
		
		DataCollection.addUserToDiscussion(idUser, 0);
		
		ServerBigData data = new ServerBigData(DataCollection.getServerName(), DataCollection.getCopyOfPeople(authentification.getMCVersion()), idUser);

		connection.sendTCP(data);
	}

	/*
	 * else if (object instanceof DataBytePacket) {
			final DataBytePacket dataPacket = (DataBytePacket) object;
			if(dataPackets.containsKey(dataPacket.getIdDataTransfert())){
				dataPackets.get(dataPacket.getIdDataTransfert()).add(dataPacket);
			}
			else {
				dataPackets.put(dataPacket.getIdDataTransfert(), new ArrayList<DataBytePacket> ());
				dataPackets.get(dataPacket.getIdDataTransfert()).add(dataPacket);
				usersConnection.get(dataPacket.getReceiver()).addListener(new TcpIdleSender() {
					@Override
					protected Object next() {
						if(dataPackets.get(dataPacket.getIdDataTransfert()).isEmpty()) {
							dataPackets.remove(dataPacket.getIdDataTransfert());
							System.gc();
							return null;
						}
						else {
							DataBytePacket dataPacketToGo = dataPackets.get(dataPacket.getIdDataTransfert()).get(0);
							dataPackets.get(dataPacket.getIdDataTransfert()).remove(0);
							return dataPacketToGo;
						}
					}
				});
			}
			//sendObjectToUser(dataPacket, dataPacket.getReceiver());
	 */
	
	private static void MCNSreceived(Connection connection, Object object) {
		if (object instanceof DataByteImagePacket) {
			DataByteImagePacket packet = (DataByteImagePacket) object;
			if(screenSharingStreams.containsKey(packet.getIdDiscussion()))
				screenSharingStreams.get(packet.getIdDiscussion()).receivePacket(packet);
		}
		else if (object instanceof DataImagePacket) {
			DataImagePacket packet = (DataImagePacket) object;
			if(screenSharingStreams.containsKey(packet.getIdDiscussion()))
				for(int idUser : screenSharingStreams.get(packet.getIdDiscussion()).getReceivers()) {
					sendObjectToUser(packet, idUser);
				}
		}
		else if (object instanceof ClientAuthentification) {
			ClientAuthentification authentification = (ClientAuthentification) object;
			
			/*if (authentification.getMCVersionNumber()<MinesChatSettings.MCVersionNumber) {
				connection.sendTCP(new Message(Message.MessageType.SERVER_POPUP, 0, 0, "/GodTalks La version de ce programme est trop ancienne. Télécharge la nouvelle!"));
				return;
			}*/
			/*
			if (!connection.getRemoteAddressTCP().toString().contains("10.0.") 
					&& !connection.getRemoteAddressTCP().toString().contains("10.42.")
						//&& !connection.getRemoteAddressTCP().toString().contains("127.0.0.1")
						) {
				temporaryClientAuthentification = authentification;
				connection.sendTCP(new Message(Message.MessageType.SERVER_DIALOG, 0, 0, DataCollection.getServerName()));
			}
			else {
			}*/
			registerNewPerson(authentification, connection);
		}
		else if  (object instanceof Message) {
            //TODO: Nettoyer ce bazar
			Message message = (Message) object;
			if(message.getId()>=0) {
				if (message.getMessage().startsWith("/join")) {
					analyseJoinRequest(message);
					return;
				}
				else if (message.getMessage().startsWith("/GodTalks")) {
					message.setIdSender(-2);
					message.setMessage(message.getMessage().substring(9).trim());
				}
				else if (message.getMessage().startsWith("/image")) {
					message.setMessage("html:<img src=\""+message.getMessage().substring(6)+"\"/>");
				}
					sendMessage(message);
			}
			else if(message.getType() == Message.MessageType.SERVER_DIALOG) {
				if(message.getMessage().equals("ME")) {
					registerNewPerson(temporaryClientAuthentification, connection);
				}
			}
		}
		else if  (object instanceof Discussion) {
			Discussion discussion = (Discussion) object;
			for(int idUser : discussion.getPeople()) {
				sendObjectToUser(discussion, idUser);
			}
			DataCollection.addDiscussion(discussion);
		}
		else if  (object instanceof DiscussionInvitation) {
			DiscussionInvitation discussionInvitation = (DiscussionInvitation) object;
			manageDiscussionInvitation(discussionInvitation);
		}
		else if (object instanceof DiscussionQuit) {
			DiscussionQuit discussionQuit = (DiscussionQuit) object;
			for(int idPerson : DataCollection.getDiscussions().get(discussionQuit.getIdDiscussion()).getPeople()) {
				sendObjectToUser(discussionQuit, idPerson);
			}
			DataCollection.removeUserFromDiscussion(discussionQuit.getIdPerson(), discussionQuit.getIdDiscussion(), true);
		}
		else if (object instanceof DataArtGrid) {
			DataArtGrid dataArt = (DataArtGrid) object;
			for(int idPerson : DataCollection.getDiscussions().get(dataArt.getIdDiscussion()).getPeople()) {
				sendObjectToUser(dataArt, idPerson);
			}
		}
		else if (object instanceof DataTransfertInvitation) {
			DataTransfertInvitation invitation = (DataTransfertInvitation) object;
			sendObjectToUser(invitation, invitation.getIdInvitedPerson());
		}
		else if (object instanceof DataTransfertResponse) {
			DataTransfertResponse response = (DataTransfertResponse) object;
			sendObjectToUser(response, response.getInvitation().getIdSender());
		}
		else if (object instanceof DataTransfertCancel) {
			DataTransfertCancel dataTransfertCancel = (DataTransfertCancel) object;
			sendObjectToUser(dataTransfertCancel, dataTransfertCancel.getIdSender());
			sendObjectToUser(dataTransfertCancel, dataTransfertCancel.getIdReceiver());
			dataPackets.remove(dataTransfertCancel.getIdDataTransfert());
		}
		else if (object instanceof DataScreenSharingRequest) {
			DataScreenSharingRequest request = (DataScreenSharingRequest) object;
			if(screenSharingStreams.containsKey(request.getIdDiscussion())) {
				screenSharingStreams.get(request.getIdDiscussion()).addReceiver(request.getIdSender(), connection);
				DataScreenSharingResponse response = new DataScreenSharingResponse
						(request.getIdDiscussion(), screenSharingStreams.get(request.getIdDiscussion()).getIdStreamer(), 2);
				sendObjectToUser(response, request.getIdSender());
				sendMessage(new Message(Message.MessageType.NORMAL, 0, request.getIdDiscussion(),
						DataCollection.getPerson(request.getIdSender()).getPseudonyme()+" s'est joint au partage d'écran!"));
			}
			else {
				ScreenSharingServer screenSharingServer = new ScreenSharingServer(request.getIdDiscussion(), request.getIdSender());
				screenSharingStreams.put(request.getIdDiscussion(), screenSharingServer);
				DataScreenSharingResponse response = new DataScreenSharingResponse
						(request.getIdDiscussion(), request.getIdSender(), 1);
				sendObjectToUser(response, request.getIdSender());
				sendMessage(new Message(Message.MessageType.NORMAL, 0, request.getIdDiscussion(),
						DataCollection.getPerson(request.getIdSender()).getPseudonyme()+" a lancé un partage d'écran!"));
			}
		}
		else if (object instanceof DataScreenSharingQuit) {
			DataScreenSharingQuit quit = (DataScreenSharingQuit) object;
			userQuitsFromScreenSharing(quit.getIdSender(), quit.getIdDiscussion());
		}
		else if (object instanceof DataTransfertConnectionInfo) {
			DataTransfertConnectionInfo info = (DataTransfertConnectionInfo) object;
			info.setSenderAddress(connection.getRemoteAddressTCP().getAddress().getHostAddress());
			sendObjectToUser(info, info.getIdReceived());
		}
		else if (object instanceof DataAudioConnection) {
			DataAudioConnection audioConnection = (DataAudioConnection) object;
			audioConnection.setIpAddress(connection.getRemoteAddressTCP().getAddress().getHostAddress());
			considerAudioConnection(audioConnection, connection);
		}
	}
	
	private static void considerAudioConnection (DataAudioConnection audioConnection, Connection connection) {
		if(audioConferences.containsKey(audioConnection.getIdDiscussion()))
			audioConferences.get(audioConnection.getIdDiscussion()).stream().filter(receiver -> receiver != audioConnection.getIdSender()).forEach(receiver -> {
				System.out.println("envoi de l'invi : " + receiver);
				sendObjectToUser(audioConnection, receiver);
			});
		
		if(audioConnection.isConnection()) {
			registerAudioConnection(audioConnection, connection);
		}
		else {
			if(audioConferences.containsKey(audioConnection.getIdDiscussion())) {
				audioConferences.get(audioConnection.getIdDiscussion()).remove((Integer)audioConnection.getIdSender());
				audioConferencesIP.get(audioConnection.getIdDiscussion()).remove(connectionIps.get(connection));
				// We inform the discussion
				for(int receiver : DataCollection.getDiscussions().get(audioConnection.getIdDiscussion()).getPeople()) {
					sendObjectToUser(new Message(Message.MessageType.NORMAL, 0, audioConnection.getIdDiscussion(), DataCollection.getPerson(audioConnection.getIdSender()).getPseudonyme() + " vient de quitter la conférence audio!"), receiver);
				}
			}
		}
	}
	
	private static void registerAudioConnection (DataAudioConnection audioConnection, Connection connection) {
		ArrayList<String> IPs;
		if (audioConferences.containsKey(audioConnection.getIdDiscussion()))
			IPs = new ArrayList<>(audioConferencesIP.get(audioConnection.getIdDiscussion()));
		else
			IPs = new ArrayList<>();
		
		if(!audioConferences.containsKey(audioConnection.getIdDiscussion())) {
			audioConferences.put(audioConnection.getIdDiscussion(), new ArrayList<>());
			audioConferencesIP.put(audioConnection.getIdDiscussion(), new ArrayList<>());
		}
		audioConferences.get(audioConnection.getIdDiscussion()).add(audioConnection.getIdSender());
		audioConferencesIP.get(audioConnection.getIdDiscussion()).add(audioConnection.getIpAddress());
		
		// We send to the user the list of all the IPs of the other machines, so that it can connect to them
		sendObjectToUser(new DataConferenceInfo(audioConnection.getIdDiscussion(), IPs), connectionUsers.get(connection));
		// We inform the discussion
		for(int receiver : DataCollection.getDiscussions().get(audioConnection.getIdDiscussion()).getPeople()) {
			sendObjectToUser(new Message(Message.MessageType.NORMAL, 0, audioConnection.getIdDiscussion(), DataCollection.getPerson(audioConnection.getIdSender()).getPseudonyme() + " vient de rejoindre la conférence audio!"), receiver);
		}
	}
	
	private static void userQuitsFromScreenSharing (int idUser, int idDiscussion) {
		if(!screenSharingStreams.containsKey(idDiscussion)) 
			return;
		
		if(screenSharingStreams.get(idDiscussion).getIdStreamer() == idUser) {
			// if it's the streamer
			DataScreenSharingQuit quit = new DataScreenSharingQuit();
			for(int idReceiver : screenSharingStreams.get(idDiscussion).getReceivers())
				sendObjectToUser(quit, idReceiver);
			screenSharingStreams.remove(idDiscussion);
			sendMessage(new Message(Message.MessageType.NORMAL, 0, idDiscussion,
					DataCollection.getPerson(idUser).getPseudonyme()+" a arrêté le partage d'écran!"));
		}
		else {
			if(screenSharingStreams.get(idDiscussion).getReceivers().contains(idUser)) {
				screenSharingStreams.get(idDiscussion).removeReceiver(idUser);
				//screenSharingStreams.get(idDiscussion).getReceivers().remove((Integer)idUser);
				sendMessage(new Message(Message.MessageType.NORMAL, 0, idDiscussion,
						DataCollection.getPerson(idUser).getPseudonyme()+" a quitté le partage d'écran!"));
			}
		}
	}

	private static void MCNSidle(Connection connection) {
	}
	
	public static void stopServer () {
		if(serverActive) {
			server.stop();
			serverActive = false;
			//log.writeMessage("SERVER : Serveur arrêté!");
			DataCollection.clearData();
			usersConnection.clear();
		}
	}

	public static boolean isServerActive() {
		return serverActive;
	}
	
	public static void programIsClosing () {
		stopServer();
	}
	
	private static void updateKryo () {
		DataCollection.updateKryo(server.getKryo());
	}

	private static void sendMessage (Message message) {
		for(int idPerson : DataCollection.getDiscussions().get(message.getIdDiscussion()).getPeople()) {
			Connection connection = usersConnection.get(idPerson);
			connection.sendTCP(message);
		}
	}
	
	private static void sendObjectToUser (Object object, int idPerson) {
		Connection connection = usersConnection.get(idPerson);
		connection.sendTCP(object);
	}

	private static void noticeConnection (Person newPerson) {
		PersonConnection userConnection;
		for(int idPerson : DataCollection.getDiscussions().get(0).getPeople()) {
			userConnection = new PersonConnection(newPerson.copy(DataCollection.getVersion(idPerson)));
			// Si jamais le client est une ancienne version, il faut adapter la couleur
			if(DisplaySettings.isNewVersionColor(newPerson.getColorIndex()) &&
					DataCollection.getVersion(idPerson).compareTo("V1.9") < 0) {
				userConnection.getPerson().setColorIndex(0);
			}
			Connection connection = usersConnection.get(idPerson);
			connection.sendTCP(userConnection);
		}
	}
	
	private static void noticeDisconnection (PersonDisconnection userDisconnection) {
		for(int idPerson : DataCollection.getDiscussions().get(0).getPeople()) {
			Connection connection = usersConnection.get(idPerson);
			connection.sendTCP(userDisconnection);
		}
	}
	
	private static void manageDiscussionInvitation (DiscussionInvitation discussionInvitation) {
		ArrayList<Integer>idInvitedPeople = discussionInvitation.getIdInvitedPeople();
		
		Discussion discussion = DataCollection.getDiscussions().get(discussionInvitation.getIdDiscussion());
		
		// We first filter the list to avoid sending the invitation to people already in it
		for(int i = 0; i < idInvitedPeople.size(); i++) {
			if(discussion.getPeople().contains(idInvitedPeople.get(i))) {
				idInvitedPeople.remove(idInvitedPeople.get(i));
				i--;
			}
		}
		
		if(idInvitedPeople.isEmpty())
			return;
		
		// We send this invitation to the people already in the group, so that they updated their list
		for(int idUser : DataCollection.getDiscussions().get(discussionInvitation.getIdDiscussion()).getPeople()) {
			sendObjectToUser(discussionInvitation, idUser);
		}
		
		// Then we update the server's one
		DataCollection.addPeopleToDiscussion(discussionInvitation.getIdDiscussion(), discussionInvitation.getIdInvitedPeople());
		
		// And we send the updated discussion to the invited people
		for(int idUser : discussionInvitation.getIdInvitedPeople()) {
			sendObjectToUser(DataCollection.getDiscussions().get(discussionInvitation.getIdDiscussion()), idUser);
		}
	}

	private static void analyseJoinRequest (Message message) {
		String request = message.getMessage();
		request = request.substring(5).trim();
		if(request.isEmpty()) {
			Message answer;
			String text = "html:";
			text += "<h1><b><center>Liste des canaux publics rejoignables</center></b></h1>";
			text += "<h2><b>[Canaux publics permanents]</b></h2>";
			text += "<ul>";
			for(Discussion discussion : DataCollection.getDiscussions().values()) {
				if(discussion.isPermanent() && !discussion.isPrivateDiscussion()) {
					text += "<li>";
					text += discussion.getName()+" [<b>"+discussion.getPeople().size()+"</b> personne(s) présente(s)]\n";
					text += "</li>";
				}
			}
			text += "</ul>";
			text += "</ul>";
			text += "<h2><b>[Canaux publics temporaires]</b></h2>";
			text += "<ul>";
			for(Discussion discussion : DataCollection.getDiscussions().values()) {
				if(!discussion.isPermanent() && !discussion.isPrivateDiscussion()) {
					text += "<li>";
					text += discussion.getName()+" [<b>"+discussion.getPeople().size()+"</b> personne(s) présente(s)]\n";
					text += "</li>";
				}
			}
			text += "</ul>";
			answer = new Message(Message.MessageType.NORMAL, 0, message.getIdDiscussion(), text);
			sendObjectToUser(answer, message.getIdSender());
		}
		else {
			Discussion discussion = DataCollection.getPublicDiscussionFromName(request);
			if(discussion==null) {
				discussion = new Discussion(new Random().nextInt(), request, new ArrayList<>(), false, false);
				discussion.getPeople().add(message.getIdSender());
				DataCollection.addDiscussion(discussion);
				sendObjectToUser(discussion, message.getIdSender());
			}
			else {
				ArrayList<Integer> invitedPeople = new ArrayList<>();
				invitedPeople.add(message.getIdSender());
				DiscussionInvitation invitation = new DiscussionInvitation(discussion.getId(), invitedPeople);
				manageDiscussionInvitation(invitation);
			}
		}
	}
	
}
