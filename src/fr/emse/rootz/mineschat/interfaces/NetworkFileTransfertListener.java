package fr.emse.rootz.mineschat.interfaces;

import fr.emse.rootz.mineschat.data.datatransmission.*;

public interface NetworkFileTransfertListener {
	void sendCancelTransfert(DataTransfertCancel transfertCancel);
	void transfertFinished(int idDataTransfert);
	void receivedDataTransfertInvitation (DataTransfertInvitation invitation);
	void receivedDataTransfertResponse (DataTransfertResponse response);
	void receivedDataPacket (DataBytePacket dataPacket);
	void receivedDataTransfertCancel(DataTransfertCancel dataTransfertCancel);
	void receivedDataTransfertInfo(DataTransfertConnectionInfo dataInfo);
}
