package fr.emse.rootz.mineschat.interfaces;

public interface TransfertDisplayer {
	void dataReceived (long nbBytes);
	void startTransfert (NetworkFileTransfertListener manager, String nameFile, long dataTotalSize, int idDataTransfert, int idSender, int idReceiver);
	void close ();
}
