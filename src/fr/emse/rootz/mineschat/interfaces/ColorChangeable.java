package fr.emse.rootz.mineschat.interfaces;

/**
 * Created by vincent on 03/06/15.
 */
public interface ColorChangeable {
    void updateColors();
}
