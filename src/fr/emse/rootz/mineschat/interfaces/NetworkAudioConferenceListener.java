package fr.emse.rootz.mineschat.interfaces;

import fr.emse.rootz.mineschat.data.audiocapture.DataAudioConnection;
import fr.emse.rootz.mineschat.data.audiocapture.DataConferenceInfo;
import fr.emse.rootz.mineschat.data.audiocapture.DataSoundPacket;

public interface NetworkAudioConferenceListener {
	void receivedSoundPacket(DataSoundPacket packet);
	void receivedConferenceInfo (DataConferenceInfo info);
	void receivedAudioConnection (DataAudioConnection connection);
}
