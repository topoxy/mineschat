package fr.emse.rootz.mineschat.interfaces;

import fr.emse.rootz.mineschat.data.*;

public interface NetworkDataListener {
	void receivedMessage (Message message);
	void receivedArtGrid (DataArtGrid dataArtGrid);
	void updateAlongDataCollection();
	void newConnection(Person person);
	void newDisconnection(int idUser);
	void newDiscussion(Discussion discussion);
	void newDiscussionUpdate(DiscussionInvitation discussionInvitation);
	void newDiscussionUpdate(DiscussionQuit discussionQuit);
	void serverLost ();
}
