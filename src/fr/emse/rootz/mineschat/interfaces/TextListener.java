package fr.emse.rootz.mineschat.interfaces;

public interface TextListener {
	void writeMessage (String message);
}
