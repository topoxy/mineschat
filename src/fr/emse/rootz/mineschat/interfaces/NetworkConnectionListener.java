package fr.emse.rootz.mineschat.interfaces;

public interface NetworkConnectionListener {
	void connectionEstablished(boolean success);
}
