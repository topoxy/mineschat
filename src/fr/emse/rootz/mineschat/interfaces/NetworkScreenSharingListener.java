package fr.emse.rootz.mineschat.interfaces;

import fr.emse.rootz.mineschat.data.screencapture.DataByteImagePacket;
import fr.emse.rootz.mineschat.data.screencapture.DataImagePacket;
import fr.emse.rootz.mineschat.data.screencapture.DataScreenSharingResponse;

import java.awt.image.BufferedImage;

public interface NetworkScreenSharingListener {
	void startScreenSharing(int idDiscussion);
	void receivedScreenSharingResponse (DataScreenSharingResponse response);
	void sendScreenShot(BufferedImage image);
	void receiveImagePacket(DataImagePacket packet);
	void receiveByteImagePacket(DataByteImagePacket packet);
	void receivedScreenSharingQuit ();
}
