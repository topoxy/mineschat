package fr.emse.rootz.mineschat.interfaces;

import fr.emse.rootz.mineschat.data.DataArtGrid;
import fr.emse.rootz.mineschat.data.Discussion;
import fr.emse.rootz.mineschat.data.Message;

public interface DiscussionInterface {
	void publishMessage (Message message);
	void publishArt (DataArtGrid artGrid);
	boolean isArtDialogActivated();
	void addDiscussionPane(Discussion discussion);
	void updatePeopleList (int idDiscussion);
	void updateAllPeopleList ();
	void clear();
	int getSelectedDiscussion();
}
