package fr.emse.rootz.mineschat.data;

import fr.emse.rootz.mineschat.display.options.DisplaySettings;

public class ArtGrid {

	private boolean[][] grid;
	
	public ArtGrid () {
		grid = new boolean [DisplaySettings.artGridSize][DisplaySettings.artGridSize];
	}
	
	public boolean getValue(int x, int y) {
		return grid[x][y];
	}
	
	public void setValue (int x, int y, boolean value) {
		grid[x][y] = value;
	}
	
	public boolean[][] getGrid() {
		return grid;
	}

	public ArtGrid copy () {
		ArtGrid artGrid = new ArtGrid();
		for(int i = 0; i < grid.length; i++) {
			for(int j = 0; j < grid.length; j++) {
				artGrid.setValue(i, j, getValue(i,j));
			}
		}
		
		return artGrid;
	}
}
