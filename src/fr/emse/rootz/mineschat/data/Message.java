package fr.emse.rootz.mineschat.data;

public class Message implements Identified {

	private int id;
	private int idSender;
	private int idDiscussion;
	private String message;
	public Message() {
		this(MessageType.NORMAL, 0, 0, "");
	}

	public Message(MessageType type, int idSender,
                   int idDiscussion, String message) {
		this.id = translateType(type);
		this.idSender = idSender;
		this.idDiscussion = idDiscussion;
		this.message = message;
	}

    public Message(int customID, int idSender, int idDiscussion, String message) {
        this.id = customID;
        this.idSender = idSender;
        this.idDiscussion = idDiscussion;
        this.message = message;
    }

	public static int translateType (MessageType type) {
		switch (type) {
			case NORMAL:
				return 0;
			case LOCAL:
				return 1;
			case SERVER_DIALOG:
				return -2;
			case SERVER_POPUP:
				return -1;
			case SERVER_BLOCK:
				return -3;
			default:
				return 0;
		}
	}
	
    public static MessageType translateId (int id) {
        switch (id) {
            case 0:
                return MessageType.NORMAL;
            case 1:
                return MessageType.LOCAL;
            case -2:
                return MessageType.SERVER_DIALOG;
            case -1:
                return MessageType.SERVER_POPUP;
            case -3:
                return MessageType.SERVER_BLOCK;
            default:
                return MessageType.PERSONALIZED;
        }
    }
	
	public int getId() {
		return id;
	}

	public int getIdSender() {
		return idSender;
	}

	public void setIdSender(int idSender) {
		this.idSender = idSender;
	}

    public String getNameSender () {
        return DataCollection.getPerson(idSender).getPseudonyme();
    }

	public int getIdDiscussion() {
		return idDiscussion;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

    public MessageType getType () { return translateId(id); }

	public enum MessageType {NORMAL, LOCAL, SERVER_DIALOG, SERVER_POPUP, SERVER_BLOCK, PERSONALIZED}

}
