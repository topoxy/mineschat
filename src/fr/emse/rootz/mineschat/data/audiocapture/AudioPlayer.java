package fr.emse.rootz.mineschat.data.audiocapture;

import fr.emse.rootz.mineschat.data.DataUtils;
import fr.emse.rootz.mineschat.display.components.ColoredInfoDialog;
import fr.emse.rootz.mineschat.display.options.DisplaySettings;

import javax.sound.sampled.*;
import java.util.HashMap;

public class AudioPlayer {

	private Mixer mixer;
	private HashMap<Integer, SourceDataLine> sources = new HashMap<>();
	private boolean playing = false;

	public AudioPlayer() {
		mixer = AudioSystem.getMixer(null); 
	}

	public void startListening() {
		if(playing)
			return;
		
		playing = true;
	}
	
	public void receiveDataSoundPacket (DataSoundPacket packet) {
		if(playing&&sources.containsKey(packet.getIdSender())) {
			byte[] data = DataUtils.decompress(packet.getData());
			sources.get(packet.getIdSender()).write(data, 0, data.length);
		}
	}
	
	public void stopPlaying() {
		if(playing) {
			for(int idLine : sources.keySet())
				stopLine(idLine);
			sources.clear();
			playing = false;
		}
		
		/*
		if(playing) {
			sourceDataLine.drain();
			sourceDataLine.stop();
			playing = false;
		}*/
	}
	
	private void addNewLine (int idLine) {
		AudioFormat audioFormat = AudioCapturer.getAudioFormat();
		  
		DataLine.Info dataLineInfo = new DataLine.Info(SourceDataLine.class,
				audioFormat);
		
		SourceDataLine sourceDataLine;
		
		try {
			sourceDataLine = (SourceDataLine) mixer.getLine(dataLineInfo);
			sourceDataLine.open(audioFormat);
			sourceDataLine.start();
			sources.put(idLine, sourceDataLine);
		} catch (LineUnavailableException e) {
			e.printStackTrace();
			new ColoredInfoDialog(null, DisplaySettings.nopeImage, "Impossible d'émettre du son...");
		}
	}

	private void stopLine(int idLine) {
		if(sources.containsKey(idLine))
			stopLine(sources.get(idLine));
	}
	
	private void stopLine(SourceDataLine line) {
		//line.drain();
		line.flush();
		line.stop();
	}
	
	public void receiveAudioConnection (int idSender, boolean isConnection) {
		if(isConnection) {
			if(!sources.containsKey(idSender))
				addNewLine(idSender);
		}
		else {
			stopLine(idSender);
			sources.remove(idSender);
		}
	}
}
