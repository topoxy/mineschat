package fr.emse.rootz.mineschat.data.audiocapture;

import java.util.ArrayList;

public class DataConferenceInfo {
	
	private int idDiscussion;
	private ArrayList<String> IPs;

	public DataConferenceInfo() {
		// TODO Auto-generated constructor stub
	}

	public DataConferenceInfo(int idDiscussion, ArrayList<String> iPs) {
		super();
		this.idDiscussion = idDiscussion;
		IPs = iPs;
	}

	public int getIdDiscussion() {
		return idDiscussion;
	}

	public void setIdDiscussion(int idDiscussion) {
		this.idDiscussion = idDiscussion;
	}

	public ArrayList<String> getIPs() {
		return IPs;
	}

	public void setIPs(ArrayList<String> iPs) {
		IPs = iPs;
	}

	
}
