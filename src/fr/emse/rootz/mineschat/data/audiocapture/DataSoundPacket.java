package fr.emse.rootz.mineschat.data.audiocapture;

public class DataSoundPacket {
	
	private int idDiscussion;
	private int idSender;
	private byte[] data;

	public DataSoundPacket() {
		this(0, 0, null);
	}

	public DataSoundPacket(int idDiscussion, int idSender, byte[] data) {
		super();
		this.idDiscussion = idDiscussion;
		this.idSender = idSender;
		this.data = data;
	}

	public int getIdDiscussion() {
		return idDiscussion;
	}

	public void setIdDiscussion(int idDiscussion) {
		this.idDiscussion = idDiscussion;
	}

	public int getIdSender() {
		return idSender;
	}

	public void setIdSender(int idSender) {
		this.idSender = idSender;
	}

	public byte[] getData() {
		return data;
	}

	public void setData(byte[] data) {
		this.data = data;
	}

}
