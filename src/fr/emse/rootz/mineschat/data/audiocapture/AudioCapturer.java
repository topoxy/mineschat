package fr.emse.rootz.mineschat.data.audiocapture;

import fr.emse.rootz.mineschat.data.DataUtils;
import fr.emse.rootz.mineschat.display.components.ColoredInfoDialog;
import fr.emse.rootz.mineschat.display.options.DisplaySettings;

import javax.sound.sampled.*;
import java.io.IOException;

public class AudioCapturer {

	private TargetDataLine targetLine;
	private AudioInputStream audioInputStream;

	private boolean capturing = false;

	public AudioCapturer() {
	}

	/*
	 * public AudioInputStream getStream() { if (capturing) return
	 * audioInputStream; else return null; }
	 */
/*
	public static AudioFormat getAudioFormat() {
		return new AudioFormat(AudioFormat.Encoding.PCM_SIGNED, 44100.0F, 16,
				2, 4, 44100.0F, false);
	}
//*/
	//*
	public static AudioFormat getAudioFormat() {
		return new AudioFormat(AudioFormat.Encoding.PCM_SIGNED, 22050.0f, // sampleRate
					16, // sampleSizeInBits
					1, // channels
					2, // frameSize
					22050.0f, // frameRate
					true); // bigEndian
	}

	public void startAudioCapture() {
		if (capturing)
			return;

		AudioFormat audioFormat = getAudioFormat();

		// DataLine.Info info = new DataLine.Info(TargetDataLine.class,
		// audioFormat);
		targetLine = null;
		try {
			targetLine = AudioSystem.getTargetDataLine(getAudioFormat());
			targetLine.open(audioFormat);
		} catch (LineUnavailableException e) {
			e.printStackTrace();
			new ColoredInfoDialog(null, DisplaySettings.nopeImage,
					"Impossible de capturer du son...");
			return;
		}

		audioInputStream = new AudioInputStream(targetLine);

		capturing = true;
		targetLine.start();
	}

	public void stopAudioCapture() {
		if (!capturing)
			return;

		//deflater.end();
		targetLine.stop();
		targetLine.close();

		capturing = false;
	}

	public boolean isCapturing() {
		return capturing;
	}

	public byte[] getNextSoundPacket() {
		byte[] packet = null;
		byte[] rawSound = new byte[500];

		if (!capturing) {
			System.out.println("PAS CAPTURING!");
			return null;
		}

		try {
			audioInputStream.read(rawSound, 0, rawSound.length);
			packet = DataUtils.compress(rawSound);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return packet;
	}
	//*/
}
