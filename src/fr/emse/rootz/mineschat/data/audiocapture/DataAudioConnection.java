package fr.emse.rootz.mineschat.data.audiocapture;

public class DataAudioConnection {
	
	private int idDiscussion;
	private int idSender;
	private boolean connection;
	private String ipAddress;

	public DataAudioConnection() {
		this(0, 0, true);
	}
	
	public DataAudioConnection(int idDiscussion, int idSender,
			boolean connection) {
		super();
		this.idDiscussion = idDiscussion;
		this.idSender = idSender;
		this.connection = connection;
	}

	public int getIdDiscussion() {
		return idDiscussion;
	}

	public void setIdDiscussion(int idDiscussion) {
		this.idDiscussion = idDiscussion;
	}

	public int getIdSender() {
		return idSender;
	}

	public void setIdSender(int idSender) {
		this.idSender = idSender;
	}

	public boolean isConnection() {
		return connection;
	}

	public void setConnection(boolean connection) {
		this.connection = connection;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

}
