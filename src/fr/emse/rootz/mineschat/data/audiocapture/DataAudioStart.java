package fr.emse.rootz.mineschat.data.audiocapture;

public class DataAudioStart {
	
	private int idDiscussion;
	private int idSender;

	public DataAudioStart() {
		// TODO Auto-generated constructor stub
	}

	public DataAudioStart(int idDiscussion, int idSender) {
		super();
		this.idDiscussion = idDiscussion;
		this.idSender = idSender;
	}

	public int getIdDiscussion() {
		return idDiscussion;
	}

	public void setIdDiscussion(int idDiscussion) {
		this.idDiscussion = idDiscussion;
	}

	public int getIdSender() {
		return idSender;
	}

	public void setIdSender(int idSender) {
		this.idSender = idSender;
	}

}
