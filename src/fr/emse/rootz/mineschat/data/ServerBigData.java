package fr.emse.rootz.mineschat.data;

import java.util.ArrayList;

public class ServerBigData {
	
	private String serverName;
	private ArrayList<Person> people;
	private int idNewUser;

	public ServerBigData() {
		this("", null, 0);
	}

	public ServerBigData(String serverName, ArrayList<Person> people, int idNewUser) {
		super();
		this.serverName = serverName;
		this.people = people;
		this.idNewUser = idNewUser;
	}

	public String getServerName() {
		return serverName;
	}

	public ArrayList<Person> getPeople() {
		return people;
	}

	public int getIdNewUser() {
		return idNewUser;
	}
	
	
	
}
