package fr.emse.rootz.mineschat.data;

public class PersonDisconnection {

	private int idUserDisconnected;
	
	public PersonDisconnection() {
		this(0);
	}

	public PersonDisconnection(int idUserDisconnected) {
		super();
		this.idUserDisconnected = idUserDisconnected;
	}

	public int getIdUserDisconnected() {
		return idUserDisconnected;
	}

	public void setIdUserDisconnected(int idUserDisconnected) {
		this.idUserDisconnected = idUserDisconnected;
	}
	
	

}
