package fr.emse.rootz.mineschat.data;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.zip.DataFormatException;
import java.util.zip.Deflater;
import java.util.zip.Inflater;

public class DataUtils {

	private final static JFileChooser fc = new JFileChooser();
	private final static JFileChooser fc2 = new JFileChooser();
	public static int SOUND_PACKET_SIZE = 300;
	private static File currentDirectory = new File(".");
	private static Deflater deflater = new Deflater();
	private static Inflater inflater = new Inflater();
	private static ByteArrayOutputStream outputDeflaterStream;
	private static ByteArrayOutputStream outputInflaterStream;
	private static byte[] deflaterBuffer = new byte[SOUND_PACKET_SIZE];
	private static byte[] inflaterBuffer = new byte[SOUND_PACKET_SIZE];
	
	public static void initializeCompressionTools () {
		deflater = new Deflater();
		deflater.setLevel(Deflater.BEST_COMPRESSION);
		outputDeflaterStream = new ByteArrayOutputStream(SOUND_PACKET_SIZE);
		outputInflaterStream = new ByteArrayOutputStream(SOUND_PACKET_SIZE);
	}

	public static byte[] compress(byte[] data) {
		deflater.reset();
		deflater.setInput(data);
		deflater.finish();

		outputDeflaterStream.reset();
		
		byte[] output = null;
		
		try {
			while (!deflater.finished()) {
				int count = deflater.deflate(deflaterBuffer); // returns the
																// generated
				// code... index
				outputDeflaterStream.write(deflaterBuffer, 0, count);
			}
			outputDeflaterStream.close();
			output = outputDeflaterStream.toByteArray();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return output;
	}

	public static byte[] decompress(byte[] data) {
		inflater.reset();
		inflater.setInput(data);
		
		outputInflaterStream.reset();
		//ByteArrayOutputStream outputStream = new ByteArrayOutputStream(data.length);
		byte[] output = null;

		try {
			while (!inflater.finished()) {
				int count;
					count = inflater.inflate(inflaterBuffer);
				outputInflaterStream.write(inflaterBuffer, 0, count);
			}
			outputInflaterStream.close();
			output = outputInflaterStream.toByteArray();

		} catch (DataFormatException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return output;
	}
	
	public static File chooseFile (JFrame parent) {
		File file = null;
		int result;

		fc.setCurrentDirectory(currentDirectory);
		result = fc.showOpenDialog(parent);
		if (result == JFileChooser.APPROVE_OPTION) {
			file = fc.getSelectedFile();
			currentDirectory = fc.getCurrentDirectory();
		}

		return file;
	}
	
	public static File chooseFileLocation (JFrame parent, String nameFile) {
		File file = null;
		int result = 0;

		
		fc2.setCurrentDirectory(currentDirectory);
		fc2.setSelectedFile(new File("./"+nameFile));
		result = fc2.showOpenDialog(parent);
		if (result == JFileChooser.APPROVE_OPTION) {
			file = fc2.getSelectedFile();
			currentDirectory = fc2.getCurrentDirectory();
		}
		
		return file;
	}
	
	public static byte[] getDataFromFile (File file) {
		byte[] data = null;
		
		try {
			data = Files.readAllBytes(file.toPath());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return data;
	
	}
	
	public static String sizeToText (long size) {
		String sizeText = "";
		if(size < 1024)
			sizeText = size + " b";
		else if (size < 1024*1024)
			sizeText = String.format("%.1f", size/1024f)  + " kb";
		else if (size < 1024*1024*1024)
			sizeText = String.format("%.1f", size/(1024f*1024f)) + " Mb";
		else 
			sizeText = String.format("%.1f", size/(1024f*1024f*1024f)) + " Gb";
		return sizeText;
	}
	
	public static String timeToText (long nbSeconds) {
		String timeText = "";
		if(nbSeconds < 60)
			timeText = nbSeconds + " secondes";
		else if (nbSeconds < 60*60)
			timeText = nbSeconds/60 + " minutes" + (nbSeconds%60 > 0 ? " et "+ nbSeconds%60 + " secondes " : "");
		else if (nbSeconds < 60*60*24)
			timeText = nbSeconds/(60*60) + " heures" + (nbSeconds%(60*60) > 0 ? " et "+ nbSeconds%(60*60) + " minutes " : "");
		return timeText;
	}
	
	public static byte[] imageToByteArray (BufferedImage image) {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		try {
			ImageIO.write(image, "jpg", baos);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		byte[] bytes = baos.toByteArray();
		return bytes;
	}
	
}
