package fr.emse.rootz.mineschat.data.screencapture;

public class DataScreenSharingRequest {

	private int idSender;
	private int idDiscussion;

	public DataScreenSharingRequest() {
		this(0, 0);
	}
	
	public DataScreenSharingRequest(int idDiscussion, int idSender) {
		super();
		this.idSender = idSender;
		this.idDiscussion = idDiscussion;
	}
	
	public int getIdSender() {
		return idSender;
	}

	public void setIdSender(int idSender) {
		this.idSender = idSender;
	}

	public int getIdDiscussion() {
		return idDiscussion;
	}

	public void setIdDiscussion(int idDiscussion) {
		this.idDiscussion = idDiscussion;
	}
	
	
}
