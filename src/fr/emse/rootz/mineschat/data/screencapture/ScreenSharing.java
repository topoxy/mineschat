package fr.emse.rootz.mineschat.data.screencapture;

public class ScreenSharing {

	protected int idStreamer;
	protected int idDiscussion;

	public ScreenSharing() {
	}
	
	public ScreenSharing(int idDiscussion, int idStreamer) {
		super();
		this.idStreamer = idStreamer;
		this.idDiscussion = idDiscussion;
	}
	public int getIdStreamer() {
		return idStreamer;
	}

	public void setIdStreamer(int idStreamer) {
		this.idStreamer = idStreamer;
	}
	
	public int getIdDiscussion() {
		return idDiscussion;
	}

	public void setIdDiscussion(int idDiscussion) {
		this.idDiscussion = idDiscussion;
	}

	
}
