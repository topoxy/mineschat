package fr.emse.rootz.mineschat.data.screencapture;

public class DataScreenSharingResponse {

	private int idStreamer;
	private int idDiscussion;
	private int response; // 0 = nope, 1 = you can start, 2 = there is already someone streaming, so it adds you to the receivers
	
	public DataScreenSharingResponse() {
		this(0, 0, 0);
	}

	public DataScreenSharingResponse(int idDiscussion, int idStreamer, int response) {
		super();
		this.idStreamer = idStreamer;
		this.idDiscussion = idDiscussion;
		this.response = response;
	}

	public int getIdStreamer() {
		return idStreamer;
	}

	public void setIdStreamer(int idStreamer) {
		this.idStreamer = idStreamer;
	}

	public int getIdDiscussion() {
		return idDiscussion;
	}

	public void setIdDiscussion(int idDiscussion) {
		this.idDiscussion = idDiscussion;
	}

	public int getResponse() {
		return response;
	}

	public void setResponse(int response) {
		this.response = response;
	}

}
