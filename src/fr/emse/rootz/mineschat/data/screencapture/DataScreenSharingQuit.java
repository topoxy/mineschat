package fr.emse.rootz.mineschat.data.screencapture;

public class DataScreenSharingQuit {
	
	private int idDiscussion;
	private int idSender;

	public DataScreenSharingQuit() {
		// TODO Auto-generated constructor stub
	}

	public DataScreenSharingQuit(int idDiscussion, int idSender) {
		super();
		this.idDiscussion = idDiscussion;
		this.idSender = idSender;
	}

	public int getIdDiscussion() {
		return idDiscussion;
	}

	public void setIdDiscussion(int idDiscussion) {
		this.idDiscussion = idDiscussion;
	}

	public int getIdSender() {
		return idSender;
	}

	public void setIdSender(int idSender) {
		this.idSender = idSender;
	}

	
}
