package fr.emse.rootz.mineschat.data.screencapture;

import fr.emse.rootz.mineschat.network.NetworkScreenSharingManager;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.Timer;
import java.util.TimerTask;

public class ScreenCapturer {

	private NetworkScreenSharingManager networkManager;
	private Robot robot = null;
	//private ImageDisplayer displayer = null;
	private ScreenCaptureFrame screenFrame = null;
	private Timer timer;
	
	public ScreenCapturer() {
		try {
			robot = new Robot();
		} catch (AWTException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void connect (ScreenCaptureFrame frame, NetworkScreenSharingManager manager) {
		this.screenFrame = frame;
		this.networkManager = manager;
	}
/*
	public void connect (ImageDisplayer displayer, ScreenCaptureFrame frame) {
		this.displayer = displayer;
		this.screenFrame = frame;
	}
	*/
	public BufferedImage takeScreenshot () {
		//System.out.println(screenFrame.getScreenArea());
		BufferedImage image = robot.createScreenCapture(screenFrame.getScreenArea());
		//System.out.println(DataUtils.imageToIntArray(image).length);
		
		//if(displayer!=null)
		//	displayer.displayImage(image, screenFrame.getScreenArea());
		return image;
	}
	
	public void startTimer () {
		timer = new Timer();
		timer.schedule(new TimerTask() {
			@Override
			public void run() {
				networkManager.sendScreenShot(takeScreenshot());
			}
		}, 100, 200);
	}
	
	public void stopTimer() {
		timer.cancel();
		screenFrame.disappear();
	}
	

}
