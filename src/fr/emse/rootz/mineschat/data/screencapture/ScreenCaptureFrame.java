package fr.emse.rootz.mineschat.data.screencapture;

import fr.emse.rootz.mineschat.display.components.ColoredPanel;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;

public class ScreenCaptureFrame {
	
	private int x1, y1, x2, y2;
	private FrameBar left, right, top, bottom;
	private FrameMover frameMover;
	private int barSize;

	public ScreenCaptureFrame(int x1, int y1, int x2, int y2) {
		super();
		this.x1 = x1;
		this.y1 = y1;
		this.x2 = x2;
		this.y2 = y2;
		barSize = 10;

		left = new FrameBar(this, new Point (x1, (y1+y2)/2), false);
		right = new FrameBar(this, new Point (x2, (y1+y2)/2), false);
		top = new FrameBar(this, new Point ((x1+x2)/2, y1), true);
		bottom = new FrameBar(this, new Point ((x1+x2)/2, y2), true);
		
		frameMover = new FrameMover();
		frameMover.setVisible(true);

		left.setCursor (Cursor.getPredefinedCursor(Cursor.W_RESIZE_CURSOR));
		right.setCursor (Cursor.getPredefinedCursor(Cursor.E_RESIZE_CURSOR));
		top.setCursor (Cursor.getPredefinedCursor(Cursor.N_RESIZE_CURSOR));
		bottom.setCursor (Cursor.getPredefinedCursor(Cursor.S_RESIZE_CURSOR));
		
		frameMover.setCursor (Cursor.getPredefinedCursor(Cursor.MOVE_CURSOR));
		
		left.connect(top, bottom);
		right.connect(top, bottom);
		top.connect(left, right);
		bottom.connect(left, right);

		
		left.adaptSize();
		right.adaptSize();
		top.adaptSize();
		bottom.adaptSize();
		left.calculateCenter();
		right.calculateCenter();
		top.adaptSize();
		bottom.adaptSize();
		
		replaceFrameMover();
		
	}
	
	public void replaceFrameMover () {
		frameMover.setLocation(right.getX() + right.getWidth() + barSize, right.getY()-barSize - frameMover.getHeight());
	}
	
	public Rectangle getScreenArea () {
		return new Rectangle(left.center().x+barSize, top.center().y+barSize, right.center().x-left.center().x-barSize*2, bottom.center().y-top.center().y-barSize*2);
	}
	
	public void disappear () {
		left.dispose();
		right.dispose();
		top.dispose();
		bottom.dispose();
		frameMover.dispose();
	}
	
	public void moveFrameBars (int dx, int dy) {
		left.setLocation(left.getX()+dx, left.getY()+dy);
		right.setLocation(right.getX()+dx, right.getY()+dy);
		top.setLocation(top.getX()+dx, top.getY()+dy);
		bottom.setLocation(bottom.getX()+dx, bottom.getY()+dy);
		
		left.calculateCenter();
		right.calculateCenter();
		top.calculateCenter();
		bottom.calculateCenter();
	}
	
	private class FrameMover extends JDialog {
		private Point mouseDownCompCoords;
		
		public FrameMover () {
			ColoredPanel panel = new ColoredPanel(ColoredPanel.ColorMode.INVERSED_CLEAR, 10);
			this.setContentPane(panel);
			
			this.setSize(barSize*4, barSize*4);
			
			this.prepareEvents();
			
			this.setUndecorated(true);
			this.setModal(false);
			this.setAlwaysOnTop(true);
			this.setVisible(true);
		}
		
		private void prepareEvents () {
			addMouseListener(new MouseAdapter() {
				public void mouseReleased(MouseEvent e) {
					mouseDownCompCoords = null;
				}

				public void mousePressed(MouseEvent e) {
					mouseDownCompCoords = e.getPoint();
				}
			});

			addMouseMotionListener(new MouseMotionAdapter() {
				public void mouseDragged(MouseEvent e) {
					Point currCoords = e.getLocationOnScreen();
					int dx = getX(), dy = getY();
					setLocation(currCoords.x - mouseDownCompCoords.x, currCoords.y - mouseDownCompCoords.y);
					dx = getX()-dx;
					dy = getY()-dy;
					moveFrameBars(dx, dy);
				}
			});
		}
	}

	private class FrameBar extends JDialog {
		
		private Point center;
		private ScreenCaptureFrame master;
		private boolean horizontal;
		private FrameBar one, two;
		private Point mouseDownCompCoords;

		public FrameBar(ScreenCaptureFrame master, Point center, boolean horizontal) {
			super();
			this.center = center;
			this.master = master;
			this.horizontal = horizontal;
			
			constructGUI();
			prepareEvents();

			this.setUndecorated(true);
			this.setModal(false);
			this.setAlwaysOnTop(true);
			this.setVisible(true);
		}
		
		private void constructGUI () {
			ColoredPanel panel = new ColoredPanel(ColoredPanel.ColorMode.INVERSED_CLEAR, 10);
			this.setContentPane(panel);
		}
		
		public void connect(FrameBar one, FrameBar two) {
			this.one = one;
			this.two = two;
		}

		private void prepareEvents () {
			addMouseListener(new MouseAdapter() {
				public void mouseReleased(MouseEvent e) {
					mouseDownCompCoords = null;
				}

				public void mousePressed(MouseEvent e) {
					mouseDownCompCoords = e.getPoint();
				}
			});

			addMouseMotionListener(new MouseMotionAdapter() {
				public void mouseDragged(MouseEvent e) {
					Point currCoords = e.getLocationOnScreen();
					if(!horizontal) {
						setLocation(currCoords.x - mouseDownCompCoords.x, getY());
						center.x = getX()+getWidth()/2;
					}
					else {
						setLocation(getX(), currCoords.y - mouseDownCompCoords.y);
						center.y = getY()+getHeight()/2;
					}
					one.adaptSize();
					two.adaptSize();
				}
			});
		}
		
		public void adaptSize() {
			if(horizontal) {
				this.setLocation(one.center().x-barSize/2, center.y-barSize/2);
				this.setSize(two.center().x - one.center().x+barSize, barSize);
			}
			else {
				this.setLocation(center.x-barSize/2, one.center().y-barSize/2);
				this.setSize(barSize, two.center().y - one.center().y+barSize);
			}
			replaceFrameMover();
		}
		
		public void calculateCenter () {
			if(!horizontal) {
				center.x = getX()+getWidth()/2;
			}
			else {
				center.y = getY()+getHeight()/2;
			}
		}
		
		public Point center() {
			return center;
		}
	}

}
