package fr.emse.rootz.mineschat.data.screencapture;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.ByteBuffer;

public class ImagePacketReceptor {
	
	private DataImagePacket imageInfo;
	private ByteBuffer data;
	//private byte[] data;
	
	public ImagePacketReceptor(DataImagePacket imageInfo) {
		super();
		this.imageInfo = imageInfo;
		//this.data = new byte[(int)imageInfo.getSizeData()];
		this.data = ByteBuffer.allocate(imageInfo.getSizeData());
	}
	
	public static BufferedImage createImageFromBytes(byte[] bytes) {
	    ByteArrayInputStream bais = new ByteArrayInputStream(bytes);
	    try {
	        return ImageIO.read(bais);
	    } catch (IOException e) {
	        throw new RuntimeException(e);
	    }
	}

	public void receiveByteImagePacket(DataByteImagePacket packet) {
		if(packet.getIdImage() == imageInfo.getIdImage())
			data.put(packet.getData());
	}

	public BufferedImage createImageFromBytes() {
	    ByteArrayInputStream bais = new ByteArrayInputStream(data.array());
	    try {
	        return ImageIO.read(bais);
	    } catch (IOException e) {
	        throw new RuntimeException(e);
	    }
	}
	
	public boolean isComplete () {
		return !data.hasRemaining();
	}
}
