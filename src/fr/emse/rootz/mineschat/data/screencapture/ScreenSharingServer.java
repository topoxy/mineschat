package fr.emse.rootz.mineschat.data.screencapture;

import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.util.TcpIdleSender;

import java.util.ArrayList;
import java.util.HashMap;

public class ScreenSharingServer extends ScreenSharing {

	private ArrayList<Integer> receivers = new ArrayList<>();
	private HashMap<Integer, ArrayList<DataByteImagePacket>> screenSharingPacketsByUser = new HashMap<>();
	private HashMap<Integer, Connection> connectionUsers = new HashMap<>();
	private HashMap<Integer, Boolean> activeReceivers = new HashMap<>();
	
	public ScreenSharingServer() {
	}

	public ScreenSharingServer(int idDiscussion, int idStreamer) {
		super(idDiscussion, idStreamer);
	}

	public ArrayList<Integer> getReceivers() {
		return receivers;
	}

	public void setReceivers(ArrayList<Integer> receivers) {
		this.receivers = receivers;
	}
	
	public void addReceiver (final int newUser, Connection connection) {
		receivers.add(newUser);
		connectionUsers.put(newUser, connection);
		addSenderToConnection(newUser, connection);
	}
	
	private void addSenderToConnection (final int newUser, Connection connection) {
		connection.addListener(new TcpIdleSender() {
			@Override
			protected Object next() {
				DataByteImagePacket packet = sendAndRemoveScreenSharingBytePacket(newUser);
				if(packet==null)
					activeReceivers.put(newUser, false);
				return packet;
			}
		});
		activeReceivers.put(newUser, true);
	}
	
	public void removeReceiver (int quitUser) {
		receivers.remove((Integer)quitUser);
		activeReceivers.remove(quitUser);
		screenSharingPacketsByUser.remove(quitUser);
		connectionUsers.remove(quitUser);
	}

	private DataByteImagePacket sendAndRemoveScreenSharingBytePacket (int idUser) {
		if(screenSharingPacketsByUser==null)
			return null;
		else if(!screenSharingPacketsByUser.containsKey(idUser))
			return null;	
		else if (screenSharingPacketsByUser.get(idUser).isEmpty()) {
			screenSharingPacketsByUser.remove(idUser);
			return null;
		}
		else {
			DataByteImagePacket packet = screenSharingPacketsByUser.get(idUser).get(0);
			screenSharingPacketsByUser.get(idUser).remove(0);
			return packet;
		}
	}

	public void receivePacket (DataByteImagePacket packet) {
		for(int idUser : receivers) {
			if(!screenSharingPacketsByUser.containsKey(idUser))
				screenSharingPacketsByUser.put(idUser, new ArrayList<>());
			if(!activeReceivers.get(idUser)) {
				addSenderToConnection(idUser, connectionUsers.get(idUser));
			}
			screenSharingPacketsByUser.get(idUser).add(packet);
		}
	}
}
