package fr.emse.rootz.mineschat.data.screencapture;

public class DataByteImagePacket {
	
	private int idImage;
	private int idDiscussion;
	private byte[] data;

	public DataByteImagePacket() {
	}

	public DataByteImagePacket(int idImage, int idDiscussion, byte[] data) {
		super();
		this.idImage = idImage;
		this.idDiscussion = idDiscussion;
		this.data = data;
	}

	public int getIdImage() {
		return idImage;
	}

	public void setIdImage(int idImage) {
		this.idImage = idImage;
	}

	public int getIdDiscussion() {
		return idDiscussion;
	}

	public void setIdDiscussion(int idDiscussion) {
		this.idDiscussion = idDiscussion;
	}

	public byte[] getData() {
		return data;
	}

	public void setData(byte[] data) {
		this.data = data;
	}

	
}
