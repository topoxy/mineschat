package fr.emse.rootz.mineschat.data.screencapture;


public class DataImagePacket {

	private int idImage;
	private int idDiscussion;
	private int sizeData;

	public DataImagePacket() {
	}
	
	public DataImagePacket(int idImage, int idDiscussion, int sizeData) {
		super();
		this.idImage = idImage;
		this.idDiscussion = idDiscussion;
		this.sizeData = sizeData;
	}

	public int getIdImage() {
		return idImage;
	}

	public void setIdImage(int idImage) {
		this.idImage = idImage;
	}

	public int getIdDiscussion() {
		return idDiscussion;
	}

	public void setIdDiscussion(int idDiscussion) {
		this.idDiscussion = idDiscussion;
	}

	public int getSizeData() {
		return sizeData;
	}

	public void setSizeData(int sizeData) {
		this.sizeData = sizeData;
	}

	

}
