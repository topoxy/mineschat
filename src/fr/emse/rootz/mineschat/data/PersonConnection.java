package fr.emse.rootz.mineschat.data;

public class PersonConnection {
	
	private Person person;

	public PersonConnection() {
		this(null);
	}

	public PersonConnection(Person person) {
		super();
		this.person = person;
	}

	public Person getPerson() {
		return person;
	}

	public void setPerson(Person person) {
		this.person = person;
	}

	

}
