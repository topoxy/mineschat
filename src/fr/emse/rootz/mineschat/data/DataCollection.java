package fr.emse.rootz.mineschat.data;

import com.esotericsoftware.kryo.Kryo;
import fr.emse.rootz.mineschat.data.audiocapture.DataAudioConnection;
import fr.emse.rootz.mineschat.data.audiocapture.DataConferenceInfo;
import fr.emse.rootz.mineschat.data.datatransmission.*;
import fr.emse.rootz.mineschat.data.screencapture.*;
import fr.emse.rootz.mineschat.display.options.DisplaySettings;

import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.HashMap;

public class DataCollection {

	protected static HashMap<Integer, Person> people = new HashMap<>();
	protected static HashMap<Integer, Discussion> discussions = new HashMap<>();
	protected static HashMap<Integer, String> versions = new HashMap<>();
	
	protected static Person user = new Person(0, "", "", DisplaySettings.getColorIndex());
	protected static String serverName = "";
	protected static int idValid = 0;
	protected static String localIP;

    public static void addPerson(Person person) {
        if(!people.containsKey(person.getId())) {
            people.put(person.getId(), person);
        }
    }
    public static void addPerson(Person person, String version) {
        if(!people.containsKey(person.getId())) {
            people.put(person.getId(), person);
        }
        if(!versions.containsKey(person.getId())) {
            versions.put(person.getId(), version);
        }
    }
	
	public static void removePerson (int idPerson) {
		ArrayList<Integer>idDiscussionsToRemove = new ArrayList<>();
		for(Discussion discussion : discussions.values()) {
			if(removeUserFromDiscussion(idPerson, discussion.getId(), false))
				idDiscussionsToRemove.add(discussion.getId());
		}
		for(int idDiscussion : idDiscussionsToRemove)
			removeDiscussion(idDiscussion);
		people.remove(idPerson);
	}

	public static void addDiscussion (Discussion discussion) {
		if(!discussions.containsKey(discussion.getId())) {
			discussions.put(discussion.getId(), discussion);
		}
	}

	public static void removeDiscussion (int idDiscussion) {
		if(discussions.containsKey(idDiscussion)) {
			discussions.remove(idDiscussion);
		}
	}
	
	public static void addPeopleToDiscussion(int idDiscussion, ArrayList<Integer> invitedPeople) {
		for(int idUser : invitedPeople)
			addUserToDiscussion(idUser, idDiscussion);
	}
	
	public static Person getPerson(int id) {
		return people.get(id);
	}

    public static String getVersion(int id) { return versions.get(id); }

	public static void updateKryo (Kryo kryo) {
		kryo.register(int.class);
		kryo.register(long.class);
		kryo.register(float.class);
		kryo.register(char[].class);
		kryo.register(boolean.class);
		kryo.register(boolean[].class);
		kryo.register(boolean[][].class);
		kryo.register(byte.class);
		kryo.register(byte[].class);
		kryo.register(String.class);
		kryo.register(ArrayList.class);
		kryo.register(ClientAuthentification.class);
		kryo.register(Discussion.class);
		kryo.register(Message.class);
		kryo.register(Person.class);
		kryo.register(ServerBigData.class);
		kryo.register(HashMap.class);
		kryo.register(PersonDisconnection.class);
		kryo.register(PersonConnection.class);
		kryo.register(DiscussionInvitation.class);
		kryo.register(DiscussionQuit.class);
		kryo.register(ArtGrid.class);
		kryo.register(DataArtGrid.class);
		kryo.register(DataTransfertInvitation.class);
		kryo.register(DataTransfertResponse.class);
		kryo.register(DataBytePacket.class);
		kryo.register(DataTransfertCancel.class);
		kryo.register(DataScreenSharingRequest.class);
		kryo.register(DataScreenSharingResponse.class);
		kryo.register(BufferedImage.class);
		kryo.register(DataImagePacket.class);
		kryo.register(DataByteImagePacket.class);
		kryo.register(DataScreenSharingQuit.class);
		kryo.register(DataTransfertConnectionInfo.class);
		kryo.register(DataTransfertComplete.class);
		kryo.register(DataAudioConnection.class);
		kryo.register(DataConferenceInfo.class);
	}
	
	public static Person getUser () {
		return user;
	}
	
	public static String getServerName () {
		return serverName;
	}
	
	public static void setServerName (String name) {
		serverName = name;
	}
	
	public static int getNewValidID () {
		idValid++;
		return idValid;
	}

    public static ArrayList<Person> getCopyOfPeople (String version) {
        ArrayList<Person> peopleArray = new ArrayList<>(people.size());
        for(Person person : people.values()) {
            peopleArray.add(person.copy(version));
        }
        return peopleArray;
    }
	
	public static ArrayList<Integer> getCopyOfPeopleID () {
		ArrayList<Integer> idPeopleArray = new ArrayList<>(people.size());
		for(Person person : people.values()) {
			idPeopleArray.add(person.getId());
		}
		return idPeopleArray;
	}
	
	public static void clearData () {
		people.clear();
		discussions.clear();
		idValid = 0;
		serverName = "";
		user.setId(0);
		user.setIp("");
		user.setPseudonyme("");
	}

	public static void addUserToDiscussion (int idUser, int idDiscussion) {
		discussions.get(idDiscussion).getPeople().add(idUser);
	}

	public static boolean removeUserFromDiscussion (int idUser, int idDiscussion, boolean canRemoveDiscussion) {
		if(discussions.get(idDiscussion).getPeople().contains(idUser)) {
			discussions.get(idDiscussion).getPeople().remove((Integer)idUser);
			// if the discussion is empty, it is removed
			if(discussions.get(idDiscussion).getPeople().isEmpty() && !discussions.get(idDiscussion).isPermanent()) {
				if(canRemoveDiscussion)
					discussions.remove(idDiscussion);
				return true;
			}
		}
		return false;
	}

	public static HashMap<Integer, Discussion> getDiscussions() {
		return discussions;
	}
	
	public static Discussion getPublicDiscussionFromName (String name) {
		for(Discussion discussion : discussions.values()) {
			if(!discussion.isPrivateDiscussion() && discussion.getName().equals(name)) {
				return discussion;
			}
		}
		return null;
	}

	public static String getLocalIP() {
		return localIP;
	}

	public static void setLocalIP(String localIP) {
		DataCollection.localIP = localIP;
	}
	
}
