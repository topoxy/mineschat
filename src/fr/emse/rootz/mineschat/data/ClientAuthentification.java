package fr.emse.rootz.mineschat.data;

public class ClientAuthentification {
	private String pseudonyme;
	private String MCVersion; 
	private int MCVersionNumber; 
	private int colorIndex;

	public ClientAuthentification() {
		this("","", 0, 0);
	}

	public ClientAuthentification(String pseudonyme, String mCVersion, int c, int n) {
		super();
		this.pseudonyme = pseudonyme;
		MCVersion = mCVersion;
		this.colorIndex = c;
		this.MCVersionNumber = n;
	}
	
	public String getPseudonyme() {
		return pseudonyme;
	}
	public String getMCVersion() {
		return MCVersion;
	}

	public int getColorIndex() {
		return colorIndex;
	}

	public int getMCVersionNumber() {
		return MCVersionNumber;
	}

}
