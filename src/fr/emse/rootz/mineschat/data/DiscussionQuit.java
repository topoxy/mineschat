package fr.emse.rootz.mineschat.data;

public class DiscussionQuit {
	
	private int idPerson;
	private int idDiscussion;
	
	public DiscussionQuit() {
		this(0,1);
	}
	
	public DiscussionQuit(int idPerson, int idDiscussion) {
		super();
		this.idPerson = idPerson;
		this.idDiscussion = idDiscussion;
	}

	public int getIdPerson() {
		return idPerson;
	}

	public void setIdPerson(int idPerson) {
		this.idPerson = idPerson;
	}

	public int getIdDiscussion() {
		return idDiscussion;
	}

	public void setIdDiscussion(int idDiscussion) {
		this.idDiscussion = idDiscussion;
	}
	
	
}
