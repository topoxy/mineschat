package fr.emse.rootz.mineschat.data.datatransmission;


public class DataTransfertInvitation {

	private String pathFile;
	private int idDataTransfert;
	private int idSender;
	private int idInvitedPerson;
	private String nameFile;
	private long sizeFile;
	
	public DataTransfertInvitation() {
		this(0, 0, 0, "", 0, "");
	}

	public DataTransfertInvitation(int idDataTransfert, int idSender,
			int idInvitedPerson, String nameFile, long sizeFile, String pathFile) {
		super();
		this.idDataTransfert = idDataTransfert;
		this.idSender = idSender;
		this.idInvitedPerson = idInvitedPerson;
		this.nameFile = nameFile;
		this.sizeFile = sizeFile;
		this.pathFile = pathFile;
	}

	public int getIdDataTransfert() {
		return idDataTransfert;
	}

	public void setIdDataTransfert(int idDataTransfert) {
		this.idDataTransfert = idDataTransfert;
	}
	public int getIdSender() {
		return idSender;
	}

	public void setIdSender(int idSender) {
		this.idSender = idSender;
	}

	public String getNameFile() {
		return nameFile;
	}

	public void setNameFile(String nameFile) {
		this.nameFile = nameFile;
	}

	public int getIdInvitedPerson() {
		return idInvitedPerson;
	}

	public void setIdInvitedPerson(int idInvitedPerson) {
		this.idInvitedPerson = idInvitedPerson;
	}

	public long getSizeFile() {
		return sizeFile;
	}

	public void setSizeFile(long sizeFile) {
		this.sizeFile = sizeFile;
	}

	public String getPathFile() {
		return pathFile;
	}

	public void setPathFile(String pathFile) {
		this.pathFile = pathFile;
	}	
	
}
