package fr.emse.rootz.mineschat.data.datatransmission;

public class DataTransfertConnectionInfo {
	
	private int idSender;
	private int idReceived;
	private String senderAddress;
	
	public DataTransfertConnectionInfo() {
		super();
	}

	public DataTransfertConnectionInfo(int idSender, int idReceived, String ip) {
		super();
		this.idSender = idSender;
		this.idReceived = idReceived;
		this.senderAddress = ip;
	}

	public int getIdSender() {
		return idSender;
	}

	public void setIdSender(int idSender) {
		this.idSender = idSender;
	}

	public int getIdReceived() {
		return idReceived;
	}

	public void setIdReceived(int idReceived) {
		this.idReceived = idReceived;
	}

	public String getSenderAddress() {
		return senderAddress;
	}

	public void setSenderAddress(String senderAddress) {
		this.senderAddress = senderAddress;
	}

}
