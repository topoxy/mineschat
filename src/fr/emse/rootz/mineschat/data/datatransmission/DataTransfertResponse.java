package fr.emse.rootz.mineschat.data.datatransmission;

public class DataTransfertResponse {
	
	private DataTransfertInvitation invitation;
	private boolean response;

	public DataTransfertResponse() {
		this(null, false);
	}
	
	public DataTransfertResponse(DataTransfertInvitation invitation,
			boolean response) {
		super();
		this.invitation = invitation;
		this.response = response;
	}

	public DataTransfertInvitation getInvitation() {
		return invitation;
	}

	public void setInvitation(DataTransfertInvitation invitation) {
		this.invitation = invitation;
	}

	public boolean isResponse() {
		return response;
	}

	public void setResponse(boolean response) {
		this.response = response;
	}

}
