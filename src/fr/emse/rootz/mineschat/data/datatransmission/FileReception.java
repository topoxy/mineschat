package fr.emse.rootz.mineschat.data.datatransmission;

import fr.emse.rootz.mineschat.interfaces.NetworkFileTransfertListener;
import fr.emse.rootz.mineschat.interfaces.TransfertDisplayer;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class FileReception {

	private int idDataTransfert;
	private String nameFile;
	private long totalSize;
	private long nbBytesReceived;
	private File file;
	private TransfertDisplayer displayer;
	//private ByteArrayOutputStream output;
	private FileOutputStream output;
	
	public FileReception(DataTransfertInvitation dataTransfert, File file) {
		super();
		this.idDataTransfert = dataTransfert.getIdDataTransfert();
		this.nameFile = dataTransfert.getNameFile();
		this.totalSize = dataTransfert.getSizeFile();
		this.file = file;
		
		nbBytesReceived = 0;
	}
	
	public void connectToDisplayer (NetworkFileTransfertListener manager, TransfertDisplayer displayer, int idSender, int idReceiver) {
		this.displayer = displayer;
		this.displayer.startTransfert(manager, nameFile, totalSize, idDataTransfert, idSender, idReceiver);
	}
	
	public void startTransfert () {
		//output = new ByteArrayOutputStream((int) totalSize);
		try {
			output = new FileOutputStream(file);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void receiveDataPacket (DataBytePacket data) {
		//System.out.println("Packet reçu! data length :" + data.getBytes().length);
		if(!(data.getIdDataTransfert() == idDataTransfert))
			return;
		if(isComplete())
			return;
				
		nbBytesReceived += data.getBytes().length;
		displayer.dataReceived(data.getBytes().length);
		
		try {
			output.write(data.getBytes());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if(isComplete()) {
			try {
				//Files.write(file.toPath(), output.toByteArray());
				//output.flush();
				output.close();
				output = null;
				System.gc();
				//NetworkFileTransfertManager.removeDataTransfert(idDataTransfert);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		/*
		try (FileOutputStream output = new FileOutputStream(file, true)) {
		    output.write(data.getBytes());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
	}
	
	public void cancelDataTransfert () {
		try {
			output.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		displayer.close();
	}

	public boolean isComplete () {
		return nbBytesReceived >= totalSize;
	}

	public String getNameFile() {
		return nameFile;
	}

	public long getTotalSize() {
		return totalSize;
	}

	public long getNbBytesReceived() {
		return nbBytesReceived;
	}

	public int getIdDataTransfert() {
		return idDataTransfert;
	}
	
	

}
