package fr.emse.rootz.mineschat.data.datatransmission;

public class DataBytePacket {
	
	private int idDataTransfert;
	private int receiver;
	private byte[] bytes;

	public DataBytePacket() {
		this(0, 0, null);
	}

	public DataBytePacket(int idDataTransfert, int receiver, byte[] bytes) {
		super();
		this.idDataTransfert = idDataTransfert;
		this.receiver = receiver;
		this.bytes = bytes;
	}

	public int getIdDataTransfert() {
		return idDataTransfert;
	}

	public void setIdDataTransfert(int idDataTransfert) {
		this.idDataTransfert = idDataTransfert;
	}

	public int getReceiver() {
		return receiver;
	}

	public void setReceiver(int receiver) {
		this.receiver = receiver;
	}

	public byte[] getBytes() {
		return bytes;
	}

	public void setBytes(byte[] bytes) {
		this.bytes = bytes;
	}

}
