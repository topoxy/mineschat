package fr.emse.rootz.mineschat.data.datatransmission;

public class DataTransfertCancel {

	private String fileName;
	private int idDataTransfert;
	private int idSender;
	private int idReceiver;
	
	public DataTransfertCancel() {
		this("",0, 0, 0);
	}
	
	public DataTransfertCancel(String fileName, int idDataTransfert, int idSender, int idR) {
		super();
		this.fileName = fileName;
		this.idDataTransfert = idDataTransfert;
		this.idSender = idSender;
		this.idReceiver = idR;
	}

	public int getIdDataTransfert() {
		return idDataTransfert;
	}

	public void setIdDataTransfert(int idDataTransfert) {
		this.idDataTransfert = idDataTransfert;
	}

	public int getIdSender() {
		return idSender;
	}

	public void setIdSender(int idSender) {
		this.idSender = idSender;
	}

	public int getIdReceiver() {
		return idReceiver;
	}

	public void setIdReceiver(int idReceiver) {
		this.idReceiver = idReceiver;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	
	
	
}
