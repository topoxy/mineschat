package fr.emse.rootz.mineschat.controls;

import javax.swing.*;

/**
 * Created by vincent on 02/06/15.
 *
 * Permet de faire l'interface entre la classe ControlsManager et le reste
 */
public interface ControlsInterface {
    JTextField generateConsole();
    void sendMessageAsServer(String text);
    void sendMessageAsServer(String text, int idDiscussion);
    void sendMessage(String text, int idMessage, int idDiscussion);
    void sendMessage(String message, int idDiscussion);
    void sendMessage(String message);
    void displayLocalMessage(String message);
}
