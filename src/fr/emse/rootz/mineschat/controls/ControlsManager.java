package fr.emse.rootz.mineschat.controls;

import fr.emse.rootz.mineschat.addon.AddonBase;
import fr.emse.rootz.mineschat.data.*;
import fr.emse.rootz.mineschat.display.components.ColoredInfoDialog;
import fr.emse.rootz.mineschat.display.components.PhasePanel;
import fr.emse.rootz.mineschat.display.options.DisplaySettings;
import fr.emse.rootz.mineschat.interfaces.DiscussionInterface;
import fr.emse.rootz.mineschat.interfaces.DisplayInterface;
import fr.emse.rootz.mineschat.interfaces.NetworkDataListener;
import fr.emse.rootz.mineschat.interfaces.NetworkManagerInterface;
import fr.emse.rootz.mineschat.network.MCNetworkClient;

import javax.swing.*;

/**
 * Cette classe centralise toutes les informations reçues et envoyées
 */
public class ControlsManager implements NetworkDataListener, ControlsInterface {

    // WINDOW MANAGERS
	private DisplayInterface displayInterface;
    // DISPLAY MANAGERS
	private DiscussionInterface discussionInterface;
    // NETWORK MANAGERS
    private NetworkManagerInterface networkManager;
    private AddonBase addonBase;

    // CONTROLS STUFF
    private MCConsole console;

	public ControlsManager() {
		MCNetworkClient.registerDataListener(this);
        this.console = new MCConsole();
        this.console.setControlsManager(this);
	}

    public void connectManagers(DisplayInterface phaseListener, DiscussionInterface discussionInterface, NetworkManagerInterface networkManager, AddonBase addonBase) {
        this.discussionInterface = discussionInterface;
        this.displayInterface = phaseListener;
        this.networkManager = networkManager;
        this.addonBase = addonBase;
    }

    @Override
    public JTextField generateConsole() {
        return console.generateConsole();
    }

    /**
     * C'est là qu'on envoie les messages.
     * @param message message à envoyer
     */
    @Override
    public void sendMessage(String message, int idDiscussion) {
        if (!addonBase.executeHook(AddonBase.HOOK_TYPE.HOOK_MESSAGESENDING, message, idDiscussion)) {
            if(MCNetworkClient.isConnected())
                networkManager.sendObject(new Message(Message.MessageType.NORMAL, DataCollection.getUser().getId(), idDiscussion, message));
            else
                discussionInterface.publishMessage(new Message(Message.MessageType.LOCAL, 0, 0, message));
        }
    }
    public void sendMessage(String message) {
        sendMessage(message, discussionInterface.getSelectedDiscussion());
    }

    @Override
    public void displayLocalMessage(String message) {
        receivedMessage(new Message(Message.MessageType.LOCAL, 0, discussionInterface.getSelectedDiscussion(), message));
    }


    public void sendMessage(String message, int idMessage, int idDiscussion) {
        if (!addonBase.executeHook(AddonBase.HOOK_TYPE.HOOK_MESSAGESENDING, message, idMessage, idDiscussion)) {
            if(MCNetworkClient.isConnected())
                networkManager.sendObject(new Message(idMessage, DataCollection.getUser().getId(), idDiscussion, message));
            else
                discussionInterface.publishMessage(new Message(Message.MessageType.LOCAL, 0, 0, message));
        }
    }

    /**
     * Permet d'afficher du texte sans le nom de l'utilisateur, comme si c'était le serveur qui parle
     * @param text
     * @param idDiscussion
     */
    @Override
    public void sendMessageAsServer(String text, int idDiscussion) {
        networkManager.sendObject(new Message(Message.MessageType.NORMAL, 0, idDiscussion, text));
    }

    @Override
    public void sendMessageAsServer(String text) {
        networkManager.sendObject(new Message(Message.MessageType.NORMAL, 0, discussionInterface.getSelectedDiscussion(), text));
    }

    /**
     * On reçoit les messages ici.
     * @param message message reçu
     */
	@Override
	public void receivedMessage(Message message) {
        if (!addonBase.executeHook(AddonBase.HOOK_TYPE.HOOK_MESSAGERECEPTION, message)) {
            displayInterface.signal();

            switch (message.getType()) {
                case NORMAL:
                case LOCAL:
                    System.out.println("message reçu : "+message);
                    discussionInterface.publishMessage(message);
                    break;
                case SERVER_POPUP:
                    new ColoredInfoDialog(displayInterface.getWindow(), DisplaySettings.optionsImage, message.getMessage());
                    break;
                default:
                    discussionInterface.publishMessage(message);
            }
        }
	}

    /**
     * C'est ici qu'on reçoit les oeuvres d'art pixel.
     * @param dataArtGrid art reçu
     */
	@Override
	public void receivedArtGrid(DataArtGrid dataArtGrid) {
        //TODO: Mettre du lua ici

        displayInterface.signal();
		discussionInterface.publishArt(dataArtGrid);
	}

    /**
     * On met à jour la base de données ici.
     */
	@Override
	public void updateAlongDataCollection() {
		SwingUtilities.invokeLater(() -> {
			Discussion generalDiscussion = new Discussion(0, "Général", DataCollection.getCopyOfPeopleID(), true, false);
			DataCollection.addDiscussion(generalDiscussion);
			discussionInterface.addDiscussionPane(generalDiscussion);
			discussionInterface.publishMessage(new Message (Message.MessageType.NORMAL, 0, 0, "Bienvenue sur le serveur " +
			DataCollection.getServerName() + ", "+DataCollection.getUser().getPseudonyme() + " !"));
		});
	}

    /**
     * Une nouvelle personne vient de se connecter
     * @param person personne connectée
     */
	@Override
	public void newConnection(Person person) {
        //TODO: Mettre du lua ici

		discussionInterface.publishMessage(new Message(Message.MessageType.NORMAL, 0, 0, "Connexion de " + person.getPseudonyme()));
		DataCollection.addPerson(person);
		DataCollection.addUserToDiscussion(person.getId(), 0);
		SwingUtilities.invokeLater(() -> discussionInterface.updatePeopleList(0));
        displayInterface.signal();
	}

    /**
     * Une nouvelle personne vient de se déconnecter
     * @param idUser personne déconnectée
     */
	@Override
	public void newDisconnection(int idUser) {
        //TODO: Mettre du lua ici

		discussionInterface.publishMessage(new Message(Message.MessageType.NORMAL, 0, 0, "Déconnexion de " + DataCollection.getPerson(idUser).getPseudonyme()));
		DataCollection.removePerson(idUser);
		SwingUtilities.invokeLater(discussionInterface::updateAllPeopleList);
        displayInterface.signal();
	}

    /**
     * L'utilisateur vient de rejoindre une nouvelle discussion
     * @param discussion discussion rejointe
     */
	@Override
	public void newDiscussion(final Discussion discussion) {
        //TODO: Mettre du lua ici

		DataCollection.addDiscussion(discussion);
		SwingUtilities.invokeLater(() -> {
            discussionInterface.addDiscussionPane(discussion);
            new ColoredInfoDialog(displayInterface.getWindow(), DisplaySettings.okImage, "Tu viens de rejoindre\n une nouvelle discussion :\n "+discussion.getName()+" !");
        });
        displayInterface.signal();
	}

    /**
     * De nouvelles personnes viennent de rejoindre la discussion
     * @param discussionInvitation données sur le changement de la changement
     */
	@Override
	public void newDiscussionUpdate(final DiscussionInvitation discussionInvitation) {
        //TODO: Mettre du lua ici

		DataCollection.addPeopleToDiscussion(discussionInvitation.getIdDiscussion(), discussionInvitation.getIdInvitedPeople());
		SwingUtilities.invokeLater(() -> {
            String message = "";
            if(discussionInvitation.getIdInvitedPeople().size() > 1)
                message = "De nouvelles personnes viennent de rejoindre la discussion !";
            else
                message = DataCollection.getPerson(discussionInvitation.getIdInvitedPeople().get(0)).getPseudonyme() +
                        " vient de rejoindre la discussion !";
            discussionInterface.publishMessage(new Message(Message.MessageType.NORMAL, 0, discussionInvitation.getIdDiscussion(), message));
            discussionInterface.updatePeopleList(discussionInvitation.getIdDiscussion());
        });
        displayInterface.signal();
	}

    /**
     * La connection est perdue.
     */
	@Override
	public void serverLost() {
        //TODO: Mettre du lua ici

		SwingUtilities.invokeLater(() -> {
            discussionInterface.clear();
            DataCollection.clearData();
            displayInterface.changePhase(PhasePanel.Type.CONNECTION);
            new ColoredInfoDialog(displayInterface.getWindow(), DisplaySettings.nopeImage, "Tu viens d'être déconnecté...");
        });
	}
	


    /**
     * L'utilisateur vient de quitter la discussion
     * @param discussionQuit
     */
	@Override
	public void newDiscussionUpdate(DiscussionQuit discussionQuit) {
        //TODO: Mettre du lua ici

		if(discussionQuit.getIdPerson()!=DataCollection.getUser().getId()) {
			DataCollection.removeUserFromDiscussion(discussionQuit.getIdPerson(), discussionQuit.getIdDiscussion(), true);
			if(discussionQuit.getIdPerson()!=DataCollection.getUser().getId()) {
				discussionInterface.updatePeopleList(discussionQuit.getIdDiscussion());
				discussionInterface.publishMessage(new Message(Message.MessageType.NORMAL, 0, discussionQuit.getIdDiscussion(),
						DataCollection.getPerson(discussionQuit.getIdPerson()).getPseudonyme() + " vient de quitter la discussion!"));
			}
		}
	}
}
