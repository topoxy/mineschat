package fr.emse.rootz.mineschat;

import fr.emse.rootz.mineschat.addon.AddonBase;
import fr.emse.rootz.mineschat.controls.ControlsManager;
import fr.emse.rootz.mineschat.data.DataCollection;
import fr.emse.rootz.mineschat.display.MainWindow;
import fr.emse.rootz.mineschat.display.options.DisplaySettings;
import fr.emse.rootz.mineschat.display.splashscreen.SplashScreen;
import fr.emse.rootz.mineschat.network.MCNetworkServer;
import fr.emse.rootz.mineschat.network.NetworkManager;

import java.util.Scanner;

/**
 *  Point de départ du programme.
 *  Lance la fenêtre principale pour le client,
 *  ou bien démarre les connexions réseaux pour le serveur.
 *  S'il est lancé avec un argument en ligne de commande,
 *  il se lance en mode serveur avec comme nom l'argument.
 *  Sinon, c'est le mode client normal.
 */
public class Main {

	  public static void main(String[] args) {
          /////////////////////////////////////////////////////////////////////////
          // LANCEMENT NORMAL
		  //*

          // MODE CLIENT
		  if(args.length == 0) {
              // Le splashscreen est lancé pour faire patienter l'utilisateur
              // le temps que le programme charge les ressources (sons et images)
			  SplashScreen splash = new SplashScreen();
			  splash.showSplashScreen(true);
			  DisplaySettings.prepareResources();

			  // On prépare la partie Réseau
			  NetworkManager networkManager = new NetworkManager();

              // On prépare la partie Contrôle
              ControlsManager controlsManager = new ControlsManager();

			  // On prépare la partie Affichage
			  MainWindow window = new MainWindow();

			  // On prépare le système de scripts lua
			  AddonBase addonBase = new AddonBase(controlsManager);

              // On connecte tout
			  networkManager.setPhaseListener(window);
              window.setNetworkManager(networkManager);
			  window.setAddonBase(addonBase);
			  window.getDiscussionPanel().setControlsInterface(controlsManager);
			  controlsManager.connectManagers(window, window.getDiscussionPanel(), networkManager, addonBase);

			  // On configure la base d'addons
			  addonBase.initialize();

              DisplaySettings.playSound("guitarSound.wav");

              splash.dispose();
          }
          // MODE SERVEUR
          else {
              boolean run = true;
              // Le serveur prend comme nom le paramètre donné en argument en ligne de commande
              DataCollection.setServerName(args[0]);
              // On va utiliser un scanner pour lire ce qui ce sera écrit sur la console
              Scanner in = new Scanner(System.in);
              String input;

              // On démarre le serveur
              MCNetworkServer.startServer();
              while (run) {
                  System.out.println("Que voulez vous faire ?   ");
                  input = in.nextLine();
                  // Si l'utilisateur le veut, on arrête le serveur
                  if (input.contains("exit") || input.contains("quit") || input.contains("stop")) {
                      run = false;
                      MCNetworkServer.stopServer();
                  }
              }
          }
          ///////////////////////////////////////////////////////////////////////*/

          /////////////////////////////////////////////////////////////////////////
          // TESTS PARTAGE ECRAN
		  /*
		  DataUtils.initializeCompressionTools();
		  final ScreenCapturer capturer = new ScreenCapturer();
		  ScreenCaptureFrame frame = new ScreenCaptureFrame(300, 300, 600, 600);
		  
		  capturer.connect(frame, null);
		  
		  Timer time = new Timer();
		  time.schedule(new TimerTask() {
				@Override
				public void run() {
					  BufferedImage image = capturer.takeScreenshot();
					  byte[] bytes = DataUtils.imageToByteArray(image);
					  byte[] bytes2 = DataUtils.compress(bytes);
					  System.out.println("Normal : "+bytes.length+" et Compressé : "+bytes2.length);
				}
			}, 100, 100);*/
		  
		  /*for(Mixer.Info info : AudioSystem.getMixerInfo()) {
			  System.out.println(info.getName());
			  
		  }
		  ///////////////////////////////////////////////////////////////////////*/
		  
		  

		  
		  /////////////////////////////////////////////////////////////////////////
          // TESTS AUDIO
		  /*

		  System.out.println(AudioSystem.getMixer(null).getMixerInfo().getDescription());
		  DataUtils.initializeCompressionTools();
		  AudioCapturer audioCapturer = new AudioCapturer();
		  AudioPlayer audioPlayer = new AudioPlayer();

		  audioPlayer.startListening();
		  audioCapturer.startAudioCapture();
		  
		  audioPlayer.receiveAudioConnection(0, true);

		  byte[] data;
		 for(int i = 0; i < 2000; i++) {
			data = audioCapturer.getNextSoundPacket();
			audioPlayer.receiveDataSoundPacket(new DataSoundPacket(0, 0, data));
		 }
		  
		  audioCapturer.stopAudioCapture();
		  audioPlayer.stopPlaying();
		  ///////////////////////////////////////////////////////////////////////*/


		  // LUA TESTING
          /*
		  Globals globals = JsePlatform.standardGlobals();
		  LuaValue chunk = globals.load("print 'hello, world'");
		  chunk.call();
		  */
	  }

}