package fr.emse.rootz.mineschat.addon;

/**
 * Created by victor on 02/06/15.
 */

import fr.emse.rootz.mineschat.addon.lua.LuaBase;
import fr.emse.rootz.mineschat.controls.ControlsInterface;

import java.util.ArrayList;

/**
 * Classe qui réunit les addons (scripts tels que les scripts lua) ajoutés au programme
 */
public class AddonBase {
    public enum HOOK_TYPE { HOOK_MESSAGERECEPTION, HOOK_MESSAGESENDING,
                            HOOK_ADDONS_RELOAD, HOOK_}
    private ArrayList<Addon> addons;
    private ControlsInterface controlsInterface;

    /**
     * Créer la base des addons
     */
    public AddonBase(ControlsInterface controlsInterface) {
        addons = new ArrayList<>();
        this.controlsInterface = controlsInterface;
    }

    /**
     * Initialize or re-initialize the addon base
     * @return le nombre d'addons chargés, -1 si il y a eu une erreur
     */
    public int initialize() {
        addons.clear();
        addons.add(new LuaBase(this, controlsInterface));
        addons.add(new AddonManager(this, controlsInterface));
        return addons.size();
    }

    public ArrayList<Addon> getAddonList() { return addons; }

    /**
     *
     * @param o Object
     * @return TRUE if MinesChat can further process the object(s), false otherwise
     */
    public boolean executeHook(HOOK_TYPE type, Object... o) {
        boolean retvalue = false;
        for (Addon addon: addons) {
            if (addon.executeHook(type, o))
                retvalue = true;
        }
        return retvalue;
    }
}
