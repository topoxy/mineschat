package fr.emse.rootz.mineschat.addon;

import fr.emse.rootz.mineschat.data.Message;

/**
 * Created by victor on 02/06/15.
 */
public interface Addon {
    boolean executeHook(AddonBase.HOOK_TYPE hookType, Object ... o);

    String getIdentifier();

    String getName();

    String getVersion();

    String getAuthor();
}
