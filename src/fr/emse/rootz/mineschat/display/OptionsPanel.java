package fr.emse.rootz.mineschat.display;

import fr.emse.rootz.mineschat.display.components.ColoredButton;
import fr.emse.rootz.mineschat.display.components.ColoredLabel;
import fr.emse.rootz.mineschat.display.components.ColoredPanel;
import fr.emse.rootz.mineschat.display.components.PhasePanel;
import fr.emse.rootz.mineschat.display.options.DisplaySettings;
import fr.emse.rootz.mineschat.display.options.MCStrings;
import fr.emse.rootz.mineschat.interfaces.ColorChangeable;
import fr.emse.rootz.mineschat.interfaces.DisplayInterface;

import javax.swing.*;
import java.awt.*;

import static fr.emse.rootz.mineschat.display.options.MCStrings.getString;

/**
 * Classe qui stocke les options du logiciel.
 * C'est principalement pour le choix des couleurs.
 */
public class OptionsPanel extends PhasePanel{

    private int buttonSize = 40;
    private ColoredButton customizedButton;

	public OptionsPanel(DisplayInterface phaseListener, Type type) {
		super(phaseListener, type);
	}

	@Override
	protected void constructGUI() {
		this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));

        // Titre du panel
		ColoredLabel phaseLabel = new ColoredLabel(getString(MCStrings.TextType.OPTIONS_LABEL), DisplaySettings.middleTitleFont);
		this.add(phaseLabel, BorderLayout.NORTH);

        // Panel central
		ColoredPanel centralPanel = new ColoredPanel();
		centralPanel.setLayout(new BoxLayout(centralPanel, BoxLayout.Y_AXIS));

        // Panel qui contient les couleurs
        ColoredPanel colorsPanel = new ColoredPanel();
        //colorsPanel.setLayout(new BoxLayout(colorsPanel, BoxLayout.X_AXIS));

        // Panel qui contient le sélectionneur de couleurs
        MCColorChooser colorChooser = new MCColorChooser();
        colorChooser.setColor(DisplaySettings.primaryColor);

        colorChooser.getSelectionModel().addChangeListener(e -> {
            Color selectedColor = colorChooser.getColor();
            colorSelected(DisplaySettings.toIntColor(selectedColor.getRed(), selectedColor.getBlue(), selectedColor.getGreen()));
        });

        colorsPanel.add(colorChooser);

		centralPanel.espacerDuReste(buttonSize);
		centralPanel.add(colorsPanel);

        // Un peu de texte d'explication
		ColoredLabel explicationLabel1 = new ColoredLabel(getString(MCStrings.TextType.OPTIONS_EXPLICATION_1), DisplaySettings.veryLittleTitleFont);
        ColoredLabel explicationLabel2 = new ColoredLabel(getString(MCStrings.TextType.OPTIONS_EXPLICATION_2), DisplaySettings.veryLittleTitleFont);
		ColoredPanel explicationsPanel = ColoredPanel.putInGroup('v', explicationLabel1, explicationLabel2);

		ColoredButton returnButton = new ColoredButton(getString(MCStrings.TextType.RETURN), DisplaySettings.returnImage, DisplaySettings.secondaryColor);
		returnButton.setMaximumSize(new Dimension(300, 100));
		returnButton.setAlignmentX(RIGHT_ALIGNMENT);
		returnButton.setFont(DisplaySettings.middleTitleFont);
		returnButton.espacerDuReste(20);

        ColoredPanel southPanel = ColoredPanel.putInGroup('h', explicationsPanel, returnButton);
        centralPanel.add(southPanel);
		
		returnButton.addActionListener(e -> phaseListener.goPreviousPhase());

		this.add(centralPanel);
	}
	
	public void colorSelected (int index) {
		DisplaySettings.changeColorSet(index);
	}

	@Override
	public void entering() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void exiting() {
		// TODO Auto-generated method stub
		
	}

    public class MCColorChooser extends JColorChooser implements ColorChangeable {

        private Color backgroundColor;
        private Color lineColor;
        private int lineThickness;

        public MCColorChooser() {
            super(Color.white);
            lineThickness = 10;
            updateColors();
            DisplaySettings.registerColorChangeable(this);
        }

        @Override
        public void updateColors () {
            backgroundColor = DisplaySettings.primaryColor;
            lineColor = DisplaySettings.secondaryColor;
            setBackground(DisplaySettings.primaryColor);
            setForeground(Color.white);
            ColoredPanel.recursivelyApplyColor(this, backgroundColor, Color.white);
        }

        @Override
        protected void paintComponent(Graphics g0) {
            super.paintComponent(g0);
            Graphics2D g = (Graphics2D) g0;
            // background
            g.setColor(backgroundColor);
            g.fillRect(0,0,getWidth(),getHeight());
            // line
            if(lineThickness>0) {
                g.setColor(lineColor);
                g.fillRect(0, 0, lineThickness, getHeight());
                g.fillRect(getWidth()-lineThickness, 0, getWidth(), getHeight());
                g.fillRect(0, 0, getWidth(), lineThickness);
                g.fillRect(0, getHeight()-lineThickness, getWidth(), getHeight());
            }

        }
    }

}
