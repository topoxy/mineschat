package fr.emse.rootz.mineschat.display;

import fr.emse.rootz.mineschat.display.components.*;
import fr.emse.rootz.mineschat.display.components.ColoredWaitDialog.WaitActions;
import fr.emse.rootz.mineschat.display.options.DisplaySettings;
import fr.emse.rootz.mineschat.display.options.MCStrings;
import fr.emse.rootz.mineschat.interfaces.DisplayInterface;
import fr.emse.rootz.mineschat.interfaces.NetworkConnectionListener;
import fr.emse.rootz.mineschat.network.MCNetworkClient;

import javax.swing.*;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;
import java.awt.*;

import static fr.emse.rootz.mineschat.display.options.MCStrings.getString;

public class ConnectionPanel extends PhasePanel implements NetworkConnectionListener {

	private ColoredButton discoveryButton;
	private ColoredButton ipConnectButton;
	private ColoredTextField ipField;
	private boolean waitForConnectionNews;

	public ConnectionPanel(DisplayInterface phaseListener, Type type) {
		super(phaseListener, type);
		prepareEvents();
		MCNetworkClient.registerConnectionListener(this);
	}

	@Override
	protected void constructGUI() {
		this.setLayout(new BorderLayout());

		ColoredPanel northPanel = new ColoredPanel();
		northPanel.setLayout(new BorderLayout());

		ColoredLabel serverPhaseLabel = new ColoredLabel(getString(MCStrings.TextType.CONNEXION_PHASE_LABEL), DisplaySettings.middleTitleFont);
		northPanel.add(serverPhaseLabel, BorderLayout.CENTER);

		ColoredButton returnButton = new ColoredButton(DisplaySettings.returnImage, new Color(0, 0, 0, 0));
		returnButton.addActionListener(e -> phaseListener.changePhase(Type.INTRO));
		returnButton.setOpaque(false);
		returnButton.espacerDuResteAvecBordure(5, 5, DisplaySettings.transparentWhite, 5);
		northPanel.add(returnButton, BorderLayout.WEST);

		this.add(northPanel, BorderLayout.NORTH);

		ColoredPanel centralPanel = new ColoredPanel();
		centralPanel.setLayout(new BoxLayout(centralPanel, BoxLayout.Y_AXIS));
		centralPanel.add(Box.createVerticalGlue());

		ColoredPanel discoveryPanel = new ColoredPanel();
		discoveryPanel.setBorder(new TitledBorder(new LineBorder(DisplaySettings.transparentWhite, 3), getString(MCStrings.TextType.AUTO_CONNEXION_BUTTON),
				TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, DisplaySettings.veryLittleTitleFont, Color.white));
		discoveryPanel.setFont(DisplaySettings.computerFont);

		discoveryButton = new ColoredButton(getString(MCStrings.TextType.AUTO_CONNEXION_LABEL),
				DisplaySettings.searchImage, DisplaySettings.secondaryColor);
		discoveryButton.setFont(DisplaySettings.computerFont);
		discoveryButton.setAlignmentX(Component.CENTER_ALIGNMENT);
		discoveryButton.setToolTipText(getString(MCStrings.TextType.DISCOVERY_BUTTON_LABEL));
		//discoveryButton.espacerDuResteAvecBordure(50, 0, Color.white, 3);
		centralPanel.add(discoveryButton, BorderLayout.WEST);

		discoveryPanel.add(discoveryButton);

		/*
		ColoredLabel serverAdvice = new ColoredLabel ("(Pour utilisateurs expérimentés)");
		serverAdvice.setForeground(Color.white);
		serverAdvice.setFont(DisplaySettings.textFont);
		serverAdvice.setAlignmentX(CENTER_ALIGNMENT);
		serverAdvice.setBackground(new Color (0, 0, 0, 0));*/

		centralPanel.add(discoveryPanel);

		centralPanel.add(Box.createVerticalGlue());

		ColoredPanel ipPanel = new ColoredPanel();
		//ipPanel.setBorder(new EmptyBorder(15, 5, 15, 5));
		ipPanel.setBorder(new TitledBorder(new LineBorder(DisplaySettings.transparentWhite, 2), getString(MCStrings.TextType.MANUAL_CONNEXION_LABEL),
				TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, DisplaySettings.veryLittleTitleFont, Color.white));

		/*
		ColoredLabel ipLabel = new ColoredLabel("Connexion manuelle par ip",
				SwingConstants.CENTER);
		ipLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
		ipLabel.setForeground(Color.white);
		ipLabel.setFont(DisplaySettings.veryLittleTitleFont);
		ipPanel.add(ipLabel);*/

		ipField = new ColoredTextField();
		ipField.setAlignmentX(Component.CENTER_ALIGNMENT);
		ipField.setPreferredSize(new Dimension(250, 60));
		ipField.setFont(DisplaySettings.computerFont);
		ipField.addActionListener(e -> ipConnectButton.doClick());
		ipPanel.add(ipField);

		ipConnectButton = new ColoredButton(getString(MCStrings.TextType.MANUAL_CONNEXION_BUTTON), 5);
		ipConnectButton.setAlignmentX(Component.CENTER_ALIGNMENT);
		ipConnectButton.setFont(DisplaySettings.computerFont);
		//ipConnectButton.setPreferredSize(new Dimension(600, 60));
		ipConnectButton.espacerDuReste(55, 5);
		//ipConnectButton.setOpaque(false);
		ipConnectButton.setToolTipText(getString(MCStrings.TextType.MANUAL_CONNEXION_BUTTON_TOOLTIP));
		ipPanel.add(ipConnectButton);

		centralPanel.add(ipPanel);
		centralPanel.add(Box.createVerticalGlue());

		this.add(centralPanel, BorderLayout.CENTER);
	}
	
	private void prepareEvents() {
		discoveryButton.addActionListener(e -> ColoredWaitDialog.doActionWithWaiting(phaseListener.getWindow(), new WaitActions(true) {
            @Override
            public void finishAction(boolean result) {
                if(result) {
                    new ColoredInfoDialog(phaseListener.getWindow(), DisplaySettings.okImage, getString(MCStrings.TextType.SERVER_FOUND)+MCNetworkClient.getAdresseIPServer());
                    startServerConnection();
                }
                else {
                    new ColoredInfoDialog(phaseListener.getWindow(), DisplaySettings.nopeImage, getString(MCStrings.TextType.CONNECTION_FAILED));
                }
            }

            @Override
            public boolean backGroundAction() {
                return MCNetworkClient.searchOnLAN();
            }
        }, getString(MCStrings.TextType.CONNECTING)));

		ipConnectButton.addActionListener(arg0 -> {
            if(!ipField.getText().isEmpty()) {
                if (!MCNetworkClient.setConnectionAddress(ipField.getText().trim())) {
                    new ColoredInfoDialog(phaseListener.getWindow(), DisplaySettings.quitImage, getString(MCStrings.TextType.IP_INVALID));
                }
                else {
                    startServerConnection();
                }
            }
        });
	}
	
	private void startServerConnection () {
		if(!MCNetworkClient.isConnected())
			ColoredWaitDialog.doActionWithWaiting(phaseListener.getWindow(), new WaitActions(true) {
				@Override
				public void finishAction(boolean result) {
					if(result) {
						new ColoredInfoDialog(phaseListener.getWindow(), DisplaySettings.okImage, getString(MCStrings.TextType.CONNEXION_SUCCEDED));
						phaseListener.changePhase(Type.DISCUSSION);
					}
					else {
						new ColoredInfoDialog(phaseListener.getWindow(), DisplaySettings.nopeImage, getString(MCStrings.TextType.CONNECTION_FAILED));
					}
				}
				
				@Override
				public boolean backGroundAction() {
					new Thread(MCNetworkClient::connectToServer).start();
					
					waitForConnectionNews = true;
					while(waitForConnectionNews) {
						try {
							Thread.sleep(300);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
					return MCNetworkClient.isConnected();
				}
			}, getString(MCStrings.TextType.CONNECTING));
		else {
			new ColoredInfoDialog(phaseListener.getWindow(), DisplaySettings.okImage, getString(MCStrings.TextType.CONNEXION_SUCCEDED));
			phaseListener.changePhase(Type.DISCUSSION);
		}
	}

	@Override
	public void entering() {
	}

	@Override
	public void exiting() {
	}

	@Override
	public void connectionEstablished(boolean success) {
		waitForConnectionNews = false;
	}
}
