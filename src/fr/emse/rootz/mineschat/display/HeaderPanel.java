package fr.emse.rootz.mineschat.display;

import fr.emse.rootz.mineschat.display.components.ColoredButton;
import fr.emse.rootz.mineschat.display.components.ColoredLabel;
import fr.emse.rootz.mineschat.display.components.ColoredPanel;
import fr.emse.rootz.mineschat.display.components.PhasePanel;
import fr.emse.rootz.mineschat.display.options.DisplaySettings;
import fr.emse.rootz.mineschat.display.options.MCStrings;
import fr.emse.rootz.mineschat.interfaces.DisplayInterface;
import fr.emse.rootz.mineschat.network.MCNetworkClient;
import fr.emse.rootz.mineschat.network.MCNetworkServer;

import javax.swing.*;
import java.awt.*;

import static fr.emse.rootz.mineschat.display.options.MCStrings.getString;

public class HeaderPanel extends PhasePanel {

	private ColoredButton soundButton;

	private ColoredLabel titleLabel;

	public HeaderPanel(DisplayInterface phaseListener, Type type) {
		super(phaseListener, type);
	}

	protected void constructGUI() {
		BorderLayout layout = new BorderLayout();
		this.setLayout(layout);
		layout.setHgap(DisplaySettings.HEADER_LAYOUT_GAP);
		layout.setVgap(DisplaySettings.HEADER_LAYOUT_GAP);

		ColoredPanel optionsPanel = new ColoredPanel();
		optionsPanel.setLayout(new BoxLayout(optionsPanel, BoxLayout.X_AXIS));

		ColoredButton optionsButton = new ColoredButton(DisplaySettings.optionsImage, new Color(0, 0, 0, 0));
		optionsButton.espacerDuResteAvecBordure(15, DisplaySettings.transparentWhite, 5);
		optionsButton.setOpaque(false);
		optionsButton.addActionListener(arg0 -> phaseListener.changePhaseDiscretly(Type.OPTIONS));
		optionsButton.setToolTipText(getString(MCStrings.TextType.OPTIONS_BUTTON_TOOLTIP));
		optionsPanel.add(optionsButton);
		optionsPanel.add(Box.createHorizontalStrut(5));
		
		titleLabel = new ColoredLabel(getString(MCStrings.TextType.TITLE_LABEL), DisplaySettings.titleFont);

		soundButton = new ColoredButton(DisplaySettings.soundOnImage, new Color(0, 0, 0, 0));
		soundButton.espacerDuResteAvecBordure(15, DisplaySettings.transparentWhite, 5);
		soundButton.setOpaque(false);
		soundButton.addActionListener(arg0 -> {
            DisplaySettings.toggleSound();
            soundButton.setIcon(new ImageIcon(!DisplaySettings.isSoundOn() ? DisplaySettings.soundOffImage : DisplaySettings.soundOnImage ));
        });
		soundButton.setToolTipText(getString(MCStrings.TextType.SOUND_BUTTON_TOOLTIP));
		optionsPanel.add(soundButton);

		ColoredPanel buttonsPanel = new ColoredPanel();
		buttonsPanel.setLayout(new BoxLayout(buttonsPanel, BoxLayout.X_AXIS));
		
		ColoredPanel miniButtonPanel = new ColoredPanel();
		miniButtonPanel.setLayout(new BoxLayout(miniButtonPanel, BoxLayout.Y_AXIS));
		miniButtonPanel.add(Box.createHorizontalStrut(2));

		ColoredButton minimizeButton = new ColoredButton(DisplaySettings.minimizeImage, new Color(0, 0, 0, 0));
		minimizeButton.addActionListener(event -> phaseListener.minimize());
		minimizeButton.espacerDuResteAvecBordure(15, 3, DisplaySettings.transparentWhite, 2);
		minimizeButton.setOpaque(false);
		minimizeButton.setToolTipText(getString(MCStrings.TextType.MINIMIZE_BUTTON_TOOLTIP));
		miniButtonPanel.add(minimizeButton);
		miniButtonPanel.add(Box.createHorizontalStrut(1));

		ColoredButton maximizeButton = new ColoredButton(DisplaySettings.maximizeImage, new Color(0, 0, 0, 0));
		maximizeButton.addActionListener(event -> phaseListener.maximize());
		maximizeButton.espacerDuResteAvecBordure(15, 3, DisplaySettings.transparentWhite, 2);
		maximizeButton.setOpaque(false);
		maximizeButton.setToolTipText(getString(MCStrings.TextType.MAXIMIZE_BUTTON_TOOLTIP));
		miniButtonPanel.add(maximizeButton);
		miniButtonPanel.add(Box.createHorizontalStrut(1));

		ColoredButton reduceButton = new ColoredButton(DisplaySettings.reduceImage, new Color(0, 0, 0, 0));
		reduceButton.addActionListener(event -> phaseListener.reduce());
		reduceButton.espacerDuResteAvecBordure(15, 3, DisplaySettings.transparentWhite, 2);
		reduceButton.setOpaque(false);
		reduceButton.setToolTipText(getString(MCStrings.TextType.REDUCE_BUTTON_TOOLTIP));
		miniButtonPanel.add(reduceButton);
		miniButtonPanel.add(Box.createHorizontalStrut(2));
		
		buttonsPanel.add(miniButtonPanel);
		
		buttonsPanel.add(Box.createHorizontalStrut(5));

		ColoredButton exitButton = new ColoredButton(DisplaySettings.quitImage, new Color(0, 0, 0, 0));
		exitButton.addActionListener(event -> {
			MCNetworkServer.programIsClosing();
			MCNetworkClient.programIsClosing();
			System.exit(0);
		});
		exitButton.espacerDuResteAvecBordure(15, DisplaySettings.transparentWhite, 5);
		exitButton.setOpaque(false);
		exitButton.setToolTipText(getString(MCStrings.TextType.EXIT_BUTTON_TOOLTIP));
		buttonsPanel.add(exitButton);
		
		this.add(optionsPanel, BorderLayout.WEST);
		this.add(titleLabel, BorderLayout.CENTER);
		this.add(buttonsPanel, BorderLayout.EAST);
	}

	@Override
	public void entering() {
	}

	@Override
	public void exiting() {
	}

	public void changeTitle(String title) {
		titleLabel.setText(title);
	}
}
