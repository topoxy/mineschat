package fr.emse.rootz.mineschat.display;

import fr.emse.rootz.mineschat.addon.AddonBase;
import fr.emse.rootz.mineschat.controls.ControlsInterface;
import fr.emse.rootz.mineschat.data.*;
import fr.emse.rootz.mineschat.display.components.*;
import fr.emse.rootz.mineschat.display.options.DisplaySettings;
import fr.emse.rootz.mineschat.display.options.MCStrings;
import fr.emse.rootz.mineschat.interfaces.DiscussionInterface;
import fr.emse.rootz.mineschat.interfaces.DisplayInterface;
import fr.emse.rootz.mineschat.interfaces.NetworkManagerInterface;
import fr.emse.rootz.mineschat.interfaces.UserActionListener;

import javax.swing.*;
import javax.swing.border.LineBorder;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;
import java.util.stream.Collectors;

import static fr.emse.rootz.mineschat.display.options.MCStrings.getString;

/**
 *	Classe qui se charge d'afficher les messages du chat
 */
public class DiscussionPanel extends PhasePanel implements DiscussionInterface, UserActionListener {

    // TABS STUFF
	protected JTabbedPane tabs;
	protected HashMap<Integer, ColoredTextViewAndList> discussionPanes = new HashMap<>();
	protected HashMap<Integer, Integer> discussionIndex = new HashMap<>();
	protected int indexTabToRemove;

	// MENU STUFF
	protected HashMap<Integer, JMenuItem> discussionItems = new HashMap<>();
	protected JPopupMenu popupPeopleList;
	protected JPopupMenu popupTab;
	protected JMenu inviteMenu;
	protected JMenuItem sendFileMenu;

	// DISCUSSION STUFF
	protected ColoredLabel userLabel;
	protected JTextField textField;
	protected ColoredButton artButton;
	protected ColoredArtDialog artDialog;
	protected int selectedDiscussion = 0;

	// INTERFACES STUFF
	protected NetworkManagerInterface networkManager;
	protected AddonBase addonBase;
	protected ControlsInterface controlsInterface;
	protected ColoredPanel southPanel;
	// GUI STUFF
    private ArrayList<JComponent> componentsToUpdate;

	public DiscussionPanel(DisplayInterface phaseListener, Type type) {
		super(phaseListener, type);
	}

    /**
     * Construis toute l'interface
     */
	@Override
	protected void constructGUI() {
		this.setLayout(new BorderLayout(10,10));
        componentsToUpdate = new ArrayList<>();
		
		tabs = new JTabbedPane();
		tabs.setBackground(DisplaySettings.secondaryColor);
		tabs.setForeground(Color.white);
		tabs.setBorder(new LineBorder(DisplaySettings.transparentWhite, 6, false));
		tabs.setFont(DisplaySettings.veryLittleTitleFont);
		tabs.setFocusable(false);
		tabs.addChangeListener(e -> {
            if(tabs.getSelectedComponent() !=null) {
                selectedDiscussion = ((ColoredTextViewAndList)tabs.getSelectedComponent()).idDiscussion;
                signalTab(selectedDiscussion, false);
            }
        });
		this.add(tabs, BorderLayout.CENTER);
		
		southPanel = new ColoredPanel();
		southPanel.setLayout(new BorderLayout());
		
		userLabel = new ColoredLabel(DisplaySettings.secondaryColor);
		userLabel.setFont(DisplaySettings.littleTitleFont);
		southPanel.add(userLabel, BorderLayout.WEST);
		
		artDialog = new ColoredArtDialog(phaseListener.getWindow());
		artDialog.registerActionForInsert(e -> {
            DataArtGrid data = new DataArtGrid(artDialog.getArtGrid().copy(), DataCollection.getUser().getId(), selectedDiscussion);
            networkManager.sendObject(data);
        });
		
		artButton = new ColoredButton(ColorMode.INVERSED, DisplaySettings.paintBrushImage);
		artButton.addActionListener(e -> artDialog.setVisible(true));
		artButton.setToolTipText(getString(MCStrings.TextType.ART_BUTTON_TOOLTIP));
		southPanel.add(artButton, BorderLayout.EAST);

		this.add(southPanel, BorderLayout.SOUTH);
		
		constructPopupMenu();
	}

	@Override
	public void updateColors() {
		super.updateColors();
        if(componentsToUpdate != null)
        componentsToUpdate.stream().filter(component -> component != null).forEach(component -> customizeComponent(component, false));
	}


    /**
     * Petite routine qui permet rapidement d'appliquer les couleurs standard à un composant de l'interface
     * @param comp
     * @param add
     */
	private void customizeComponent (JComponent comp, boolean add) {
	    comp.setBackground(DisplaySettings.primaryColor);
	    comp.setBorder(new LineBorder(DisplaySettings.secondaryColor));
	    comp.setForeground(DisplaySettings.textColor);
	    comp.setFont(DisplaySettings.textFont);
		if(add && componentsToUpdate != null)
			componentsToUpdate.add(comp);
	}

    /**
     * Permet de signaler à un onglet qu'un nouveau message lui est parvenu.
     * @param idDiscussion
     * @param bool
     */
	private void signalTab (final int idDiscussion, final boolean bool) {
		SwingUtilities.invokeLater(() -> {
            if(discussionIndex.containsKey(idDiscussion) && discussionPanes.containsKey(idDiscussion)) {
                if(bool) {
                    tabs.setBackgroundAt(discussionIndex.get(idDiscussion), DisplaySettings.transparentWhite);
                    tabs.setForegroundAt(discussionIndex.get(idDiscussion), DisplaySettings.secondaryColor);
                }
                else {
                    tabs.setBackgroundAt(discussionIndex.get(idDiscussion), DisplaySettings.secondaryColor);
                    tabs.setForegroundAt(discussionIndex.get(idDiscussion), Color.white);
                }
            }
        });
	}

    /**
     * Procédure pour créer une nouvelle discussion.
     * On demande un nom de discussion à l'utilisateur.
     * Si des gens ont été sélectionnés sur le panel de droite, ils seront rajoutés à la discussion.
     * Puis la demande est envoyée au serveur.
     */
	private void createNewDiscussion () {
		String discussionName = "";
		while(discussionName.isEmpty()) {
			discussionName = new ColoredInputDialog(phaseListener.getWindow(), getString(MCStrings.TextType.DISCUSSION_NAME_DIALOG)).getInput();
			if(discussionName.isEmpty()) {
				new ColoredInfoDialog(phaseListener.getWindow(), DisplaySettings.quitImage, getString(MCStrings.TextType.DISCUSSION_NAME_DIALOG_EMPTY_WARNING));
			}
		}
		ArrayList<Integer> peopleIds = new ArrayList<>();
		peopleIds.add(DataCollection.getUser().getId());
		peopleIds.addAll(getActualTab().clientList.getSelectedPeople().stream().filter(person -> person.getId() != DataCollection.getUser().getId()).map(Person::getId).collect(Collectors.toList()));
		
		Discussion newDiscussion = new Discussion(new Random().nextInt(), discussionName, peopleIds, true, false);
		networkManager.sendObject(newDiscussion);
	}
	
	private void prepareSendingFiles () {
		ArrayList<Person> receivers = new ArrayList<>(((ColoredTextViewAndList)tabs.getSelectedComponent()).clientList.getSelectedPeople());
		networkManager.sendDataTransfertInvitation(receivers);
	}

    /**
     * Construis le menu qui apparaît avec le clic droit
     */
	private void constructPopupMenu () {
	    popupPeopleList = new JPopupMenu();
	    inviteMenu = new JMenu(getString(MCStrings.TextType.MENU_ITEM_INVITE));
	    JMenuItem newDiscussion = new JMenuItem(getString(MCStrings.TextType.MENU_ITEM_NEW_DISCUSSION));

		customizeComponent(popupPeopleList, true);
		customizeComponent(inviteMenu, true);
		customizeComponent(newDiscussion, true);
		
	    popupPeopleList.add(inviteMenu);
	    inviteMenu.add(newDiscussion);
	    
	    newDiscussion.addActionListener(e -> createNewDiscussion());
	    
	    sendFileMenu = new JMenuItem(getString(MCStrings.TextType.MENU_ITEM_SEND_FILE));
	    customizeComponent(sendFileMenu, true);
	    popupPeopleList.add(sendFileMenu);
	    
	    sendFileMenu.addActionListener(e -> prepareSendingFiles());
	    
	    popupTab = new JPopupMenu();
	    JMenuItem closeTabItem = new JMenuItem(getString(MCStrings.TextType.MENU_ITEM_QUIT_DISCUSSION));

		customizeComponent(popupTab, true);
		customizeComponent(closeTabItem, true);
		
		popupTab.add(closeTabItem);
		
		closeTabItem.addActionListener(e -> quitDiscussion(((ColoredTextViewAndList)tabs.getComponentAt(indexTabToRemove)).idDiscussion));
	}

    /**
     * Ajoute un nouvel onglet de discussion.
     * @param discussion
     */
	public void addDiscussionPane (final Discussion discussion) {
		ColoredTextPane textPane = new ColoredTextPane ();
		ColoredClientList clientList = new ColoredClientList(this);
		
		clientList.setPeopleAccordingDiscussion(discussion);
	    
		clientList.addPopupMouseListener(new PopupListenerPeopleList() {
			@Override
			public int getNumberSelectedElement() {
				return getActualTab().clientList.getSelectedPeople().size();
			}
		});
		
		ColoredTextViewAndList panel = new ColoredTextViewAndList(textPane, clientList, discussion.getId());
		discussionPanes.put(discussion.getId(), panel);
		tabs.addTab(discussion.getName(), panel);
		SwingUtilities.invokeLater(() -> discussionIndex.put(discussion.getId(), tabs.indexOfComponent(discussionPanes.get(discussion.getId()))));
		
		if(discussion.getId() != 0) {
			JMenuItem discussionItem = new JMenuItem(discussion.getName());
			inviteMenu.insert(discussionItem, 0);
			discussionItems.put(discussion.getId(), discussionItem);
			
			discussionItem.addActionListener(e -> {
                ArrayList<Integer> peopleIds = new ArrayList<>();
                peopleIds.add(DataCollection.getUser().getId());
				peopleIds.addAll(getActualTab().
						clientList.getSelectedPeople().stream().
						filter(person -> person.getId() != DataCollection.getUser().getId()).map(Person::getId).collect(Collectors.toList()));
                DiscussionInvitation discussionInvitation = new DiscussionInvitation(discussion.getId(), peopleIds);
                networkManager.sendObject(discussionInvitation);
            });
			
			tabs.addMouseListener(new MouseAdapter() {
				public void checkTabs(MouseEvent e) {
					for(int i = 1;i<tabs.getTabCount();i++){
						if(tabs.getBoundsAt(i).contains(e.getPoint())){
							indexTabToRemove = i;
							if (e.isPopupTrigger()) {
								popupTab.show(e.getComponent(),
										e.getX(), e.getY());
							}
							System.out.println("");
							return;
						}
					}
				}
				public void mouseReleased(MouseEvent e) {
					checkTabs(e);
				}
				public void mousePressed(MouseEvent e) {
					checkTabs(e);
				}
			});
			
			//final int index = tabs.indexOfTab(discussion.getName());
			/*
			tabs.getTabComponentAt(index).addMouseListener(new PopupListenerTab() {
				@Override
				public int getIndexTabToRemove() {
					return index;
				}
			});*/
			
		}
		
	}

    /**
     * Affiche un message sur l'écrit
     * @param message message publié
     */
	public void publishMessage (Message message) {
		discussionPanes.get(message.getIdDiscussion()).textPane.publishMessage(message);
		if(selectedDiscussion != message.getIdDiscussion())
			signalTab(message.getIdDiscussion(), true);
		EventQueue.invokeLater(this::repaint);
	}

    /**
     * Affiche un ArtGrid sur l'écran
     * @param dataArtGrid
     */
	public void publishArt (DataArtGrid dataArtGrid) {
		discussionPanes.get(dataArtGrid.getIdDiscussion()).textPane.publishArtGrid(dataArtGrid);
	}

	@Override
	public void entering() {
		networkManager.startDialog();
		
		userLabel.setText(" "+DataCollection.getUser().getPseudonyme()+" : ");
        textField.requestFocusInWindow();
	}

	@Override
	public void exiting() {
		artDialog.dispose();
	}

	public void updatePeopleList (int idDiscussion) {
		if(DataCollection.getDiscussions().containsKey(idDiscussion))
			discussionPanes.get(idDiscussion).clientList.setPeopleAccordingDiscussion(DataCollection.getDiscussions().get(idDiscussion));
	}
	
	public void updateAllPeopleList () {
		discussionPanes.keySet().forEach(this::updatePeopleList);
	}

	private void quitDiscussion (int idDiscussion) {
		tabs.remove(discussionPanes.get(idDiscussion));
		inviteMenu.remove(discussionItems.get(idDiscussion));
		networkManager.sendObject(new DiscussionQuit(DataCollection.getUser().getId(), idDiscussion));
		DataCollection.removeDiscussion(idDiscussion);
	}
	
	public void clear() {
		discussionItems.clear();
		discussionPanes.clear();
		tabs.removeAll();
		userLabel.setText("");
	}

    @Override
    public int getSelectedDiscussion() {
        return selectedDiscussion;
    }

	protected ColoredTextViewAndList getActualTab () {
		return (ColoredTextViewAndList)tabs.getSelectedComponent();
	}

	public boolean isArtDialogActivated () {
		return artDialog.isActivated();
	}

	public void setClientListsVisible(boolean bool) {
		for (ColoredTextViewAndList panels : discussionPanes.values()) {
			panels.setClientListVisible(bool);
		}
	}

	@Override
	public void discussionButtonPressed() {
		createNewDiscussion();
	}
	
	@Override
	public void sendFileButtonPressed() {
		ArrayList<Person> receivers = new ArrayList<>(((ColoredTextViewAndList)tabs.getSelectedComponent()).clientList.getSelectedPeople());
		networkManager.sendDataTransfertInvitation(receivers);
	}

	@Override
	public void screenSharingButtonPressed() {
		networkManager.startScreenSharing(selectedDiscussion);
	}

	@Override
	public void audioConferenceButtonPressed() {
		networkManager.startAudioConference(selectedDiscussion);
	}

	public void setNetworkManager (NetworkManagerInterface networkManager) {
		this.networkManager = networkManager;
	}

	public void setControlsInterface (ControlsInterface controlsInterface) {
		this.controlsInterface = controlsInterface;

		textField = controlsInterface.generateConsole();
		southPanel.add(textField, BorderLayout.CENTER);
	}

	public void setAddonBase(AddonBase addonBase) {
		this.addonBase = addonBase;
	}

	private abstract class PopupListenerPeopleList extends MouseAdapter {
	    public void mousePressed(MouseEvent e) {
	        maybeShowPopup(e);
	    }

	    public void mouseReleased(MouseEvent e) {
	        maybeShowPopup(e);
	    }

	    private void maybeShowPopup(MouseEvent e) {
	        if (e.isPopupTrigger() && getNumberSelectedElement()>0) {
	            popupPeopleList.show(e.getComponent(),
	                       e.getX(), e.getY());
	        }
	    }

	    public abstract int getNumberSelectedElement();
	}
}
