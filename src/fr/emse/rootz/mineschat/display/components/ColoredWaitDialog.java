package fr.emse.rootz.mineschat.display.components;

import fr.emse.rootz.mineschat.display.options.DisplaySettings;

import javax.swing.*;
import java.awt.*;

public class ColoredWaitDialog extends JDialog {

	protected ColoredPanel panel;
	protected JLabel dialogTitleLabel;
	protected JLabel processLabel;
	protected ColoredButton exitButton;
	protected boolean processusValide;
	
	public ColoredWaitDialog(JFrame parent, boolean canBeCanceled) {
		super(parent);
		
		constructGUI(canBeCanceled);
		this.setUndecorated(true);
		//this.setOpacity(0.98f);
		this.setModal(true);
		this.setSize(650, 300);
		this.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		this.setLocationRelativeTo(null);
	}
	
	public static void doActionWithWaiting (final JFrame parent, final WaitActions actions, String message) {
		SwingWorker<Void, Void> mySwingWorker = new SwingWorker<Void, Void>() {
			@Override
			protected Void doInBackground() throws Exception {
				actions.setResult(actions.backGroundAction());
				return null;
			}
		};

		final ColoredWaitDialog waitingDialog = new ColoredWaitDialog(parent, actions.canBeCanceled);

		mySwingWorker.addPropertyChangeListener(evt -> {
           if (evt.getPropertyName().equals("state")) {
              if (evt.getNewValue() == SwingWorker.StateValue.DONE) {
                  boolean processusValide1;
                      processusValide1 = waitingDialog.processusFini();
                      if(actions.isCanBeCanceled()&& processusValide1) {
                          actions.finishAction(actions.getResult());
                      }
                      else if (!actions.isCanBeCanceled()) {
                          actions.finishAction(actions.getResult());
                      }

              }
           }
        });
	      mySwingWorker.execute();

		waitingDialog.fairePatienter(message);
	}
	
	protected void constructGUI (boolean canBeCanceled) {
		panel = new ColoredPanel(ColoredPanel.ColorMode.INVERSED_WITH_BORDERS, 10);
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));

		panel.add(Box.createVerticalGlue());

		ColoredPanel northPanel = new ColoredPanel(ColoredPanel.ColorMode.INVERSED);
		northPanel.setLayout(new BoxLayout(northPanel, BoxLayout.X_AXIS));

		dialogTitleLabel = new JLabel("Veuillez patienter...");
		dialogTitleLabel.setForeground(Color.white);
		dialogTitleLabel.setFont(DisplaySettings.middleTitleFont);
		dialogTitleLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
		northPanel.add(dialogTitleLabel);

		if(canBeCanceled) {
			northPanel.add(Box.createHorizontalStrut(40));
			exitButton = new ColoredButton("Annuler", DisplaySettings.quitImage, DisplaySettings.quitColor);
			exitButton.setFont(DisplaySettings.textFont);
			exitButton.addActionListener(e -> {
                processusValide = false;
                dispose();
            });
			northPanel.add(exitButton);
		}

		panel.add(northPanel);
		panel.add(Box.createVerticalGlue());

		JLabel waitingImage = new JLabel (new ImageIcon (DisplaySettings.loadingImage));
		waitingImage.setAlignmentX(Component.CENTER_ALIGNMENT);
		panel.add(waitingImage);
		panel.add(Box.createVerticalGlue());

		processLabel = new JLabel("Recherche de serveurs en cours...");
		processLabel.setForeground(Color.white);
		processLabel.setFont(DisplaySettings.littleTitleFont);
		processLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
		panel.add(processLabel);
		panel.add(Box.createVerticalGlue());

		this.setContentPane(panel);
	}
	
	public void fairePatienter (String message) {
		processusValide = true;
		processLabel.setText(message);
		this.setVisible(true);
	}
	
	public boolean processusFini () {
		dispose();
		return processusValide;
	}
	
	public static abstract class WaitActions {
		private boolean canBeCanceled;
		private boolean result;

		public WaitActions (boolean canBeCanceled) {
			this.canBeCanceled = canBeCanceled;
		}

		public abstract boolean backGroundAction();
		
		public abstract void finishAction(boolean result);
		
		public boolean isCanBeCanceled () {
			return canBeCanceled;
		}
		
		public boolean getResult() {
			return this.result;
		}
		
		public void setResult(boolean res) {
			this.result = res;
		}
		
		
	}

}
