package fr.emse.rootz.mineschat.display.components;

import fr.emse.rootz.mineschat.data.DataCollection;
import fr.emse.rootz.mineschat.data.Discussion;
import fr.emse.rootz.mineschat.data.Person;
import fr.emse.rootz.mineschat.display.options.DisplaySettings;
import fr.emse.rootz.mineschat.interfaces.UserActionListener;

import javax.swing.*;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import java.awt.*;
import java.awt.event.MouseListener;
import java.util.List;

public class ColoredClientList extends ColoredPanel {

	protected JList<Person> peopleList;
	protected DefaultListModel<Person> listModel;
	protected ColoredLabel connectedPeopleLabel;
	protected String discussionName;
	protected ColoredPanel buttonsList;
	protected ColoredButton discussionButton;
	protected ColoredButton sendFileButton;
	protected ColoredButton screenSharingButton;
	protected ColoredButton audioConferenceButton;
    protected JScrollPane scrollPane;

	protected UserActionListener actionListener;
	
	public ColoredClientList(UserActionListener actionListener) {
		super(6);
		this.actionListener = actionListener;
		constructGUI();
	}

    @Override
    public void updateColors() {
        super.updateColors();
        if(scrollPane != null) {
            scrollPane.getVerticalScrollBar().setBackground(DisplaySettings.secondaryColor);
            scrollPane.getHorizontalScrollBar().setBackground(DisplaySettings.secondaryColor);
            peopleList.setBackground(DisplaySettings.primaryColor);
			scrollPane.setBorder(new LineBorder(DisplaySettings.secondaryColor, 6));
        }
    }

    protected void constructGUI() {
		constructButtonList();

		scrollPane = new JScrollPane();
		scrollPane.setViewportView(peopleList);
		scrollPane.setBorder(new LineBorder(DisplaySettings.secondaryColor, 6));

		this.setLayout(new BorderLayout());
		this.add(scrollPane, BorderLayout.CENTER);
		JScrollBar sb = scrollPane.getVerticalScrollBar();
		sb.setBackground(DisplaySettings.secondaryColor);
		sb.setUI(new ColoredScrollBar());
		sb = scrollPane.getHorizontalScrollBar();
		sb.setBackground(DisplaySettings.secondaryColor);
		sb.setUI(new ColoredScrollBar());
		this.setPreferredSize(new Dimension(300, 10));

		connectedPeopleLabel = new ColoredLabel("Connectés", DisplaySettings.veryLittleTitleFont);
		connectedPeopleLabel.setBorder(new EmptyBorder(15, 5, 5, 5));
		this.add(connectedPeopleLabel, BorderLayout.NORTH);
		
		buttonsList = new ColoredPanel();
		//buttonsList.setLayout(new BoxLayout(buttonsList, BoxLayout.X_AXIS));

		discussionButton = new ColoredButton(DisplaySettings.discussionImage, DisplaySettings.secondaryColor);
		discussionButton.addActionListener(e -> actionListener.discussionButtonPressed());
		discussionButton.setToolTipText("<html><div style=\"text-align: center;\">"
				+ "Lance une conversation privée avec les gens sélectionnés<br/>"
				+ "(utilise CTRL + Clics pour en sélectionner plusieurs)"
				+ "</html>");
		buttonsList.add(discussionButton);
		
		sendFileButton = new ColoredButton(DisplaySettings.receiveLittleImage, DisplaySettings.secondaryColor);
		sendFileButton.addActionListener(e -> actionListener.sendFileButtonPressed());
		sendFileButton.setToolTipText("<html><div style=\"text-align: center;\">"
				+ "Envoie un fichier à la personne selectionnée"
				+ "</html>");
		buttonsList.add(sendFileButton);
		
		screenSharingButton = new ColoredButton(DisplaySettings.screenSharingImage, DisplaySettings.secondaryColor);
		screenSharingButton.addActionListener(e -> {
            System.out.println("BAM!");
            actionListener.screenSharingButtonPressed();
        });
		screenSharingButton.setToolTipText("<html><div style=\"text-align: center;\">"
				+ "Démarre/rejoint un partage d'écran"
				+ "</html>");
		buttonsList.add(screenSharingButton);
		
		audioConferenceButton = new ColoredButton(DisplaySettings.audioConferenceImage, DisplaySettings.secondaryColor);
		audioConferenceButton.addActionListener(e -> actionListener.audioConferenceButtonPressed());
		audioConferenceButton.setToolTipText("<html><div style=\"text-align: center;\">"
				+ "Démarre/rejoint une conférence audio"
				+ "</html>");
		buttonsList.add(audioConferenceButton);

		this.add(buttonsList, BorderLayout.SOUTH);
	}

	private void constructButtonList() {
		peopleList = new JList<>();
		peopleList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		peopleList.setSelectionModel(new CustomListSelectionModel());
		peopleList.setLayoutOrientation(JList.VERTICAL);
		peopleList.setDragEnabled(true);
		peopleList.setBackground(DisplaySettings.primaryColor);
		// peopleList.setBorder(new EmptyBorder(5,5,5,5));

		listModel = new DefaultListModel<>();
		peopleList.setModel(listModel);
		peopleList.setCellRenderer(new MyCellRenderer());
	}

	public void setPeopleAccordingDiscussion(final Discussion discussion) {
		discussionName = discussion.getName();
		SwingUtilities.invokeLater(() -> {
            connectedPeopleLabel
            .setText("<html><center><b>Connectés</b> <br/><font size=\"7\">"
					+ discussionName
					+ "</font></center></html>");
            listModel.clear();
            for (int id_person : discussion.getPeople()) {
                addPerson(DataCollection.getPerson(id_person));
            }
        });
	}

	public void addPopupMouseListener(MouseListener listener) {
		peopleList.addMouseListener(listener);
	}

	public void addPerson(Person person) {
		listModel.addElement(person);
	}

	public void removePerson(Person person) {
		listModel.removeElement(person);
	}

	public List<Person> getSelectedPeople() {
		return peopleList.getSelectedValuesList();
	}

	class MyCellRenderer extends JLabel implements ListCellRenderer<Person> {
		public MyCellRenderer() {
			setOpaque(true);
		}

		public Component getListCellRendererComponent(
				JList<? extends Person> list, Person value, int index,
				boolean isSelected, boolean cellHasFocus) {

			this.setText("<html><b>" + value.getPseudonyme()
					+ "</b> <font size=\"3\">(" + value.getIp()
					+ ")</font></html>");
			this.setFont(DisplaySettings.textFont);
			this.setBorder(new CompoundBorder(new LineBorder(
					DisplaySettings.transparentWhite, 1), new EmptyBorder(5, 5,
					5, 5)));

			Color background;
			Color foreground;
			Color backColor = DisplaySettings.toSecondaryColor(value.getColorIndex());

			// check if this cell represents the current DnD drop location
			JList.DropLocation dropLocation = list.getDropLocation();
			if (dropLocation != null && !dropLocation.isInsert()
					&& dropLocation.getIndex() == index) {

				background = DisplaySettings.quitColor;
				foreground = Color.white;

				// check if this cell is selected
			} else if (isSelected) {
				background = DisplaySettings.transparentWhite;
				foreground = backColor;

				// unselected, and not the DnD drop location
			} else {
				background = backColor;
				foreground = Color.white;
			}

			setBackground(background);
			setForeground(foreground);
			return this;
		}
	}

	class CustomListSelectionModel extends DefaultListSelectionModel {

		/*
		boolean gestureStarted = false;

	    @Override
	    public void setSelectionInterval(int index0, int index1) {
	        if(!gestureStarted){
	            if (isSelectedIndex(index0)) {
	                super.removeSelectionInterval(index0, index1);
	            } else {
	                super.addSelectionInterval(index0, index1);
	            }
	        }
	        gestureStarted = true;
	    }

	    @Override
	    public void setValueIsAdjusting(boolean isAdjusting) {
	        if (isAdjusting == false) {
	            gestureStarted = false;
	        }
	    }*/
	    
		//*
		@Override
		public void setSelectionInterval(int index0, int index1) {
			/*
			if (isSelectedIndex(index0)) {
				super.removeSelectionInterval(index0, index1);
			} else {
				super.setSelectionInterval(index0, index1);
			}*/
			if(index0==index1)
				super.setSelectionInterval(index0, index1);
		}
		//*/

	}

}
