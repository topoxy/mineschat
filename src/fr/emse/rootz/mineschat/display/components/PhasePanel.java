package fr.emse.rootz.mineschat.display.components;

import fr.emse.rootz.mineschat.interfaces.DisplayInterface;

/**
 * Représente un sous-panel de la fenêtre principale.
 * De façon générale, un PhasePanel prend vie à la création d'une MainWindow.
 * Quand un certain bouton est appuyé, celle-ci demande au sous-panel d'entrer en scène.
 * Les fonctions entering() et exiting() sont là pour permettre au panel de préparer ou faire ses affaires.
 */
public abstract class PhasePanel extends ColoredPanel {

	protected DisplayInterface phaseListener;
    protected Type type;
	public PhasePanel(DisplayInterface phaseListener, Type type) {
		super();
        this.type = type;

		this.phaseListener = phaseListener;
		constructGUI();
	}

    /**
     * Traduis le type de sous-panel en texte.
     * @param type le type à traduire
     * @return la traduction en français du type
     */
    public static String translateType(Type type) {
        String name;
        switch (type) {
            case INTRO:
                name = "Accueil";
                break;
            case OPTIONS:
                name = "Options";
                break;
            case CONNECTION:
                name = "Connexion";
                break;
            case SERVER:
                name = "Serveur";
                break;
            case HEADER:
                name = "Header";
                break;
            case DISCUSSION:
                name = "Discussion";
                break;
            default:
                name = "NULL";
        }
        return name;
    }

    public Type getType() {
        return type;
    }

	protected abstract void constructGUI();

	public abstract void entering();

	public abstract void exiting();

    public enum Type {NULL, INTRO, OPTIONS, CONNECTION, SERVER, HEADER, DISCUSSION}

}
