package fr.emse.rootz.mineschat.display.components;

import fr.emse.rootz.mineschat.display.options.DisplaySettings;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class ColoredQuestionDialog extends JDialog {

	protected ColoredPanel panel;
	protected JLabel dialogTitleLabel;
	protected JLabel processLabel;
	protected ColoredButton okButton;
	protected ColoredButton noButton;
	protected boolean processusValide;
	protected Image image;
	protected String message;
	protected String titre;
	protected boolean result;
	
	public ColoredQuestionDialog(JFrame parent, Image image, String titre, String message) {
		super(parent);
		
		this.image = image;
		this.message = message;
		this.titre = titre;
		
		constructGUI();
		this.setUndecorated(true);
		//this.setOpacity(0.98f);
		this.setModal(true);
		this.setSize(550, 500);
		this.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		this.setLocationRelativeTo(parent);
	}
	
	protected void constructGUI () {
		panel = new ColoredPanel(ColoredPanel.ColorMode.INVERSED_WITH_BORDERS, 20);
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
		
		panel.add(Box.createVerticalGlue());
		
		dialogTitleLabel = new JLabel(titre);
		dialogTitleLabel.setForeground(Color.white);
		dialogTitleLabel.setFont(DisplaySettings.middleTitleFont);
		dialogTitleLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
		
		panel.add(dialogTitleLabel);
		panel.add(Box.createVerticalGlue());
		
		JLabel infoImage = new JLabel (new ImageIcon (image));
		infoImage.setAlignmentX(Component.CENTER_ALIGNMENT);
		panel.add(infoImage);
		panel.add(Box.createVerticalGlue());
		
		processLabel = new JLabel("<html><div style=\"text-align: center;\">"+message+"</html>", SwingConstants.CENTER);
		processLabel.setForeground(Color.white);
		processLabel.setFont(DisplaySettings.veryLittleTitleFont);
		processLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
		panel.add(processLabel);
		panel.add(Box.createVerticalGlue());

		ColoredPanel buttonsPanel = new ColoredPanel (ColoredPanel.ColorMode.TRANSPARENT);
		buttonsPanel.setLayout(new BoxLayout(buttonsPanel, BoxLayout.X_AXIS));
		buttonsPanel.add(Box.createHorizontalGlue());
		
		//okButton = new ColoredButton("Ok!", DisplaySettings.okImage, DisplaySettings.optionsColor);
		okButton = new ColoredButton("Ok!", DisplaySettings.optionsColor);
		okButton.setFont(DisplaySettings.littleTitleFont);
		okButton.setAlignmentX(Component.CENTER_ALIGNMENT);
		okButton.addActionListener(e -> {
            result = true;
            dispose();
        });
		this.addKeyListener(new KeyAdapter() {

			@Override
			public void keyPressed(KeyEvent e) {
				if(e.getKeyCode() == KeyEvent.VK_ENTER)
					okButton.doClick();
			}
			
		});
		
		buttonsPanel.add(okButton);
		buttonsPanel.add(Box.createHorizontalGlue());
		
		noButton = new ColoredButton("Nope", DisplaySettings.quitColor);
		noButton.setFont(DisplaySettings.littleTitleFont);
		noButton.setAlignmentX(Component.CENTER_ALIGNMENT);
		noButton.addActionListener(e -> {
            result = false;
            dispose();
        });
		
		buttonsPanel.add(noButton);
		buttonsPanel.add(Box.createHorizontalGlue());
		
		panel.add(buttonsPanel);
		panel.add(Box.createVerticalGlue());
		
		this.setContentPane(panel);
	}


	public boolean getResult () {
		return result;
	}
}
