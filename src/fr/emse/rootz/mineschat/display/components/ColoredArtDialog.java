package fr.emse.rootz.mineschat.display.components;

import fr.emse.rootz.mineschat.data.ArtGrid;
import fr.emse.rootz.mineschat.display.options.DisplaySettings;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class ColoredArtDialog  extends JDialog {

	static Point mouseDownCompCoords;
	protected ColoredPanel panel;
	protected ColoredArtGridPanel artPanel;
	protected ColoredButton insertButton;
	private boolean activated = false;

	public ColoredArtDialog(JFrame parent) {
		super(parent);
		constructGUI();
		this.setUndecorated(true);
		//this.setOpacity(0.98f);
		this.setModal(false);
		this.setSize(400, 700);
		this.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		this.setLocationRelativeTo(null);
		prepareEvents ();
	}

	protected void constructGUI () {
		panel = new ColoredPanel(ColoredPanel.ColorMode.INVERSED_WITH_BORDERS, 10);
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));

		ColoredLabel dialogTitleLabel = new ColoredLabel("Oeuvre d'art", DisplaySettings.littleTitleFont);
		artPanel = new ColoredArtGridPanel (true);

		ColoredButton resetButton = new ColoredButton(ColoredPanel.ColorMode.SIMPLE, "Effacer");
		resetButton.setFont(DisplaySettings.veryLittleTitleFont);
		resetButton.setAlignmentX(Component.CENTER_ALIGNMENT);
		resetButton.addActionListener(e -> {
            artPanel.createEmptyGrid();
            artPanel.repaint();
        });

		ColoredPanel buttonsPanel = new ColoredPanel(ColoredPanel.ColorMode.TRANSPARENT);

		insertButton = new ColoredButton("Insérer", DisplaySettings.optionsColor);
		insertButton.setFont(DisplaySettings.computerFont);
		insertButton.setAlignmentX(Component.CENTER_ALIGNMENT);
		buttonsPanel.add(insertButton);

		ColoredButton closeButton = new ColoredButton("Fermer", DisplaySettings.secondaryColor);
		closeButton.setFont(DisplaySettings.middleTitleFont);
		closeButton.setAlignmentX(Component.CENTER_ALIGNMENT);
		closeButton.addActionListener(e -> dispose());
		buttonsPanel.add(closeButton);

		panel.add(Box.createVerticalGlue());
		panel.add(dialogTitleLabel);
		panel.add(Box.createVerticalGlue());
		panel.add(artPanel);
		panel.add(Box.createVerticalStrut(10));
		panel.add(resetButton);
		panel.add(Box.createVerticalGlue());
		panel.add(buttonsPanel);
		//panel.add(Box.createVerticalGlue());

		this.setContentPane(panel);
	}
	
	private void prepareEvents() {
		addMouseListener(new MouseAdapter() {
			public void mouseReleased(MouseEvent e) {
				mouseDownCompCoords = null;
			}

			public void mousePressed(MouseEvent e) {
				mouseDownCompCoords = e.getPoint();
			}
		});

		addMouseMotionListener(new MouseMotionAdapter() {
			public void mouseDragged(MouseEvent e) {
				Point currCoords = e.getLocationOnScreen();
				setLocation(currCoords.x - mouseDownCompCoords.x, currCoords.y
						- mouseDownCompCoords.y);
			}
		});
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowActivated(WindowEvent arg0) {
				setIconImage(DisplaySettings.splashImage);
				DisplaySettings.ableFutureSound();
				activated = true;
			}
			@Override
			public void windowDeactivated(WindowEvent arg0) {
				activated = false;
			}
		});
	}
	
	public void registerActionForInsert (ActionListener action) {
		insertButton.addActionListener(action);
	}
	
	public ArtGrid getArtGrid() {
		return artPanel.getArtGrid();
	}
	
	public boolean isActivated() {
		return activated;
	}

}
