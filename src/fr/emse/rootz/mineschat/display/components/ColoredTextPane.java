package fr.emse.rootz.mineschat.display.components;

import fr.emse.rootz.mineschat.data.DataArtGrid;
import fr.emse.rootz.mineschat.data.DataCollection;
import fr.emse.rootz.mineschat.data.Message;
import fr.emse.rootz.mineschat.display.options.DisplaySettings;
import fr.emse.rootz.mineschat.interfaces.ColorChangeable;

import javax.swing.*;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.event.HyperlinkEvent;
import javax.swing.text.*;
import javax.swing.text.html.HTMLEditorKit;
import java.awt.*;
import java.io.IOException;
import java.io.StringReader;
import java.net.URISyntaxException;
import java.util.Calendar;

public class ColoredTextPane extends JTextPane implements ColorChangeable {

	public Style timeTextStyle;
	public Style coloredTextStyle;
	public Style boldTextStyle;
	public Style normalTextStyle;
	private int lastMessageIdSender = 0;
	private boolean lastObjectWasAnImage = false;
	private int maxNumberOfArtGridPerLine = 4;
	private int accumulatedNumberOfArtGrid = 0;

	public ColoredTextPane() {
		super();
		DisplaySettings.registerColorChangeable(this);
        updateColors();

		this.setBorder(new CompoundBorder(new LineBorder(
				DisplaySettings.transparentWhite, 2, false), new EmptyBorder(
				10, 10, 10, 10)));
		this.setFont(DisplaySettings.textFont);
		this.setEditable(false);
		this.setContentType("text/html");
		this.putClientProperty(JEditorPane.HONOR_DISPLAY_PROPERTIES, true);
		DefaultCaret caret = (DefaultCaret) this.getCaret();
		caret.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);
		((HTMLEditorKit)this.getEditorKit()).getStyleSheet().addRule("A {color:white}");

		this.addHyperlinkListener(e -> {
            if (e.getEventType() == HyperlinkEvent.EventType.ACTIVATED) {
                if (Desktop.isDesktopSupported()) {
                    try {
                        Desktop.getDesktop().browse(e.getURL().toURI());
                    } catch (IOException | URISyntaxException e1) {
                        // TODO Auto-generated catch block
                        e1.printStackTrace();
                    }
				}
            }
        });

		StyledDocument doc = (StyledDocument) getDocument();
		boldTextStyle = doc.addStyle("Bold", null);
		StyleConstants.setBold(boldTextStyle, true);
		StyleConstants.setFontSize(boldTextStyle, 14);
		normalTextStyle = doc.addStyle("Normal", null);
		StyleConstants.setBold(normalTextStyle, false);
		StyleConstants.setFontSize(normalTextStyle, 14);
		coloredTextStyle = doc.addStyle("Bold", null);
		// StyleConstants.setBold(coloredTextStyle, true);
		StyleConstants.setItalic(coloredTextStyle, true);
		StyleConstants.setFontSize(coloredTextStyle, 14);
		timeTextStyle = doc.addStyle("Normal", null);
		StyleConstants.setFontSize(timeTextStyle, 7);

	}

	@Override
	public void updateColors() {
		this.setBackground(DisplaySettings.secondaryColor);
		this.setForeground(DisplaySettings.textColor);
	}

	public boolean checkForCommandsBefore (String message) {
		if(message.startsWith("/clean")||message.startsWith("/clear")||message.startsWith("/effacer")) {
			this.setText("");
			System.gc();
			return true;
		}
		
		return false;
	}
	
	public boolean checkForCommands (Message message) {
		if(message.getMessage().startsWith("/GodTalks")||message.getMessage().startsWith("/join"))
			return true;
		if(message.getMessage().startsWith("/help"))
			message.setMessage("TASKETE ONII-CHAAAAAAN!");
		
		return false;
	}

	public void publishMessage(Message message) {
		Document doc = getDocument();
		String senderName;
		try {
			switch (message.getIdSender()) {
			case 0:
				senderName = DataCollection.getServerName();
				break;
			default:
				senderName = DataCollection.getPerson(message.getIdSender())
						.getPseudonyme();
				break;
			}
			
			if(checkForCommands(message))
				return;
			
			if(lastObjectWasAnImage) {
				lastObjectWasAnImage = false;
				doc.insertString(doc.getLength(), "\n", normalTextStyle);
			}
			
			//doc.insertString(doc.getLength(), getTimeTag(), timeTextStyle);
			
			/*
			if(!message.getMessage().startsWith("html:")) {
				if(message.getMessage().startsWith("/me ")) {
					doc.insertString(doc.getLength(), senderName + " " + message.getMessage().substring(4) + "\n", coloredTextStyle);
					
				} else if (message.getIdSender() == 0) {
					doc.insertString(doc.getLength(), senderName + " : " + message.getMessage() + "\n", coloredTextStyle);
				}
				else {
					doc.insertString(doc.getLength(), senderName + " : ", boldTextStyle);
					doc.insertString(doc.getLength(), message.getMessage() + "\n", normalTextStyle);
				}
			}*/
			//else {
			HTMLEditorKit editor = (HTMLEditorKit) this.getEditorKit();
			StringReader reader;
			
			if(message.getMessage().startsWith("html:"))
				message.setMessage(message.getMessage().substring(5));
			
			if(message.getMessage().startsWith("/me ")) {
				reader = new StringReader("<font size=\"1\">"+getTimeTag()+"</font>"+"<i> "+senderName + " "+ message.getMessage().substring(4) + " </i>");
			}
			else if(message.getIdSender() != 0)
				reader = new StringReader("<font size=\"1\">"+getTimeTag()+"</font>"+"<b> "+senderName + " : </b>" + message.getMessage());
			else
				reader = new StringReader("<font size=\"1\">"+getTimeTag()+"</font>  "+message.getMessage());
			
			
			editor.read(reader, doc, doc.getLength());
			EventQueue.invokeLater(() -> {
                repaint();
                setVisible(true);
            });
			//}
			
		} catch (BadLocationException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			setCaretPosition(getDocument().getLength());
		}
	}
	
	public void publishArtGrid (DataArtGrid dataArtGrid) {
		//maxNumberOfArtGridPerLine = this.getWidth()/(DisplaySettings.artGridSize*DisplaySettings.artGridCellSize+1);
		//System.out.println(maxNumberOfArtGridPerLine);
		Document doc = getDocument();
		String senderName;
		try {
			switch (dataArtGrid.getIdSender()) {
			case 0:
				senderName = DataCollection.getServerName();
				break;
			default:
				senderName = DataCollection.getPerson(dataArtGrid.getIdSender())
						.getPseudonyme();
				break;
			}
			
			if(!lastObjectWasAnImage) {
				accumulatedNumberOfArtGrid = 1;
			}
			else {
				if(lastMessageIdSender==dataArtGrid.getIdSender())
					accumulatedNumberOfArtGrid++;
				else
					accumulatedNumberOfArtGrid = 1;
				if(accumulatedNumberOfArtGrid> (!DisplaySettings.reduced ? 4 : 2)) {
					//accumulatedNumberOfArtGrid = 1;
					//doc.insertString(doc.getLength(), "\n", boldTextStyle);
					return;
				}
			}
			
			if(lastMessageIdSender!=dataArtGrid.getIdSender()) {
				if(lastObjectWasAnImage)
					doc.insertString(doc.getLength(), "\n", boldTextStyle);

				HTMLEditorKit editor = (HTMLEditorKit) this.getEditorKit();
				StringReader reader = null;

				reader = new StringReader("<font size=\"1\">"+getTimeTag()+"</font>"+"<b> "+senderName+ "</b> : ");
				editor.read(reader, doc, doc.getLength());
				
				//doc.insertString(doc.getLength(), getTimeTag(), timeTextStyle);
				//doc.insertString(doc.getLength(), senderName + " : \n", boldTextStyle);
			}
			
			Style style = ((StyledDocument)doc).addStyle("StyleName", null);
		    StyleConstants.setIcon(style, new ImageIcon(ColoredArtGridPanel.createImage(dataArtGrid.getArtGrid(), false)));
		    doc.insertString(doc.getLength(), "Image", style);
		    
			//this.insertIcon(new ImageIcon(ColoredArtGridPanel.createImage(dataArtGrid.getArtGrid(), false)));

			//if(lastMessageIdSender!=dataArtGrid.getIdSender())
			//	doc.insertString(doc.getLength(), " \n", boldTextStyle);
			/*HTMLEditorKit editor = (HTMLEditorKit) this.getEditorKit();
			StringReader reader = new StringReader("<br/>");
			editor.read(reader, doc, doc.getLength());*/
			
			lastObjectWasAnImage = true;
			lastMessageIdSender = dataArtGrid.getIdSender();
				
		} catch (BadLocationException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			setCaretPosition(getDocument().getLength());
		}
	}
	
	private String getTimeTag () {
		Calendar calendar = Calendar.getInstance();
		String textTime = "[";
		textTime+=calendar.get(Calendar.HOUR_OF_DAY)+":";
		textTime+=calendar.get(Calendar.MINUTE)+":";
		textTime+=calendar.get(Calendar.SECOND);
		textTime+="]";
		return textTime;
	}

}