package fr.emse.rootz.mineschat.display.components;

import fr.emse.rootz.mineschat.display.options.DisplaySettings;
import fr.emse.rootz.mineschat.interfaces.ColorChangeable;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import java.awt.*;

/**
 * Brique de base des interfaces graphiques.
 * On passe par cette classe afin de pouvoir contrôler la couleur des composants de l'interface.
 */
public class ColoredPanel extends JPanel implements ColorChangeable {

	private Color backgroundColor;
	private Color foregroundColor;
	private Color lineColor;
	private int lineThickness;
	private ColorMode mode;

	public ColoredPanel() {
		this(ColorMode.SIMPLE);
	}

	public ColoredPanel(ColorMode mode) {
		super();
		this.mode = mode;
        if(mode != ColorMode.INDEPENDANT)
            DisplaySettings.registerColorChangeable(this);
		updateColors();
	}

	public ColoredPanel(int lineThickness) {
		this(ColorMode.WITH_BORDERS);
		this.lineThickness = lineThickness;
	}

	public ColoredPanel(ColorMode mode, int thickness) {
		this(mode);
		this.lineThickness = thickness;
	}

	/**
	 * Permet de créer un panel tout en lui donnant des composants à ajouter
	 * @param orientation
	 * @param components
	 * @return
	 */
	public static ColoredPanel putInGroup(char orientation, Component... components) {
		ColoredPanel panel = new ColoredPanel();
		panel.setLayout(new BoxLayout(panel, orientation == 'h' ? BoxLayout.X_AXIS : BoxLayout.Y_AXIS));
		for(Component component : components)
			panel.add(component);
		return panel;
	}

	/**
	 * Permet d'appliquer une couleur à toute la descendance d'un composant
	 * @param parentComponent
	 * @param backgroundColor
	 * @param foregroundColor
	 */
	public static void recursivelyApplyColor(JComponent parentComponent, Color backgroundColor, Color foregroundColor) {
		parentComponent.setBackground(backgroundColor);
		parentComponent.setForeground(foregroundColor);
		for(Component component : parentComponent.getComponents())
			recursivelyApplyColor((JComponent) component, backgroundColor, foregroundColor);
	}

	@Override
	public void updateColors() {
		switch (mode) {
			case SIMPLE:
				backgroundColor = DisplaySettings.primaryColor;
				foregroundColor = DisplaySettings.textColor;
				lineColor = DisplaySettings.primaryColor;
				lineThickness = 0;
				break;
			case WITH_BORDERS:
				backgroundColor = DisplaySettings.primaryColor;
				foregroundColor = DisplaySettings.textColor;
				lineColor = DisplaySettings.secondaryColor;
				break;
			case INVERSED:
				backgroundColor = DisplaySettings.secondaryColor;
				foregroundColor = DisplaySettings.textColor;
				lineColor = DisplaySettings.secondaryColor;
				lineThickness = 0;
				break;
			case INVERSED_WITH_BORDERS:
				backgroundColor = DisplaySettings.secondaryColor;
				foregroundColor = DisplaySettings.textColor;
				lineColor = DisplaySettings.primaryColor;
				break;
            case TRANSPARENT:
                backgroundColor = DisplaySettings.transparent;
                foregroundColor = DisplaySettings.textColor;
                lineColor = DisplaySettings.primaryColor;
                lineThickness = 0;
                break;
            case INVERSED_CLEAR:
                backgroundColor = DisplaySettings.secondaryColor;
                foregroundColor = DisplaySettings.textColor;
                lineColor = DisplaySettings.transparentWhite;
                lineThickness = 0;
                break;
            default:
                break;
		}

		setBackground(backgroundColor);
		setForeground(foregroundColor);
        if (getBorder() instanceof TitledBorder)
            ((TitledBorder) getBorder()).setTitleColor(foregroundColor);
	}

	/**
	 * Place du vide autour du composant.
	 * @param espace
	 */
	public void espacerDuReste (int espace) {
		setBorder( new EmptyBorder( espace, espace, espace, espace));
	}

	protected void paintComponent(Graphics g0) {
		Graphics2D g = (Graphics2D) g0;
		// background
		g.setColor(backgroundColor);
		g.fillRect(0,0,getWidth(),getHeight());
		// line
		if(lineThickness>0) {
			g.setColor(lineColor);
			g.fillRect(0, 0, lineThickness, getHeight());
			g.fillRect(getWidth()-lineThickness, 0, getWidth(), getHeight());
			g.fillRect(0, 0, getWidth(), lineThickness);
			g.fillRect(0, getHeight()-lineThickness, getWidth(), getHeight());
		}
	}

	/**
	 * Ajoute de la "glue", ça sert à l'espacement dans les layouts.
	 * @param nbGlue quantité de "glue" à ajouter
	 * @param orientation 'v' ou 'h', pour Vertical ou Horizontal
	 */
	public void addGlue(int nbGlue, char orientation) {
		switch(orientation) {
			case 'h':
				for(int i = 0; i < nbGlue; i++)
					add(Box.createHorizontalGlue());
				break;
			case 'v':
				for(int i = 0; i < nbGlue; i++)
					add(Box.createVerticalGlue());
				break;
			default:
				break;
		}
	}

	public enum ColorMode { SIMPLE, WITH_BORDERS, INVERSED, INVERSED_WITH_BORDERS, TRANSPARENT, INDEPENDANT, INVERSED_CLEAR}
}
