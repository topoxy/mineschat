package fr.emse.rootz.mineschat.display.components;

import fr.emse.rootz.mineschat.display.options.DisplaySettings;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class ColoredInfoDialog extends JDialog{

	protected ColoredPanel panel;
	protected JLabel dialogTitleLabel;
	protected JLabel processLabel;
	protected ColoredButton okButton;
	protected boolean processusValide;
	protected Image image;
	protected String message;

	public ColoredInfoDialog(JFrame parent, Image image, String message, boolean modal) {
		super(parent);
		
		this.image = image;
		this.message = message;
		
		constructGUI();
		this.setUndecorated(true);
		//this.setOpacity(0.98f);
		this.setModal(modal);
		this.setSize(550, 650);
		this.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		this.setLocationRelativeTo(parent);
		this.setVisible(true);
	}
	
	public ColoredInfoDialog(JFrame parent, Image image, String message) {
		super(parent);
		
		this.image = image;
		this.message = message;
		
		constructGUI();
		this.setUndecorated(true);
		//this.setOpacity(0.98f);
		this.setModal(true);
		this.setSize(550, 650);
		this.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		this.setLocationRelativeTo(parent);
		this.setVisible(true);
	}
	
	protected void constructGUI () {
		panel = new ColoredPanel(ColoredPanel.ColorMode.INVERSED_WITH_BORDERS, 10);
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
		
		panel.add(Box.createVerticalGlue());
		
		dialogTitleLabel = new JLabel("Information");
		dialogTitleLabel.setForeground(Color.white);
		dialogTitleLabel.setFont(DisplaySettings.middleTitleFont);
		dialogTitleLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
		
		panel.add(dialogTitleLabel);
		panel.add(Box.createVerticalGlue());
		
		JLabel infoImage = new JLabel (new ImageIcon (image));
		infoImage.setAlignmentX(Component.CENTER_ALIGNMENT);
		panel.add(infoImage);
		panel.add(Box.createVerticalGlue());
		
		processLabel = new JLabel("<html><div style=\"text-align: center;\">"+message+"</html>", SwingConstants.CENTER);
		processLabel.setForeground(Color.white);
		processLabel.setFont(DisplaySettings.veryLittleTitleFont);
		processLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
		panel.add(processLabel);
		panel.add(Box.createVerticalGlue());

		//okButton = new ColoredButton("Ok!", DisplaySettings.okImage, DisplaySettings.optionsColor);
		okButton = new ColoredButton("Ok!", DisplaySettings.optionsColor);
		okButton.setFont(DisplaySettings.titleFont);
		okButton.setAlignmentX(Component.CENTER_ALIGNMENT);
		okButton.addActionListener(e -> dispose());
		this.addKeyListener(new KeyAdapter() {

			@Override
			public void keyPressed(KeyEvent e) {
				if(e.getKeyCode() == KeyEvent.VK_ENTER)
					okButton.doClick();
			}
			
		});
		
		panel.add(okButton);
		panel.add(Box.createVerticalGlue());
		
		this.setContentPane(panel);
	}

}
