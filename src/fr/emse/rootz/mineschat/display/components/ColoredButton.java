package fr.emse.rootz.mineschat.display.components;

import fr.emse.rootz.mineschat.display.components.ColoredPanel.ColorMode;
import fr.emse.rootz.mineschat.display.options.DisplaySettings;
import fr.emse.rootz.mineschat.interfaces.ColorChangeable;

import javax.swing.*;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class ColoredButton extends JButton implements ColorChangeable {

	private Color backgroundColor;
	private Color foregroundColor;
	private Color lineColor;
	private ColorMode mode;
	private Color secondBackgroundColor;
	private boolean pressed;
	private String text1;
	private String text2;
	private int size;
	private int lineThickness;
	private int voidSpaceX = 0;
	private int voidSpaceY = 0;

	public ColoredButton(ColorMode mode, String text) {
		super(text);
		this.mode = mode;
		updateColors();
		if(mode != ColorMode.INDEPENDANT)
			DisplaySettings.registerColorChangeable(this);
	}

	public ColoredButton(String text) {
		this(ColorMode.INVERSED, text);
	}

	public ColoredButton(String text, int size) {
		this(ColorMode.INVERSED, text);
		this.size = size;
		this.espacerDuReste(size);
	}

	public ColoredButton(ColorMode mode, String text, int size) {
		this(mode, text);
		this.size = size;
	}

	public ColoredButton(String text, Color color) {
		this(ColorMode.INDEPENDANT, text);
		this.backgroundColor = color;
		this.setBackground(color);
		this.setForeground(DisplaySettings.textColor);
		this.setFocusable(false);
	}

	public ColoredButton(String text, Color colorFirst, Color colorSecond, int size, int lineThickness) {
		this(ColorMode.INDEPENDANT, text);
		this.backgroundColor = colorFirst;
		this.setBackground(colorFirst);
		this.setFocusable(false);
		this.size = size;
		this.secondBackgroundColor = colorSecond;
		this.lineThickness = lineThickness;
		this.espacerDuResteAvecBordure(size, colorSecond, lineThickness);
	}

    public ColoredButton(ColorMode mode, Image image)  {
        this(image, null);
        this.mode = mode;
        updateColors();
        if(mode != ColorMode.INDEPENDANT)
            DisplaySettings.registerColorChangeable(this);
    }

	public ColoredButton(Image image, Color color)  {
		this(null, image, color);
	}

    public ColoredButton(ColorMode mode, Image image, String text)  {
        this(image, null);
        this.mode = mode;
        this.setText(text);
        updateColors();
        if(mode != ColorMode.INDEPENDANT)
            DisplaySettings.registerColorChangeable(this);
    }

	public ColoredButton(String text, Image image, Color color)  {
		super(text, new ImageIcon(image));
		setCommonSettings(color);
        if(color != null) {
            this.mode = ColorMode.INVERSED;
            DisplaySettings.registerColorChangeable(this);
        }
	}

	@Override
	public void updateColors() {
		switch (mode) {
			case SIMPLE:
				backgroundColor = DisplaySettings.primaryColor;
				foregroundColor = DisplaySettings.textColor;
				lineColor = DisplaySettings.primaryColor;
				lineThickness = 0;
				break;
			case WITH_BORDERS:
				backgroundColor = DisplaySettings.primaryColor;
				foregroundColor = DisplaySettings.textColor;
				lineColor = DisplaySettings.secondaryColor;
				break;
			case INVERSED:
				backgroundColor = DisplaySettings.secondaryColor;
				foregroundColor = DisplaySettings.textColor;
				lineColor = DisplaySettings.secondaryColor;
				lineThickness = 0;
				break;
			case INVERSED_WITH_BORDERS:
				backgroundColor = DisplaySettings.secondaryColor;
				foregroundColor = DisplaySettings.textColor;
				lineColor = DisplaySettings.primaryColor;
				break;
			case TRANSPARENT:
				backgroundColor = DisplaySettings.transparentWhite;
				foregroundColor = DisplaySettings.textColor;
				lineColor = DisplaySettings.secondaryColor;
				lineThickness = 0;
                setBorder(new CompoundBorder(new LineBorder(lineColor, lineThickness), new EmptyBorder(voidSpaceY, voidSpaceX, voidSpaceY, voidSpaceX)));
				break;
			default:
				break;
		}

		if(mode != ColorMode.INDEPENDANT) {
			setBackground(backgroundColor);
			setForeground(foregroundColor);
		}

	}
	
	private void setCommonSettings (Color color) {
		if(color!=null)
			this.setBackground(color);
		else {
			setContentAreaFilled(false); 
			setOpaque(false);
		}
		this.setForeground(DisplaySettings.textColor);
		this.setFocusable(false);
	}
	
	public void setSecondarySettings (String text2_0, Color color2_0) {
		this.backgroundColor = this.getBackground();
		this.secondBackgroundColor = color2_0;
		this.text1 = this.getText();
		this.text2 = text2_0;
		this.pressed = false;
		
		this.addMouseListener(new MouseAdapter () {
			@Override
			public void mouseReleased(MouseEvent arg0) {
				if(!pressed) {
					pressed = true;
					setText(text2);
					setBackground(secondBackgroundColor);
				}
				else {
					pressed = false;
					setText(text1);
					setBackground(backgroundColor);
				}
			}
		});
	}

	public void espacerDuReste (int espace) {
		setBorder( new EmptyBorder( espace, espace, espace, espace));
	}
	public void espacerDuReste (int espaceX, int espaceY) {
		setBorder( new EmptyBorder(espaceY, espaceX, espaceY, espaceX));
	}

	public void espacerDuResteAvecBordure (int espace, Color color, int thickness) {
		voidSpaceX = espace;
		voidSpaceY = espace;
        lineColor = color;
        this.lineThickness = thickness;
		setBorder(new CompoundBorder(new LineBorder(color, thickness), new EmptyBorder( espace, espace, espace, espace)));
	}
	
	public void espacerDuResteAvecBordure (int espaceX, int espaceY, Color color, int thickness) {
		voidSpaceX = espaceX;
		voidSpaceY = espaceY;
        lineColor = color;
        this.lineThickness = thickness;
		setBorder(new CompoundBorder(new LineBorder(lineColor, thickness), new EmptyBorder(espaceY, espaceX, espaceY, espaceX)));
	}
}
