package fr.emse.rootz.mineschat.display.components;

import fr.emse.rootz.mineschat.display.options.DisplaySettings;

import javax.swing.*;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;
import java.awt.*;

public class ColoredInputDialog extends JDialog {
	
	protected ColoredPanel panel;
	protected JLabel dialogTitleLabel;
	protected JTextField textField;
	protected ColoredButton okButton;
	protected boolean processusValide;
	protected String message;
	
	public ColoredInputDialog(JFrame parent, String message) {
		super(parent);
		
		this.message = message;
		
		constructGUI();
		this.setUndecorated(true);
		//this.setOpacity(0.98f);
		this.setModal(true);
		this.setSize(500, 300);
		this.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		this.setLocationRelativeTo(parent);
		this.setVisible(true);
	}
	
	protected void constructGUI () {
		panel = new ColoredPanel(ColoredPanel.ColorMode.INVERSED_WITH_BORDERS, 10);
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
		
		panel.add(Box.createVerticalGlue());
		
		dialogTitleLabel = new JLabel("Demande");
		dialogTitleLabel.setForeground(Color.white);
		dialogTitleLabel.setFont(DisplaySettings.middleTitleFont);
		dialogTitleLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
		
		panel.add(dialogTitleLabel);
		panel.add(Box.createVerticalGlue());
		
		JLabel infoImage = new JLabel (message);
		infoImage.setAlignmentX(Component.CENTER_ALIGNMENT);
		infoImage.setForeground(Color.white);
		infoImage.setFont(DisplaySettings.littleTitleFont);
		infoImage.setAlignmentX(Component.CENTER_ALIGNMENT);
		panel.add(infoImage);
		panel.add(Box.createVerticalGlue());
		
		textField = new JTextField();
		textField.setBackground(DisplaySettings.secondaryColor);
		textField.setForeground(Color.white);
		textField.setFont(DisplaySettings.littleTitleFont);
		textField.setAlignmentX(Component.CENTER_ALIGNMENT);
		textField.setHorizontalAlignment(JTextField.CENTER);
		textField.setMaximumSize(new Dimension(400, 40));
		textField.setCaretColor(Color.white);
		textField.setDocument(new JTextFieldLimit(20));
		textField.addActionListener(e -> okButton.doClick());
		
		panel.add(textField);
		panel.add(Box.createVerticalGlue());

		okButton = new ColoredButton("Confirmer", DisplaySettings.optionsColor);
		okButton.setFont(DisplaySettings.veryLittleTitleFont);
		okButton.setAlignmentX(Component.CENTER_ALIGNMENT);
		okButton.addActionListener(e -> dispose());
		
		panel.add(okButton);
		panel.add(Box.createVerticalGlue());
		
		this.setContentPane(panel);
	}
	
	public String getInput () {
		return textField.getText();
	}
	
	public class JTextFieldLimit extends PlainDocument {
		  private int limit;
		  // optional uppercase conversion
		  private boolean toUppercase = false;
		  
		  JTextFieldLimit(int limit) {
		   super();
		   this.limit = limit;
		   }
		   
		  JTextFieldLimit(int limit, boolean upper) {
		   super();
		   this.limit = limit;
		   toUppercase = upper;
		   }
		 
		  public void insertString
		    (int offset, String  str, AttributeSet attr)
		      throws BadLocationException {
		   if (str == null) return;

		   if ((getLength() + str.length()) <= limit) {
		     if (toUppercase) str = str.toUpperCase();
		     super.insertString(offset, str, attr);
		     }
		   }
		}

}
