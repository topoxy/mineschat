package fr.emse.rootz.mineschat.display.components;

import fr.emse.rootz.mineschat.data.DataUtils;
import fr.emse.rootz.mineschat.data.datatransmission.DataTransfertCancel;
import fr.emse.rootz.mineschat.display.options.DisplaySettings;
import fr.emse.rootz.mineschat.interfaces.NetworkFileTransfertListener;
import fr.emse.rootz.mineschat.interfaces.TransfertDisplayer;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

public class ColoredProgressDialog extends JDialog implements TransfertDisplayer {

	protected ColoredPanel panel;
	protected JLabel processLabel;
	protected JLabel informationLabel;
	protected ColoredButton cancelButton;
	protected ColoredProgressBar progressBar;
	protected boolean stillUndergoing = true;
	protected Point mouseDownCompCoords;

	protected String fileName;
	protected long dataReceived = 0;
	protected long dataTotalSize = 0;
	protected long transfertSpeed = 0;
	protected int idDataTansfert;
	protected int idSender;
	protected int idReceiver;
	
	protected Date date;
	protected NetworkFileTransfertListener manager;
	protected Timer timer;
	
	protected JFrame parent;
	private int turns = 0;

	public ColoredProgressDialog(JFrame parent) {
		super(parent);
		this.parent = parent;
		constructGUI();
		addMovementDisposibilities();

		this.setModal(false);
		this.setSize(650, 400);
		this.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		this.setUndecorated(true);
		this.setLocationRelativeTo(null);
	}
	
	protected void constructGUI() {
		panel = new ColoredPanel(ColoredPanel.ColorMode.INVERSED_WITH_BORDERS, 10);
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));

		panel.add(Box.createVerticalGlue());

		processLabel = new JLabel("Téléchargement en cours...");
		processLabel.setForeground(Color.white);
		processLabel.setFont(DisplaySettings.littleTitleFont);
		processLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
		panel.add(processLabel);

		panel.add(Box.createVerticalGlue());

		progressBar = new ColoredProgressBar(500, 60);
		panel.add(progressBar);

		panel.add(Box.createVerticalGlue());

		informationLabel = new JLabel("",SwingConstants.CENTER);
		updateInfo();
		informationLabel.setForeground(Color.white);
		informationLabel.setFont(DisplaySettings.textFont);
		informationLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
		panel.add(informationLabel);

		panel.add(Box.createVerticalGlue());

		cancelButton = new ColoredButton("Annuler", DisplaySettings.quitColor);
		cancelButton.setFont(DisplaySettings.titleFont);
		cancelButton.setAlignmentX(Component.CENTER_ALIGNMENT);
		cancelButton.addActionListener(e -> {
            stillUndergoing = false;
            if(!isComplete()) {
                manager.sendCancelTransfert(new DataTransfertCancel(fileName, idDataTansfert, idSender, idReceiver));
            }
            dispose();
        });
		panel.add(cancelButton);

		panel.add(Box.createVerticalGlue());

		this.setContentPane(panel);
	}
	
	public void startTransfert (NetworkFileTransfertListener manager, String nameFile, long dataTotalSize0, int idDataTransfert0, int idSender0, int idReceiver0) {
		setPourcentage(0);
		this.fileName = nameFile;
		this.dataTotalSize = dataTotalSize0;
		this.transfertSpeed = 0;
		this.dataReceived = 0;
		this.idDataTansfert = idDataTransfert0;
		this.idSender = idSender0;
		this.idReceiver = idReceiver0;
		this.manager = manager;
		date = new Date();
		
		timer = new Timer ();
		timer.scheduleAtFixedRate(new TimerTask() {
			
			private long dataReceivedBefore;
			
			@Override
			public void run() {
				// Calcul du temps de transfert
				if(turns%3 == 0) {
					Date dateNow = new Date();
					transfertSpeed = (long) ((dataReceived - dataReceivedBefore)*1000.0/(dateNow.getTime() - date.getTime()));
					dataReceivedBefore = dataReceived;
					date = dateNow;
				}
				turns++;
				
				updateInfo();
				setPourcentage(((float)dataReceived)/dataTotalSize);
				
				if(isComplete())
					transfertIsComplete();
				
				repaint();
			}
		}, 200, 300);
	}
	
	private void updateInfo () {
		informationLabel.setText("<html><div style=\"text-align: center;\"><b>"+fileName + " :</b><br/> "+ String.format("%.1f", progressBar.getPourcentage()) +"%, \t" 
	+ DataUtils.sizeToText(transfertSpeed)+ "/s, \t" + DataUtils.sizeToText(dataReceived) + "/" + DataUtils.sizeToText(dataTotalSize)+
		"<br/>Temps restant : "+(transfertSpeed>0 ? DataUtils.timeToText((dataTotalSize-dataReceived)/transfertSpeed) : "temps infini")+ "</html>");
	}

	public void dataReceived (long nbBytes) {
		this.dataReceived += nbBytes;
		setPourcentage(((float)dataReceived)/dataTotalSize);
		progressBar.repaint();
	}
	
	private void transfertIsComplete () {
		new ColoredInfoDialog(parent, DisplaySettings.okImage, "Le transfert du fichier <br/><b>"+fileName+" !</b> <br/> est terminé!");
		manager.transfertFinished(idDataTansfert);
		close();
	}
	
	private void addMovementDisposibilities () {
		addMouseListener(new MouseAdapter() {
			public void mouseReleased(MouseEvent e) {
				mouseDownCompCoords = null;
			}

			public void mousePressed(MouseEvent e) {
				mouseDownCompCoords = e.getPoint();
			}
		});
		
		addMouseMotionListener(new MouseMotionAdapter() {
			public void mouseDragged(MouseEvent e) {
				Point currCoords = e.getLocationOnScreen();
				setLocation(currCoords.x - mouseDownCompCoords.x, currCoords.y
						- mouseDownCompCoords.y);
			}
		});
	}
	
	public boolean isCanceled () {
		return !stillUndergoing;
	}
	
	public boolean isComplete () {
		return dataReceived == dataTotalSize;
	}
	
	private void setPourcentage (float p) {
		progressBar.setPourcentage(p*100);
	}

	@Override
	public void close() {
		timer.cancel();
		dispose();
	}
}
