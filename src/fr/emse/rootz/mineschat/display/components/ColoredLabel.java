package fr.emse.rootz.mineschat.display.components;

import fr.emse.rootz.mineschat.display.options.DisplaySettings;
import fr.emse.rootz.mineschat.interfaces.ColorChangeable;

import javax.swing.*;
import java.awt.*;

/**
 * Created by vincent on 03/06/15.
 */
public class ColoredLabel extends JLabel implements ColorChangeable {

    private Color backgroundColor = null;

    public ColoredLabel(Color color) {
        super();
        this.backgroundColor = color;
        updateColors();
        DisplaySettings.registerColorChangeable(this);
        setAlignmentX(CENTER_ALIGNMENT);

    }

    public ColoredLabel(ImageIcon icon) {
        super(icon, SwingConstants.CENTER);
        setAlignmentX(CENTER_ALIGNMENT);
    }

    public ColoredLabel (String text, Font font) {
        super(text, SwingConstants.CENTER);
        setFont(font);
        setAlignmentX(CENTER_ALIGNMENT);
        updateColors();
        DisplaySettings.registerColorChangeable(this);
    }

    @Override
    public void updateColors() {
        if(backgroundColor != null) {
            backgroundColor = DisplaySettings.primaryColor;
            this.setBackground(backgroundColor);
        }
        this.setForeground(DisplaySettings.textColor);
    }
}
