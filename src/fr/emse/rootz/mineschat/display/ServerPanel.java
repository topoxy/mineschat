package fr.emse.rootz.mineschat.display;

import fr.emse.rootz.mineschat.data.DataCollection;
import fr.emse.rootz.mineschat.display.components.*;
import fr.emse.rootz.mineschat.display.options.DisplaySettings;
import fr.emse.rootz.mineschat.display.options.MCStrings;
import fr.emse.rootz.mineschat.interfaces.DisplayInterface;
import fr.emse.rootz.mineschat.network.MCNetworkServer;
import fr.emse.rootz.mineschat.network.NetworkSettings;

import javax.swing.*;
import java.awt.*;
import java.net.Inet4Address;
import java.net.UnknownHostException;

import static fr.emse.rootz.mineschat.display.options.MCStrings.getString;

public class ServerPanel extends PhasePanel {

	private ColoredTextView logPanel;
	private ColoredButton startServer;

	public ServerPanel(DisplayInterface phaseListener, Type type) {
		super(phaseListener, type);

		prepareNetworkStuff();
	}
	
	protected void constructGUI() {
		this.setLayout(new BorderLayout(5, 5));

		JLabel serverImage = new JLabel(new ImageIcon(DisplaySettings.serverImage));
		this.add(serverImage, BorderLayout.WEST);
		
		ColoredPanel southPanel = new ColoredPanel();
		southPanel.setLayout(new BorderLayout());
		
		startServer = new ColoredButton(getString(MCStrings.TextType.SERVER_START), DisplaySettings.optionsColor);
		startServer.setFont(DisplaySettings.titleFont);
		startServer.setSecondarySettings(getString(MCStrings.TextType.SERVER_STOP), DisplaySettings.quitColor);
		southPanel.add(startServer, BorderLayout.CENTER);
		
		ColoredButton returnButton = new ColoredButton(DisplaySettings.returnImage, DisplaySettings.secondaryColor);
		returnButton.addActionListener(e -> phaseListener.changePhase(Type.INTRO));
		returnButton.setOpaque(false);
		returnButton.espacerDuResteAvecBordure(5, 5, DisplaySettings.transparentWhite, 5);
		southPanel.add(returnButton, BorderLayout.EAST);
		
		this.add(southPanel, BorderLayout.SOUTH);

		ColoredPanel rigthPanel = new ColoredPanel();
		rigthPanel.setLayout(new BoxLayout(rigthPanel, 3));
		JLabel ipServerLabel = new JLabel();
		ipServerLabel.setFont(DisplaySettings.textFont);
		ipServerLabel.setForeground(Color.white);
		rigthPanel.add(ipServerLabel, BorderLayout.CENTER);
		this.add(rigthPanel, BorderLayout.EAST);

		JLabel serverPhaseLabel = new JLabel(getString(MCStrings.TextType.SERVER_PHASE_LABEL), SwingConstants.CENTER);
		serverPhaseLabel.setForeground(Color.white);
		serverPhaseLabel.setFont(DisplaySettings.middleTitleFont);
		this.add(serverPhaseLabel, BorderLayout.NORTH);
		
		logPanel = new ColoredTextView ();
		this.add(logPanel.rendreScrollable(), BorderLayout.CENTER);
		MCNetworkServer.registerLogger(logPanel);
	}
	
	protected void prepareNetworkStuff () {
		startServer.addActionListener(e -> {
            if(!MCNetworkServer.isServerActive()) {
                String serverName = "";

                while(serverName.isEmpty()) {
                    serverName = new ColoredInputDialog(phaseListener.getWindow(), getString(MCStrings.TextType.SERVER_NAME_CHOICE)).getInput();
                    if(serverName.isEmpty()) {
                        new ColoredInfoDialog(phaseListener.getWindow(), DisplaySettings.quitImage, getString(MCStrings.TextType.SERVER_NAME_INVALID));
                    }
                }
                DataCollection.setServerName(serverName);

                MCNetworkServer.startServer();
            }
            else
                MCNetworkServer.stopServer();
        });
	}

	@Override
	public void entering() {
		try {
			String ipServer = Inet4Address.getLocalHost().getHostAddress();
			logPanel.writeMessage(getString(MCStrings.TextType.IP_ADDRESS_LABEL)+ ipServer);
			logPanel.writeMessage("Port TCP : "+NetworkSettings.MCNETWORK_TCP_PORT);
			logPanel.writeMessage("Port UCP : "+NetworkSettings.MCNETWORK_UDP_PORT);
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
		}
	}

	@Override
	public void exiting() {
	}

}
