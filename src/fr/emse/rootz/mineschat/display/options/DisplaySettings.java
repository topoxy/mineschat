package fr.emse.rootz.mineschat.display.options;

import fr.emse.rootz.mineschat.data.DataUtils;
import fr.emse.rootz.mineschat.interfaces.ColorChangeable;

import javax.sound.sampled.*;
import javax.sound.sampled.LineEvent.Type;
import javax.swing.*;
import javax.swing.plaf.ColorUIResource;
import java.awt.*;
import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;

public class DisplaySettings {

	// //////////////////////
	// WINDOW
	// //////////////////////

	public static final int artGridSize = 20;
	public static final int artGridCellSize = 8;
    // Permet de détecter une couleur interface issue d'une version récette
    private static final int COLOR_SHIFT = 20;
	public static boolean reduced = false;
	public static int INTRO_SCREEN_SIZE_X = 1100;
	public static int INTRO_SCREEN_SIZE_Y = 700;
	public static int REDUCED_SCREEN_SIZE_X = 400;
	public static int REDUCED_SCREEN_SIZE_Y = 700;
	public static int REDUCED_EXTENDED_SCREEN_SIZE_Y;
	public static int currentScreenSizeX = INTRO_SCREEN_SIZE_X;
	public static int currentScreenSizeY = INTRO_SCREEN_SIZE_Y;
	public static int HEADER_LAYOUT_GAP = 10;
	public static int PANEL_DEFAULT_LINE_THICKNESS = 10;
	// //////////////////////
	// IMAGES
	// //////////////////////
	public static Image splashImage = null;
	public static Image signalImage;
	public static Image quitImage;
	public static Image optionsImage;
	public static Image serverImage;
	public static Image searchImage;
	public static Image loadingImage;
	public static Image okImage;
	public static Image nopeImage;
	public static Image returnImage;
	public static Image soundOnImage;
	public static Image soundOffImage;
	public static Image minimizeImage;
	public static Image maximizeImage;
	public static Image reduceImage;
	public static Image paintBrushImage;
	public static Image receiveImage;
	public static Image receiveLittleImage;
	public static Image screenSharingImage;
	public static Image discussionImage;
	public static Image audioConferenceImage;
	public static Image minerImage;
	public static Image mineWagonImage;

	// //////////////////////
	// COLORS
	// //////////////////////
	public static int indexColorSet;
	public static Color primaryColor;
	public static Color secondaryColor;
    public static Color textColor;
	public static Color optionsColor = new Color(1, 54, 31);
	public static Color quitColor = new Color(54, 1, 1);
	public static Color transparentWhite = new Color(255, 255, 255, 100);
	public static final Color lineColor = transparentWhite;
	public static Color transparent = new Color(0, 0, 0, 0);
	public static ColorPair[] colors = new ColorPair[] {
			new ColorPair(new Color(52, 73, 94), new Color(1, 31, 54)),
			new ColorPair(new Color(56, 110, 40), new Color(30, 60, 21)),
			new ColorPair(new Color(214, 44, 33), new Color(60, 13, 10)),
			new ColorPair(new Color(119, 45, 65), new Color(62, 23, 34)),
			new ColorPair(new Color(145, 72, 72), new Color(65, 33, 33)),
			new ColorPair(new Color(109, 58, 243), new Color(28, 5, 86)),
			new ColorPair(new Color(49, 106, 119), new Color(15, 33, 36)),
			new ColorPair(new Color(135, 147, 55), new Color(62, 67, 24)),
			new ColorPair(new Color(167, 167, 167), new Color(70, 70, 70)) };
	public static Font titleFont;
	public static Font middleTitleFont;
	public static Font littleTitleFont;
	public static Font veryLittleTitleFont;
	public static Font textFont;
	public static Font bigTextFont;
	public static Font computerFont;
	// //////////////////////
	// FONTS
	// //////////////////////
	public static  Color caseColor;
	private static ArrayList<ColorChangeable> changeablesPanels = new ArrayList<>();
	private static boolean soundOn = true;
	private static boolean soundFinished = true;
	private static boolean soundPlayed = false;

    public static void registerColorChangeable (ColorChangeable changeable) {
        if(!changeablesPanels.contains(changeable))
            changeablesPanels.add(changeable);
    }

	public static Image getImage(String name) {
		// String path = "/fr.emse.rootz.mineschat.display.options/";
		String path = "";
		return new ImageIcon(DisplaySettings.class.getResource(path + name))
				.getImage();
	}

	private static Font getFont(String name) {
		// String path = "/fr.emse.rootz.mineschat.display.options/";
		String path = "";
		Font font = null;
		try {
			InputStream input = DisplaySettings.class.getResourceAsStream(name);
			font = Font.createFont(Font.TRUETYPE_FONT, input);
			GraphicsEnvironment.getLocalGraphicsEnvironment()
					.registerFont(font);
		} catch (FontFormatException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return font;
	}

	// //////////////////////
	// DISPLAY
	// //////////////////////

	public static void prepareResources() {
		if(splashImage==null)
			splashImage = getImage("splash.png");
		quitImage = getImage("white_cat_paw.png");
		optionsImage = getImage("options.png");
		serverImage = getImage("cloud-server.png");
		searchImage = getImage("search2.png");
		loadingImage = getImage("cat_hugging_fish.gif");
		okImage = getImage("fallout_guy_little.png");
		nopeImage = getImage("sad_panda.png");
		returnImage = getImage("return.png");
		soundOnImage = getImage("volume_on.png");
		soundOffImage = getImage("volume_off.png");
		minimizeImage = getImage("minimize_little.png");
		maximizeImage = getImage("maximize_little.png");
		reduceImage = getImage("reduce_little.png");
		signalImage = getImage("signalImage.png");
		paintBrushImage = getImage("little_pinceau.png");
		receiveImage = getImage("receive.png");
		screenSharingImage = getImage("screensharing.png");
		discussionImage = getImage("discussion.png");
		receiveLittleImage = getImage("receive_little.png");
		audioConferenceImage = getImage("audioConference.png");
		minerImage = getImage("miner.png");
		mineWagonImage = getImage("mine_wagon.png");

		titleFont = getFont("KGBrokenVesselsSketch.ttf").deriveFont(110f);
		middleTitleFont = getFont("KGBrokenVesselsSketch.ttf").deriveFont(40f);
		littleTitleFont = getFont("Hazelnut_Water.ttf").deriveFont(60f);
		veryLittleTitleFont = getFont("Hazelnut_Water.ttf").deriveFont(40f);
		textFont = getFont("GeosansLight.ttf").deriveFont(20f);
		bigTextFont = getFont("GeosansLight.ttf").deriveFont(40f);
		computerFont = getFont("Computerfont.ttf").deriveFont(40f);

		System.setProperty("sun.net.useExclusiveBind", "false");

		generalUIModifications();
		DataUtils.initializeCompressionTools();
	}

	private static void generalUIModifications() {
		MCStrings.prepareStrings();
		indexColorSet = readColorIndexFromSettings();

        changeColors(indexColorSet);

		UIManager.put("TabbedPane.contentBorderInsets", new Insets(0, 0, 0, 0));
		UIManager.put("TabbedPane.selected", DisplaySettings.transparentWhite);
		UIManager.put("TabbedPane.selectedForeground",
				DisplaySettings.secondaryColor);
		UIManager.put("ToolTip.background", new ColorUIResource(DisplaySettings.secondaryColor));
		UIManager.put("ToolTip.foreground", new ColorUIResource(Color.white));
		UIManager.put("ToolTip.border", BorderFactory.createLineBorder(DisplaySettings.transparentWhite));
		ToolTipManager.sharedInstance().setDismissDelay(15000);
	}

    private static void changeColors (int indexColorSet) {
        // S'il s'agit d'une ancienne couleur, on prend celle du tableau
        if(!isNewVersionColor(indexColorSet)) {
            primaryColor = colors[indexColorSet].primary;
            secondaryColor = colors[indexColorSet].secondary;
        }
        // Sinon on récupère les composantes stockées dedans
        else {
            primaryColor = new Color(red(indexColorSet), blue(indexColorSet), green(indexColorSet));
            secondaryColor = getMean(primaryColor, Color.black);
        }
        caseColor = getMean(getMean(secondaryColor, Color.white), Color.white);


        // Si la couleur est sombre, on laisse le texte en blanc
        if((primaryColor.getRed() + primaryColor.getBlue() + primaryColor.getGreen())/3 > 255*4/5) {
			textColor = Color.black;
        }
        else
			textColor = Color.white;


        // On met à jour tous les composants dont la couleur peut changer
        changeablesPanels.stream().filter( (panel) -> panel != null).forEach(ColorChangeable::updateColors);
    }

	public static boolean isNewVersionColor (int colorIndex) {
		//TODO: A mettre à jour
		return colorIndex>COLOR_SHIFT-1;
	}

	public static int toIntColor(int r, int g, int b) {
		int c = r;
		c = (c << 8) | g;
		c = (c << 8) | b;
        c += COLOR_SHIFT;
		return c;
	}

	public static int red(int c) {
		return (c-COLOR_SHIFT) >> 16 & 0xFF;
	}

	public static int green(int c) {
		return (c-COLOR_SHIFT) >> 8 & 0xFF;
	}

	public static int blue(int c) {
		return (c-COLOR_SHIFT) & 0xFF;
	}

	public static ColorPair[] getColors() {
		return colors;
	}

	public static int getColorIndex() {
		return indexColorSet;
	}

	public static Color getMean(Color color1, Color color2) {
		return new Color((color1.getRed() + color2.getRed()) / 2,
				(color1.getGreen() + color2.getGreen()) / 2,
				(color1.getBlue() + color2.getBlue()) / 2);
	}

	public static Color toPrimaryColor(int colorIndex) {
        if(isNewVersionColor(colorIndex))
		    return new Color(red(colorIndex), blue(colorIndex), green(colorIndex));
        else
            return colors[colorIndex].secondary;
	}

	public static Color toSecondaryColor(int colorIndex) {
        if(isNewVersionColor(colorIndex))
		    return getMean(new Color(red(colorIndex), blue(colorIndex), green(colorIndex)), Color.black);
        else
            return colors[colorIndex].secondary;
	}

	public static void changeColorSet(int newIndex) {
		indexColorSet = newIndex;
        changeColors(newIndex);
		String text = "" + indexColorSet;
		//String text = "" + toIntColor(122, 122, 122);

		try {
			File file = new File(getUserDataDirectory() + "settings.txt");
			file.getParentFile().mkdirs();
			// PrintWriter writer = new
			// PrintWriter(getUserDataDirectory()+"settings.txt", "UTF-8");
			PrintWriter writer = new PrintWriter(file, "UTF-8");
			writer.write(text);
			writer.close();
		} catch (FileNotFoundException | UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private static int readColorIndexFromSettings() {
		int index = 0;
		File file = new File(getUserDataDirectory() + "settings.txt");
		if (file.exists()) {
			try {
				Scanner scanner = new Scanner(file);
				index = scanner.nextInt();
				scanner.close();
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return index;
	}

	public static String getUserDataDirectory() {
		return System.getProperty("user.home") + File.separator + ".roots"
				+ File.separator + "Mines'Chat" + File.separator;
	}


	//////////////////////
	//    ART GRID 
	//////////////////////

	public static boolean isSoundOn() {
		return soundOn;
	}

	public static void toggleSound() {
		soundOn = !soundOn;
	}

	public static void ableFutureSound() {
		soundPlayed = false;
	}

	public static void playSound(final String soundName) {
		if (!isSoundOn() || !soundFinished || soundPlayed)
			return;
		new Thread(() -> {
            try {
                soundPlayed = true;
                soundFinished = false;
                class AudioListener implements LineListener {

                    @Override
                    public synchronized void update(LineEvent event) {
                        Type eventType = event.getType();
                        if (eventType == Type.OPEN) {
                            System.out.println("OPEN");
                        } else if (eventType == Type.CLOSE) {
                            System.out.println("CLOSE");
                        } else if (eventType == Type.START) {
                            System.out.println("START");
                        } else if (eventType == Type.STOP) {
                            System.out.println("STOP");
                        }

                        if (eventType == Type.STOP
                                || eventType == Type.CLOSE) {
                            soundFinished = true;
                            notifyAll();
                        }
                    }

                    public synchronized void waitUntilDone()
                            throws InterruptedException {
                        while (!soundFinished) {
                            wait();
                        }
                    }
                }

                AudioListener listener = new AudioListener();


				try (AudioInputStream audioInputStream = AudioSystem
						.getAudioInputStream(new BufferedInputStream(
								DisplaySettings.class
										.getResourceAsStream(soundName)))) {
					//Clip clip = AudioSystem.getClip();
					Clip clip = (Clip) AudioSystem.getLine(new DataLine.Info(Clip.class, audioInputStream.getFormat()));
					clip.addLineListener(listener);
					clip.open(audioInputStream);
					/*
					if (clip.isControlSupported(FloatControl.Type.MASTER_GAIN)) {
						// If inside this if, the Master_Gain must be supported. Yes?
						FloatControl volume = (FloatControl) clip.getControl(FloatControl.Type.MASTER_GAIN);
						// This line throws an exception. "Master_Gain not supported"
						volume.setValue(-10.0F);
					}*/

					try {
						clip.start();
						listener.waitUntilDone();
					} finally {
						clip.close();
					}
				}
            } catch (IOException | UnsupportedAudioFileException
                    | LineUnavailableException | InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }).start();

	}

}