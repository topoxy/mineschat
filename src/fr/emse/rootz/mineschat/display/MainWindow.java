package fr.emse.rootz.mineschat.display;

import fr.emse.rootz.mineschat.addon.AddonBase;
import fr.emse.rootz.mineschat.display.components.ColoredPanel;
import fr.emse.rootz.mineschat.display.components.PhasePanel;
import fr.emse.rootz.mineschat.display.options.DisplaySettings;
import fr.emse.rootz.mineschat.interfaces.DisplayInterface;
import fr.emse.rootz.mineschat.interfaces.NetworkManagerInterface;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;

/**
 * Fenêtre principale du mode client.
 * C'est elle qui contient absolument tout ce qui concerne l'interface graphique.
 *
 * Elle est elle-même constituée de deux parties,
 * une partie haute qui contient le titre et les boutons d'options,
 * et une partie centrale qui contient les sous-panels.
 *
 */
public class MainWindow extends JFrame implements DisplayInterface {

    // On garde la position de la souris, c'est utilisé pour déplacer la fenêtre en cas de drag et drop.
	public static Point mouseDownCompCoords;
    // Elements de GUI
	private CardLayout layout;
	private ColoredPanel centralPanel;
	private HeaderPanel headerPanel;
	private DiscussionPanel discussionPanel;
	private ArrayList<PhasePanel> panels;
	private PhasePanel.Type previousPhaseName = PhasePanel.Type.NULL;
	private PhasePanel.Type actualPhaseName = PhasePanel.Type.INTRO;
    // Etats de la fenêtre
	private boolean activated = true;
	private boolean extended = false;
	private boolean reduced = false;

	public MainWindow() {
		super("Mines'Chat");

		this.constructGUI();
		this.prepareEvents();
		this.autres();
	}

    /**
     * Construit l'interface graphique du logiciel.
     */
	private void constructGUI() {
        //
		this.setIconImage(DisplaySettings.splashImage); 

		// C'est le panel qui contient tout.
		ColoredPanel mainPanel = new ColoredPanel(ColoredPanel.ColorMode.WITH_BORDERS, 10);
		mainPanel.setLayout(new BorderLayout(5, 5));
		mainPanel.espacerDuReste(15);

		// C'est le panel du haut, où il y a les boutons
		headerPanel = new HeaderPanel(this, PhasePanel.Type.HEADER);
		mainPanel.add(headerPanel, BorderLayout.NORTH);

		// C'est le panel central, dans lequel on va placer tous les PhasePanel
		centralPanel = new ColoredPanel();
		layout = new CardLayout();
		centralPanel.setLayout(layout);

        // On va créer et ajouter tous les sous-panels
		IntroPanel introPanel = new IntroPanel(this, PhasePanel.Type.INTRO);
		ServerPanel serverPanel = new ServerPanel(this, PhasePanel.Type.SERVER);
		ConnectionPanel connectionPanel = new ConnectionPanel(this, PhasePanel.Type.CONNECTION);
		discussionPanel = new DiscussionPanel(this, PhasePanel.Type.DISCUSSION);
		OptionsPanel optionsPanel = new OptionsPanel(this, PhasePanel.Type.OPTIONS);

        // On stocke tous ces sous-panels dans une liste
        // pour qu'ils soient plus facile à manipuler
        panels = new ArrayList<>();
		panels.add(introPanel);
        panels.add(serverPanel);
        panels.add(connectionPanel);
        panels.add(discussionPanel);
        panels.add(optionsPanel);

        // Chaque sous-panel est ajouté au panel central, et également signalé au layout.
		for(PhasePanel panel : panels) {
			layout.addLayoutComponent(panel, PhasePanel.translateType(panel.getType()));
            centralPanel.add(panel);
		}

		mainPanel.add(centralPanel, BorderLayout.CENTER);
		
		this.setContentPane(mainPanel);
		layout.first(centralPanel);
		actualPhaseName = panels.get(0).getType();
	}

	/**
	 * On prépare les évènements liés à la fenêtre, la souris et au clavier,
	 * et on configure la fenêtre avant de la faire apparaitre
	 */
	private void prepareEvents() {
        // Evènement pour agrandir la fenêtre si on détecte un double-clic
		addMouseListener(new MouseAdapter() {
			public void mouseReleased(MouseEvent e) {
				mouseDownCompCoords = null;
			}

			public void mousePressed(MouseEvent e) {
				mouseDownCompCoords = e.getPoint();
			}
			
			public void mouseClicked(MouseEvent e) {
				if (e.getClickCount()==2)
					maximize();
			}
		});

        // Evènement pour enregistrer les mouvements de la souris.
        // Cela servira pour déplacer la fenêtre.
		addMouseMotionListener(new MouseMotionAdapter() {
			public void mouseDragged(MouseEvent e) {
				Point currCoords = e.getLocationOnScreen();
				setLocation(currCoords.x - mouseDownCompCoords.x, currCoords.y
						- mouseDownCompCoords.y);
			}
		});

        // Quand le focus revient à la fenêtre, on remet l'icône du chat Bleu
        // dans les cas où l'icône aurait été passée au Rouge.
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowActivated(WindowEvent arg0) {
				setIconImage(DisplaySettings.splashImage);
				DisplaySettings.ableFutureSound();
				activated = true;
			}
			@Override
			public void windowDeactivated(WindowEvent arg0) {
				activated = false;
			}
			
		});

		GraphicsEnvironment env = GraphicsEnvironment.getLocalGraphicsEnvironment();
		Rectangle usableBounds = env.getMaximumWindowBounds();
		setMaximizedBounds(new Rectangle(0, 0, usableBounds.width, usableBounds.height));
		this.setSize(DisplaySettings.INTRO_SCREEN_SIZE_X, DisplaySettings.INTRO_SCREEN_SIZE_Y);
		this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setUndecorated(true);
		this.setVisible(true);
	}

    /**
     * On récupère des données de dimension pour le mode Réduit de la fenêtre.
     */
	private void autres() {
		GraphicsEnvironment env = GraphicsEnvironment.getLocalGraphicsEnvironment();
		GraphicsDevice screen = env.getDefaultScreenDevice();
		GraphicsConfiguration config = screen.getDefaultConfiguration();
		DisplaySettings.REDUCED_EXTENDED_SCREEN_SIZE_Y = config.getBounds().height - Toolkit.getDefaultToolkit().getScreenInsets(getGraphicsConfiguration()).bottom;
	}

	/**
	 * Permet de changer de PhasePanel.
	 * En gros, l'utilisateur a appuyé sur un bouton qui fait avancer ou reculer le programme,
	 * et et on doit mettre le panel qui correspond au nom donné.
	 * @param type Intitulé du panel à faire apparaître
	 */
	@Override
	public void changePhase(PhasePanel.Type type) {
		// On signale à la phase actuelle qu'on va la quitter.
		// Ca peut servir pour faire des transitions, et aussi pour les déconnexions réseau.
		if(actualPhaseName != PhasePanel.Type.NULL)
			getPhasePanel(actualPhaseName).exiting();

		// On change le nom de la phase actuelle
		if(actualPhaseName != type) {
			previousPhaseName = actualPhaseName;
			actualPhaseName = type;
		}
		// et on fait apparaître le nouveau panel
		layout.show(centralPanel, PhasePanel.translateType(type));
		// tout en lui signalant que c'est à son tour de jouer.
		getPhasePanel(type).entering();
	}

	/**
	 * Petite fonction pour retourner le panel qui correspond.
	 * @param type Type du panel à retourner
	 * @return Le panel qui correspond au nom
	 */
	private PhasePanel getPhasePanel(PhasePanel.Type type) {
		return panels.stream().filter(p -> p.getType() == type).findFirst().get();
	}

	/**
	 * Permet de revenir au panel précédent, plutôt pratique pour le bouton retour.
	 */
	@Override
	public void goPreviousPhase() {
		changePhaseDiscretly(previousPhaseName);
	}

    /**
     * Permet de changer de phase discrètement (sans activer les fonctions "entering" et "exiting" des phases.
     * Est utilisée par le bouton retour et options.
     * @param type
     */
	@Override
	public void changePhaseDiscretly(PhasePanel.Type type) {

		if(actualPhaseName != PhasePanel.Type.NULL && actualPhaseName != type) {
            previousPhaseName = actualPhaseName;
            actualPhaseName = type;
            layout.show(centralPanel, PhasePanel.translateType(actualPhaseName));
		}

	}

    /**
     * Minimise la fenêtre.
     */
	@Override
	public void minimize() {
		this.setExtendedState(JFrame.ICONIFIED);
	}

    /**
     * Maximise la fenêtre.
     */
	@Override
	public void maximize() {
			if(!extended) {
				if(!reduced) {
					setExtendedState(getExtendedState()|JFrame.MAXIMIZED_BOTH );
				}
				else {
					setSize(DisplaySettings.currentScreenSizeX, DisplaySettings.REDUCED_EXTENDED_SCREEN_SIZE_Y);
					setLocationRelativeTo(null);
				}
				//setExtendedState(JFrame.MAXIMIZED_BOTH);
				extended = true;
			}
			else {
				setExtendedState(JFrame.NORMAL);
				setSize(DisplaySettings.currentScreenSizeX, DisplaySettings.currentScreenSizeY);
				extended = false;
			}
	}

    /**
     * Réduit la fenêtre.
     * Par réduction, en fait ça fait disparaître la partie de droite avec la liste des clients,
     * et ça réduit la largeur de la fenêtre.
     * Il faut également adapter cela en fonction de si la fenêtre a été maximisée ou pas.
     */
	public void reduce() {
		reduced = !reduced;
		
		DisplaySettings.reduced = reduced;

        // Réduction
		if(reduced) {
			setExtendedState(JFrame.MAXIMIZED_VERT);
			headerPanel.changeTitle("");
			DisplaySettings.currentScreenSizeX = DisplaySettings.REDUCED_SCREEN_SIZE_X;
			DisplaySettings.currentScreenSizeY = DisplaySettings.REDUCED_SCREEN_SIZE_Y;
			if(!extended)
				setSize(DisplaySettings.currentScreenSizeX, DisplaySettings.currentScreenSizeY);
			//setLocationRelativeTo(null);
			discussionPanel.setClientListsVisible(false);
			//setAlwaysOnTop(true);
		}
        // Rétablissement de la taille normale
		else {
			headerPanel.changeTitle("Mines'Chat");
			DisplaySettings.currentScreenSizeX = DisplaySettings.INTRO_SCREEN_SIZE_X;
			DisplaySettings.currentScreenSizeY = DisplaySettings.INTRO_SCREEN_SIZE_Y;
			if(!extended)
				setSize(DisplaySettings.currentScreenSizeX, DisplaySettings.currentScreenSizeY);
			discussionPanel.setClientListsVisible(true);
			//setAlwaysOnTop(false);
			//setLocationRelativeTo(null);
		}
	}

    public void setNetworkManager (NetworkManagerInterface networkManager) {
        discussionPanel.setNetworkManager(networkManager);
    }

	public void setAddonBase (AddonBase addonBase) {
		discussionPanel.setAddonBase(addonBase);
	}

	@Override
	public boolean isWindowDeactivated() {
		return !activated;
	}

    /**
     * Change l'icône de la fenêtre pour la version Rouge du chat.
     */
	@Override
	public void signal() {
		if(isWindowDeactivated() && !discussionPanel.isArtDialogActivated()) {
			DisplaySettings.playSound("son.wav");
			setIconImage(DisplaySettings.signalImage);
		}

	}
	
	public JFrame getWindow () {
		return this;
	}

	public DiscussionPanel getDiscussionPanel() {
		return discussionPanel;
	}
}
