package fr.emse.rootz.mineschat.display.splashscreen;

import fr.emse.rootz.mineschat.display.options.DisplaySettings;

import javax.swing.*;
import java.awt.*;

public class SplashScreen extends JWindow {
    private static final long serialVersionUID = 1L;

    ImageIcon ii;

    private boolean loaded = false;

    public SplashScreen() {
        try {
        	if(DisplaySettings.splashImage == null)
        		DisplaySettings.splashImage = DisplaySettings.getImage("splash.png");

            ii = new ImageIcon(DisplaySettings.splashImage);

            this.setContentPane(new SplashPicture());

            setSize(ii.getIconWidth(), ii.getIconHeight());
            setLocationRelativeTo(null);
            setBackground(new Color(0, 255, 0, 0));

            loaded = true;
        } catch (Exception exception) {
            exception.printStackTrace();
        }

        repaint();
    }

    public void showSplashScreen(boolean flag) {
        if (!loaded) {
            System.err.println("Splash screen image isn't loaded.");
            return;
        }
        setVisible(flag);
    }

    public void dispose() {
        super.dispose();
    }


    class SplashPicture extends JPanel {

        public void paintComponent(Graphics g) {
            //g.drawImage(backgroundImage, 0, 0, null);
            g.drawImage(DisplaySettings.splashImage, 0, 0, null);
        }
    }
}
